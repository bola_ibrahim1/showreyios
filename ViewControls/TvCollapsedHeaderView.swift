//
//  TvCollapsedHeaderView.swift
//  ShowRey
//
//  Created by Appsinnovate on 1/14/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(header: TvCollapsedHeaderView, section: Int)
}

class TvCollapsedHeaderView: UITableViewHeaderFooterView {

    
@IBOutlet weak var lblTxtQuestion: UILabel!
    
    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0
    
     let arrowLabel = UILabel()
    
    let titleLabel = UILabel()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        //
        // Constraint the size of arrow label for auto layout
        //
        if #available(iOS 9.0, *) {
            arrowLabel.widthAnchor.constraintEqualToConstant(12).active = true
            arrowLabel.heightAnchor.constraintEqualToConstant(12).active = true
        } else {
            // Fallback on earlier versions
        } //widthAnchor.constraint(equalToConstant: 12).isActive = true
        
       // lblTxtQuestion.text = "hfggfghffhhffgfdfds"
      
        arrowLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
         titleLabel.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(arrowLabel)
        contentView.addSubview(titleLabel)
        //
        // Call tapHeader when tapping on this header
        //
        
        addGestureRecognizer(UITapGestureRecognizer(target:self, action:#selector(TvCollapsedHeaderView.tapHeader(_:))))
        
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        contentView.backgroundColor = UIColor(hex: 0xb379e7)
        
       
        arrowLabel.textColor = UIColor.whiteColor()
        titleLabel.textColor = UIColor.whiteColor()
        
        let views = [
                    "titleLabel" : titleLabel,
                    "arrowLabel" : arrowLabel,
                ]
        
       contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-5-[titleLabel]-[arrowLabel]-5-|", options: [], metrics: nil, views: views))
//        
//        
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[titleLabel]-|", options: [], metrics: nil, views: views))
//        
//        
        contentView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[arrowLabel]-|", options: [], metrics: nil, views: views))
//        
    }

    func tapHeader(gestureRecognizer : UITapGestureRecognizer){
        guard let cell = gestureRecognizer.view as? TvCollapsedHeaderView else {
            return
        }
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(collapsed: Bool){
        //
        // Animate the arrow rotation (see Extensions.swf)
        //
        arrowLabel.rotate(collapsed ? 0.0 : CGFloat(M_PI_2))
    }


    func setcell(headerTitle: String)
    {
        //titleLabel.numberOfLines = 0
        
        titleLabel.sizeToFit()
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.font = UIFont(name: "Montserrat-Bold", size: 50)
        titleLabel.text = headerTitle
    }
    
}
