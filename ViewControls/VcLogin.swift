//
//  VcLogin.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 9/17/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import PureLayout
import EGFloatingTextField
import FacebookLogin
import FacebookCore
import FBSDKShareKit
import Toast_Swift
import MBProgressHUD
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import Firebase
import FirebaseCrash
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import Crashlytics



enum LoginTypes : Int
{
    case Normal=0
    case Facebook=1
    case Instagram=2
}

class VcLogin: UIViewController,UITextFieldDelegate,CountryListViewDelegate{
    
    static var Username:String = ""
    // @IBOutlet weak var ChkRemember: CheckboxButton!
    //@IBOutlet weak var TxtUserName: EGFloatingTextField!
    @IBOutlet weak var mobileNumberTextField: EGFloatingTextField!
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    
    
    @IBOutlet weak var RemembeerMeOutlet: UIButton!
    var RememberMe : Bool = true;
    static var savedPinCode = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
         TxtUserName.floatingLabel = true
         TxtUserName.validationType = .OnlyText
         // TxtUserName.errorLabel = LblErrorUser
         
         if let accessToken = AccessToken.current {
         // User is logged in, use 'accessToken' here.
         //   "id,name,link,email,picture.type(large),gender, birthday,first_name,last_name,locale,timezone,updated_time,verified");
         
         let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,name,link,email,picture.type(large),gender, birthday,first_name,last_name,locale,timezone,updated_time,verified"], tokenString:"\(accessToken)", version: nil, HTTPMethod: "GET")
         req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
         if(error == nil)
         {
         print("result \(result)")
         }
         else
         {
         print("error \(error)")
         }
         })
         }
         
         
         
         TxtPassword.floatingLabel = true
         TxtPassword.setPlaceHolder("Password")
         
         
         
         
         TxtUserName.floatingLabel = true
         TxtUserName.setPlaceHolder("User Name")
         
         
         */
        
        
        //AA : To setup mobile number textField
        mobileNumberTextField.floatingLabel = true
        mobileNumberTextField.validationType = .Phone
        mobileNumberTextField.setPlaceHolder("Mobile Number")
        
        let loginButton = LoginButton(readPermissions: [ .PublicProfile, .Email, .UserFriends ])
        loginButton.center = view.center
        
        NSUserDefaults.standardUserDefaults().setObject(FIRInstanceID.instanceID().token(), forKey: "regId")
        
        //openedScreen = self
        /*
         let TxtUserNameSky = SkyFloatingLabelTextField(frame: CGRectMake(10, 10, 120, 45))
         TxtUserNameSky.placeholder = "Arrival"
         TxtUserNameSky.title = "Flying to"
         TxtUserNameSky.tintColor = UIColor.blueColor()
         TxtUserNameSky.selectedTitleColor = UIColor.redColor()
         TxtUserNameSky.selectedLineColor = UIColor.greenColor()
         
         self.view.addSubview(TxtUserNameSky)*/
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        self.title = "Login"
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func BtnLogin(sender: AnyObject)
    {
        
        if(mobileNumberTextField.text == "")
        {
            self.view.makeToast("Please Enter Mobile Number")
            
            return
        }
        
        self.view.endEditing(true)
        //VcLogin.Login(self,mobileNubmer: mobileNumberTextField.text!);
        
        //AA : To remove the first zero if its zero
        if (mobileNumberTextField.text?.hasPrefix("0"))! {
            mobileNumberTextField.text?.removeAtIndex((mobileNumberTextField.text?.startIndex)!)
        }
        
        if (self.countryCodeLabel.text?.hasPrefix("+"))! {
            self.countryCodeLabel.text?.removeAtIndex((self.countryCodeLabel.text?.startIndex)!)
        }
        
        mobileNumberValidator(self.countryCodeLabel.text! + self.mobileNumberTextField.text!)
        
        //sendPinCode(self, mobileNubmer: countryCodeLabel.text! + mobileNumberTextField.text!)
        
    }
    
    static func Login(target:UIViewController,mobileNubmer:String)
    {
        
        
        var token = FIRInstanceID.instanceID().token()
        // print("InstanceID token: \(token)")
        
        if token == nil {
            
            token = NSUserDefaults.standardUserDefaults().stringForKey("regId")
            
            if token == nil {
                token = ""
            }
        }
        
        // login
        let RequestParameters : NSDictionary = [
            "RegId" : token ?? "",
            "MobileNumber" : mobileNubmer,
            "PinCode" : self.savedPinCode
            
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(target.view, animated: true)
        loadingNotification.label.text = "Logging in"
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.Login, hTTPMethod: .post, parameters: nil, httpBody: RequestParametersString, responseType: .DictionaryJson, callbackString: nil, callbackDictionary: { (JSON, NSError) in
            MBProgressHUD.hideHUDForView(target.view, animated: true)
            if(NSError != nil)
            {
                if (NSError!.code == -1009)
                {
                    target.view.makeToast("No Internet connection")
                }
                else
                {
                    target.view.makeToast("An error has been occurred")
                    print("error")
                }
            }
            else
            {
                
                let ResultResponse = JSON["ResultResponse"].stringValue
                
                print(ResultResponse)
                
                //  VcLogin.LastLoginType = .Instagram
                if (ResultResponse == "0" || ResultResponse == "1")
                {
                    let userid = JSON["UserId"].stringValue
                    // get profile
                    VcLogin.GetProfile(target,UserID: userid){success,pro in
                        if(success)
                        {
                            
                            //AA : To save the mobile number into the user defaults to be able to login directly
                            let userDefaults = NSUserDefaults.standardUserDefaults()
                            userDefaults.setValue(mobileNubmer, forKey: "MobileNumber")
                            
                            /*
                             if(rememberMe)
                             {
                             let userDefaults = NSUserDefaults.standardUserDefaults()
                             userDefaults.setValue(username, forKey: "UserName")
                             userDefaults.setValue(password, forKey: "Password")
                             userDefaults.synchronize();
                             LastLoginType = LoginTypes.Normal;
                             }
                             */
                            let welcome = AppDelegate.RoutToScreen("VcPostingOptions") as! VcPostingOptions
                            
                            
                            if redirectionId != nil{
                                // welcome.openLink = true
                                if redirectionScreen == "VcOffersDetails" {
                                    let offersDetails = VcOffersDetails(nibName: redirectionScreen, bundle: nil)
                                    offersDetails.loadFromID(redirectionId)
                                    AppDelegate.navControler.pushViewController(offersDetails, animated: true)
                                    
                                }
                            }
                        }
                    }
                    
                }
                    //                else if (ResultResponse == "1")
                    //                {
                    //                    // no verefied
                    //                    target.view.makeToast("This account has not been verified")
                    //                }
                    
                else if (ResultResponse == "2")
                {
                    //   invalid user or pass
                    target.view.makeToast("Invalid username or password")
                }
                else
                {
                    target.view.makeToast("Somthing went wrong. try again")
                }
            }
            
        })
    }
    
    @IBAction func InstgramLogin(sender: AnyObject)
    {
        if(!InstagramEngine.sharedEngine.LoadToken())
        {
            InstagramEngine.sharedEngine.loginFromViewController(self) { (success, error) in
                if(success)
                {
                    print("Got instagram access token")
                    VcLogin.InstgramLogin(self)
                }
                else
                {
                    self.view.makeToast(error!.localizedDescription)
                    print(error)
                }
            }
        }
        else
        {
            VcLogin.InstgramLogin(self)
        }
    }
    //    internal static func InstgramFetchProfile(completionHandler: ((success:Bool)->())?)
    //    {
    //        InstagramEngine.sharedEngine.selfWithSuccessClosure({ (profile, meta) in
    //            print(profile)
    //
    //            let InstUser = ModProfileInformation()
    //            //id
    //            InstUser.UserName = profile.username
    //            InstUser.FullName = profile.fullName
    //
    //
    //            // load the profile to a global variable
    //            if((completionHandler) != nil)
    //            {
    //                completionHandler!(success: meta.code == "200");
    //            }
    //
    //            }, error: { (error) in
    //                if((completionHandler) != nil)
    //                { completionHandler!(success: false);}
    //                print(error)
    //        })
    //    }
    
    /// instagram
    ///
    /// - parameter target: the view in which to view the alert or the loading views or toasts
    internal static func InstgramLogin(target:UIViewController)
    {
        var token = FIRInstanceID.instanceID().token()
        //print("InstanceID token: \(token!)")
        
        if token == nil {
            
            token = NSUserDefaults.standardUserDefaults().stringForKey("regId")
            
            if token == nil {
                token = ""
            }
        }
        
        
        if(InstagramEngine.sharedEngine.LoadToken())
        {
            InstagramEngine.sharedEngine.selfWithSuccessClosure({ (profile, meta) in
                print(profile)
                if(meta.code == "200")
                {
                    // login social
                    let RequestParameters : NSDictionary = [
                        "RegId" : token!,
                        "RegType" : 3,
                        "SUserId" : profile.ID!
                    ]
                    let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
                    
                    NetworkHelper.RequestHelper(nil, service: WSMethods.LoginSocial, hTTPMethod: .post, parameters: nil
                        , httpBody: RequestParametersString, responseType: .DictionaryJson, callbackString: nil, callbackDictionary: { (JSON, NSError) in
                            
                            if(NSError != nil)
                            {
                                if (NSError!.code == -1009) {
                                    target.view.makeToast("No Internet connection")
                                }
                                else
                                {
                                    target.view.makeToast("An error has been occurred");
                                    print("error")
                                }
                            }
                            else
                            {
                                
                                if (JSON["ResultResponse"].stringValue == "0")
                                {
                                    
                                    
                                    let userid = JSON["UserId"].stringValue
                                    // get profile
                                    VcLogin.GetProfile(target,UserID: userid){success,pro in
                                        if(success)
                                        {
                                            VcLogin.LastLoginType = .Instagram
                                            let welcome = AppDelegate.RoutToScreen("VcPostingOptions") as! VcPostingOptions
                                            //  welcome.openLink = true
                                            
                                            if redirectionId != nil{
                                                if redirectionScreen == "VcOffersDetails" {
                                                    let offersDetails = VcOffersDetails(nibName: redirectionScreen, bundle: nil)
                                                    offersDetails.loadFromID(redirectionId)
                                                    AppDelegate.navControler.pushViewController(offersDetails, animated: true)
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                                else if (JSON["ResultResponse"].stringValue == "2")
                                {
                                    // regestration
                                    let regScreen = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcRegisteration") as! VcRegisteration
                                    regScreen.instagramProfile = profile;
                                    //  target.navigationController!.presentViewController(regScreen, animated: true, completion: nil)
                                    
                                    target.navigationController?.pushViewController(regScreen, animated: true)
                                }
                            }
                            
                    })
                }
                else
                {
                    AppDelegate.RoutToScreen("VcLogin")
                }
                
                }, error: { (error) in
                    print(error)
                    AppDelegate.RoutToScreen("VcLogin")
                    
            })
            
        }
        else
        {
            AppDelegate.RoutToScreen("VcLogin")
        }
        
    }
    
    internal static var LastLoginType : LoginTypes!
        {
        get
        {
            let val = NSUserDefaults.standardUserDefaults().objectForKey("LastLoginType")
            if val == nil {return nil}
            return (LoginTypes(rawValue: (val?.integerValue)!));
        }
        set
        {
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setValue(newValue.rawValue, forKey: "LastLoginType")
            userDefaults.synchronize();
        }
    }
    
    
    @IBAction func BtnRememberMe(sender: AnyObject)
    {
        if(!RememberMe)
        {
            RemembeerMeOutlet.setImage(UIImage(named: "Check-box-selected"), forState: .Normal)
        }
        else
        {
            RemembeerMeOutlet.setImage(UIImage(named: "Check-box-unselected"), forState: .Normal)
        }
        RememberMe = !RememberMe;
    }
    
    @IBAction func BtnFaceBook(sender: AnyObject)
    {
        let loginManager = LoginManager()
        
        if((AccessToken.current) == nil)
        {
            loginManager.logIn([ .PublicProfile, .Email, .UserFriends ], viewController: self) { loginResult in
                switch loginResult {
                case .Failed(let error):
                    
                    self.view.makeToast("An error has been occurred");
                    print("User cancelled login.")
                case .Cancelled:
                    self.view.makeToast("User cancelled login.");
                    print("User cancelled login.")
                case .Success(let grantedPermissions, let declinedPermissions, let accessToken):
                    print("Logged in!")
                    VcLogin.FaceBookLogin(self);
                }
            }
        }
        else
        {
            MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            // loadingNotification.label.text = "Logging in"
            
            VcLogin.FaceBookLogin(self);
        }
        
        //VcLogin.FaceBookLogin(self);
    }
    static func FaceBookLogin(target:UIViewController)
    {
        var token = FIRInstanceID.instanceID().token()
        
        if token == nil {
            
            token = NSUserDefaults.standardUserDefaults().stringForKey("regId")
            
            if token == nil {
                token = ""
            }
        }
        
        // print("InstanceID token: \(token!)")
        
        if let accessToken = AccessToken.current {
            // User is logged in, use 'accessToken' here.
            
            let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name,picture.type(large)"], tokenString:"\(accessToken)", version: nil, HTTPMethod: "GET")
            req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
                MBProgressHUD.hideHUDForView(target.view, animated: true)
                if(error == nil)
                {
                    print("result \(result)")
                    
                    let resultdict = result as? NSDictionary
                    
                    if resultdict != nil {
                        // Extract a value from the dictionary
                        let FacebookID = resultdict!["id"] as? String
                        let FacebookEmail = resultdict!["email"] as? String
                        let FacebookName = resultdict!["name"] as? String
                        let FacebookImageUrl = resultdict!["picture"]!["data"]!["url"] as? String
                        let IsDefaultPicture = resultdict!["picture"]!["data"]!["is_silhouette"] as? Int
                        
                        
                        
                        // login social
                        let RequestParameters : NSDictionary = [
                            "RegId" : token!,
                            "RegType" : 2,
                            "SUserId" : FacebookID!
                        ]
                        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
                        
                        NetworkHelper.RequestHelper(nil, service: WSMethods.LoginSocial, hTTPMethod: .post, parameters: nil
                            , httpBody: RequestParametersString, responseType: .DictionaryJson, callbackString: nil, callbackDictionary: { (JSON, NSError) in
                                
                                if(NSError != nil)
                                {
                                    if (NSError!.code == -1009) {
                                        target.view.makeToast("No Internet connection")
                                    }
                                    else
                                    {
                                        target.view.makeToast("An error has been occurred");
                                        print("error")
                                    }
                                }
                                else
                                {
                                    
                                    if (JSON["ResultResponse"].stringValue == "0")
                                    {
                                        
                                        let userid = JSON["UserId"].stringValue
                                        // get profile
                                        VcLogin.GetProfile(target,UserID: userid){(success,pro) in
                                            if(success)
                                            {
                                                VcLogin.LastLoginType = LoginTypes.Facebook
                                                let welcome = AppDelegate.RoutToScreen("VcPostingOptions") as! VcPostingOptions
                                                //   welcome.openLink = true
                                                
                                                if redirectionId != nil{
                                                    if redirectionScreen == "VcOffersDetails" {
                                                        let offersDetails = VcOffersDetails(nibName: redirectionScreen, bundle: nil)
                                                        offersDetails.loadFromID(redirectionId)
                                                        AppDelegate.navControler.pushViewController(offersDetails, animated: true)
                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else if (JSON["ResultResponse"].stringValue == "2")
                                    {
                                        // regestration
                                        let regScreen = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcRegisteration") as! VcRegisteration
                                        regScreen.FacebookID = FacebookID!;
                                        
                                        regScreen.FacebookEmail = FacebookEmail!
                                        regScreen.FacebookName = FacebookName!
                                        regScreen.FacebookImageUrl = IsDefaultPicture == 1 ? "" : FacebookImageUrl!
                                        
                                        //target.navigationController!.presentViewController(regScreen, animated: true, completion: nil)
                                        
                                        
                                        target.navigationController?.pushViewController(regScreen, animated: true)
                                        
                                        
                                    }
                                }
                                
                        })
                        
                        
                        
                    }
                    
                    print("result \(result)")
                    
                    
                }
                else
                {
                    target.view.makeToast(error!.localizedDescription)
                    print("error \(error)")
                    AppDelegate.RoutToScreen("VcLogin")
                }
            })
        }
        else
        {
            AppDelegate.RoutToScreen("VcLogin")
        }
        
    }
    
    @IBAction func BtnForgetPassword(sender: AnyObject)
    {
        
        let secondview = self.storyboard?.instantiateViewControllerWithIdentifier("VcForgetPassword1") as! VcForgetPassword1
        self.navigationController?.pushViewController(secondview, animated: true)
    }
    
    @IBAction func BtnRegister(sender: AnyObject)
    {
        
        
        
        let secondview = self.storyboard?.instantiateViewControllerWithIdentifier("TcRegisteration") as! TcRegisteration
        self.navigationController?.pushViewController(secondview, animated: true)
    }
    
    @IBAction func registerButtonAction(sender: AnyObject) {
        
        let secondview = self.storyboard?.instantiateViewControllerWithIdentifier("TcRegisteration") as! TcRegisteration
        self.navigationController?.pushViewController(secondview, animated: true)
        
    }
    
    @IBAction func loginButtonAction(sender: AnyObject) {
        
        if(mobileNumberTextField.text == "")
        {
            self.view.makeToast("Please Enter Mobile Number")
            
            return
        }
        
        self.view.endEditing(true)
        //VcLogin.Login(self,mobileNubmer: mobileNumberTextField.text!);
        
        //AA : To remove the first zero if its zero
        if (mobileNumberTextField.text?.hasPrefix("0"))! {
            mobileNumberTextField.text?.removeAtIndex((mobileNumberTextField.text?.startIndex)!)
        }
        
        //AA : To remove the first zero if its zero an dthe plus (+) insidce country code
        if (self.mobileNumberTextField.text?.hasPrefix("0"))! {
            self.mobileNumberTextField.text?.removeAtIndex((self.mobileNumberTextField.text?.startIndex)!)
        }
        
        if (self.countryCodeLabel.text?.hasPrefix("+"))! {
            self.countryCodeLabel.text?.removeAtIndex((self.countryCodeLabel.text?.startIndex)!)
        }
        
        mobileNumberValidator(self.countryCodeLabel.text! + self.mobileNumberTextField.text!)
        
        //AA : To call sendPinCode API
        // self.sendPinCode(self, mobileNubmer: self.countryCodeLabel.text! + self.mobileNumberTextField.text!)
        
    }
    
    @IBAction func instagramButtonAction(sender: AnyObject) {
        
        if(!InstagramEngine.sharedEngine.LoadToken())
        {
            InstagramEngine.sharedEngine.loginFromViewController(self) { (success, error) in
                if(success)
                {
                    print("Got instagram access token")
                    VcLogin.InstgramLogin(self)
                }
                else
                {
                    self.view.makeToast(error!.localizedDescription)
                    print(error)
                }
            }
        }
        else
        {
            VcLogin.InstgramLogin(self)
        }
        
    }
    
    static func autologin(target:UIViewController)
    {
        
        VcLogin.GetAppConfiguration(target) {success in
            if(success)
            {
                
                let lastLoginType = VcLogin.LastLoginType
                if(lastLoginType == nil)
                {
                    AppDelegate.RoutToScreen("VcLogin")
                }
                else if(lastLoginType == LoginTypes.Instagram)
                {
                    VcLogin.InstgramLogin(target)
                }
                else if lastLoginType == LoginTypes.Facebook
                {
                    VcLogin.FaceBookLogin(target)
                }
                else if lastLoginType == LoginTypes.Normal
                {
                    
                    
                    //AA : To get the saved mobile number and pin code
                    let mobilenubmer = NSUserDefaults.standardUserDefaults().objectForKey("MobileNumber")
                    let pinCode = NSUserDefaults.standardUserDefaults().objectForKey("PinCode")
                    self.savedPinCode = pinCode as? String ?? ""
                    
                    /*
                     let username = NSUserDefaults.standardUserDefaults().objectForKey("UserName")
                     let password = NSUserDefaults.standardUserDefaults().objectForKey("Password")
                     */
                    if mobilenubmer == nil
                    {
                        
                        AppDelegate.RoutToScreen("VcLogin")
                        
                    }
                        
                    else
                    {
                        VcLogin.Login(target, mobileNubmer: mobilenubmer as! String)
                    }
                }
            }
        }
    }
    
    
    static func GetProfile(target: UIViewController ,ForClient:Bool = true,UserID:String, complition:((success:Bool,profile:ModProfileInformation?)->())?)
    {
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(target.view, animated: true)
        loadingNotification.label.text = "Loading Profile..."
        
        let RequestParameters : NSDictionary = [
            "ProfileUserId" : UserID,
            "UserId" : ForClient ? UserID : (User?.Id)!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetProfileInformation, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(target.view, animated: true)
                
                if(response.result.isFailure)
                {
                    if(complition != nil)
                    {complition!(success: false,profile: nil);}
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            target.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            target.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response = ModProfileInformationResponse(json:JSON)
                        
                        if (Response.ResultResponse != "0"){
                            target.view.makeToast("An error has been occurred")
                            
                            if(complition != nil)
                            {complition!(success: false,profile: nil);}
                        }
                        else
                        {
                            //                            target.view.makeToast("got the profile :D")
                            if ForClient
                            {User = Response.Profileobj;}
                            
                            if(complition != nil)
                            {complition!(success: true,profile: Response.Profileobj);}
                            
                            
                        }
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
    }
    
    static func GetAppConfiguration(target: UIViewController , complition:((success:Bool)->())?)
    {
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(target.view, animated: true)
        loadingNotification.label.text = "Loading App Configuration..."
        
        
        let RequestParameters : NSDictionary = [
            "LUDate" : "11/3/2000"
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetApplicationConfiguration, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(target.view, animated: true)
                
                if(response.result.isFailure)
                {
                    if(complition != nil)
                    {complition!(success: false);}
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            target.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            target.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response = ModAppConfiguration(json:JSON)
                        
                        if (Response.ResultResponse != "0"){
                            target.view.makeToast("An error has been occurred")
                            
                            if(complition != nil)
                            {complition!(success: false);}
                        }
                        else
                        {
                            AppConfiguration = Response
                            
                            if(complition != nil)
                            {complition!(success: true);}
                        }
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
    }
    
    func sendPinCode(target:UIViewController,mobileNubmer:String)
    {
        
        // login
        let RequestParameters : NSDictionary = [
            "MobileNumber" : mobileNubmer
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(target.view, animated: true)
        loadingNotification.label.text = "Logging in"
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.SendPinCode, hTTPMethod: .post, parameters: nil, httpBody: RequestParametersString, responseType: .DictionaryJson, callbackString: nil, callbackDictionary: { (JSON, NSError) in
            MBProgressHUD.hideHUDForView(target.view, animated: true)
            if(NSError != nil)
            {
                if (NSError!.code == -1009)
                {
                    target.view.makeToast("No Internet connection")
                }
                else
                {
                    target.view.makeToast("An error has been occurred")
                    print("error")
                }
            }
            else
            {
                
                let ResultResponse = JSON["ResultResponse"].stringValue
                
                print(ResultResponse)
                
                //  VcLogin.LastLoginType = .Instagram
                if (ResultResponse == "0" || ResultResponse == "1")
                {
                    
                    //AA : Go to pin code screen
                    let pinCodeScreen = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcPinCode") as! PinCodeViewController
                    pinCodeScreen.passedPhoneNumber = self.countryCodeLabel.text! + self.mobileNumberTextField.text!
                    pinCodeScreen.isFromLogin = true
                    
                    target.navigationController?.pushViewController(pinCodeScreen, animated: true)
                    
                }
                else if (ResultResponse == "2")
                {
                    //   invalid user or pass
                    target.view.makeToast("Invalid username or password")
                }
                else
                {
                    target.view.makeToast("Somthing went wrong. try again")
                }
            }
            
        })
    }
    
    func mobileNumberValidator(mobileNumber:String) {
        
        let RequestParameters : NSDictionary = [
            "MobileNumber" : mobileNumber
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.MobileNumberValidator, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString:nil , callbackDictionary:
            {response in
                
                print(response)
                if((response.NSError) != nil)
                {
                    
                    if let networkError = response.NSError {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                        }
                        return
                    }
                }
                else{
                    if (response.JSON["ResultResponse"].stringValue != "1")
                    {
                        if (response.JSON["ResultResponse"].stringValue == "0")
                        {
                            
                            self.view.makeToast("Please Check Mobile Number")
                            
                        }else{
                            
                            self.view.makeToast(response.JSON["Description"].string!)
                            
                        }
                        
                    }
                    else{
                        
                        //AA : To remove the first zero if its zero an dthe plus (+) insidce country code
                        if (self.mobileNumberTextField.text?.hasPrefix("0"))! {
                            self.mobileNumberTextField.text?.removeAtIndex((self.mobileNumberTextField.text?.startIndex)!)
                        }
                        
                        if (self.countryCodeLabel.text?.hasPrefix("+"))! {
                            self.countryCodeLabel.text?.removeAtIndex((self.countryCodeLabel.text?.startIndex)!)
                        }
                        
                        //AA : To call sendPinCode API
                        self.sendPinCode(self, mobileNubmer: self.countryCodeLabel.text! + self.mobileNumberTextField.text!)
                        
                    }
                    
                    
                }
                
            }
        )
    }
    
    func didSelectCountry(country: [NSObject : AnyObject]!) {
        
        //AA : To update country code label
        countryCodeLabel.text = country["dial_code"] as? String ?? ""
        
    }
    
    @IBAction func selectCountryCode(sender: AnyObject) {
        
        let countryListVc = CountryListViewController(nibName: "CountryListViewController", delegate: self)
        self.presentViewController(countryListVc, animated: true, completion: nil)
        
    }
}








