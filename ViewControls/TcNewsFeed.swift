//
//  TcNewsFeed.swift
//  ShowRey
//
//  Created by M-Hashem on 10/21/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Kingfisher
import QuartzCore
import AVKit
import AVFoundation
import KILabel
import SSImageBrowser
import MBProgressHUD

class TcNewsFeed: UITableViewCell,UIActionSheetDelegate,UIAlertViewDelegate{

    @IBOutlet weak var ShareBar: UIView!
    @IBOutlet weak var SharerName: UILabel!
    @IBOutlet weak var PostOwnerName: UILabel!
    @IBOutlet weak var ShareBarHeight: NSLayoutConstraint!
    var ShareBarMaxHeight:CGFloat = 0
    @IBOutlet weak var ShareBar_Datetime: UILabel!
    
    @IBOutlet weak var TextCont: UIView!
    @IBOutlet weak var PostText: KILabel!
    //@IBOutlet weak var TextHeight: NSLayoutConstraint!
    
    @IBOutlet weak var ShadowView: UIView!
    @IBOutlet weak var ContainerView: UIView!
    @IBOutlet weak var CellContainer: UIView!

    @IBOutlet weak var personalImage: UIImageView!
    @IBOutlet weak var personNameLbl: UILabel!
    @IBOutlet weak var ToImage: UIImageView!
    @IBOutlet weak var ToPersonName: UILabel!
    @IBOutlet weak var Datetime: UILabel!
    @IBOutlet weak var PostType: UIImageView!
    @IBOutlet weak var BodyView: UIView!
    var BodyHeight: NSLayoutConstraint!
    
    @IBOutlet weak var NumbsCont: UIView!
    @IBOutlet weak var CommentsLbl: UILabel!
    @IBOutlet weak var LikesLbl: UILabel!
    @IBOutlet weak var DislikesLbl: UILabel!

    
    @IBOutlet weak var LikeCont: UIView!
    @IBOutlet weak var YesLbl: UILabel!
    @IBOutlet weak var LikeImage: UIImageView!
    @IBOutlet weak var DislikeCont: UIView!
    @IBOutlet weak var NoLbl: UILabel!
    @IBOutlet weak var DislikeImage: UIImageView!
    @IBOutlet weak var shareCont: UIView!
    @IBOutlet weak var ShareImage: UIImageView!
    
    
    var FeedData:ModFeedFeedobj!
    
    var Parent:VcMasterNewsFeed!
    
    var _IsFashionPost = false
    
    var IsFashionPost:Bool
    {
        set
        {
            PostType.image = newValue ? UIImage(named: "Main-Wall-(hanger-icon)") : UIImage(named: "Main-Wall-(earth-icon)")
            _IsFashionPost = newValue
        }
        get{return  _IsFashionPost}
    }
    
    var _Liked:Bool = false
    var Liked:Bool
    {
        set
        {
            LikeImage.image = newValue ? UIImage(named: "Offers-(Like)") : UIImage(named: "Offers-(Like)-unselected")
            YesLbl.textColor = newValue ? UIColor(hex:"b379e7") : UIColor.lightGrayColor()
            _Liked = newValue
            let mainfeed = FeedData.SharedPost ?? FeedData
            mainfeed?.Liked = newValue
        }
        get{return _Liked}
    }
    var _Disliked:Bool = false
    var Disliked:Bool
    {
        set
        {
            DislikeImage.image = newValue ? UIImage(named: "Offers-(DisLike)") : UIImage(named: "Offers-(DisLike)-unselected")
            NoLbl.textColor = newValue ? UIColor(hex:"b379e7") : UIColor.lightGrayColor()
            _Disliked = newValue
            (FeedData.SharedPost ?? FeedData)?.Disliked = newValue
        }
        get{return _Disliked}
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        BodyHeight = NSLayoutConstraint(item: BodyView, attribute: .Height
             , relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
        BodyView.addConstraint(BodyHeight)
        
        ShareBarMaxHeight = ShareBarHeight.constant;
        
        personalImage.layer.cornerRadius = personalImage.frame.width/2;
        personalImage.layer.borderWidth = 0
        personalImage.clipsToBounds = true
        
        // radus
        ContainerView.layer.cornerRadius = 3;
        ContainerView.clipsToBounds = true;
        
        LikeCont.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.Like)));
        DislikeCont.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.Dislike)))
        shareCont.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.Share)))
        
        personalImage.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.toPostOwnerProfile)))
        personNameLbl.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.toPostOwnerProfile)))
        ToPersonName.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.toPostWallProfile)))
        PostOwnerName.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.toPostOwnerProfile)))
        
        SharerName.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.toPostSharerProfile)))
        LikesLbl.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.toLikers)))
        DislikesLbl.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.toDisLikers)))
        CommentsLbl.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(TcNewsFeed.CommentsClicked)))
        
    }
    
    func CommentsClicked()
    {
        let commentsController = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .All
        commentsController.InitMod(0, ObjId: (FeedData.Id?.integerValue)!, PostType: (FeedData.PostType?.integerValue)!,isItPrivate : FeedData.CommentPrivacy)
        
        Parent.parent.navigationController?.pushViewController(commentsController, animated: true)
        commentsController.Commentblock = { (id) in
          
            VcMasterNewsFeed.RefrehFeed(self.Parent, PostID: id,feedmaster: self.Parent , callback: { (feed) in })
        }
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        // shadow
        if Parent.Mode != FeedMode.SinglePost
        {
            Styles.Shadow(self, target: ContainerView, Radus: 1,shadowview: ShadowView)
        }
        else
        {
            Parent.FeedsTable.scrollEnabled = false
            CellContainer.backgroundColor = UIColor.whiteColor()
            
         //   ShadowView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
         //   ShadowView.backgroundColor = UIColor.redColor()
//            ContainerView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)//
//            ContainerView.backgroundColor = UIColor.greenColor()
            
        }
    }
   
    func toPostWallProfile()
    {
        var mainFeed = FeedData
        if(FeedData.SharedPost != nil)
        {
            mainFeed = FeedData.SharedPost!
        }
        
        let profile = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        profile.SetupForTimeLine((mainFeed.ToUserProfile?.Id?.stringValue)!)
        Parent.parent.navigationController?.pushViewController(profile, animated: true)
    }
    // takes you to the profile of the post owner
    func toPostOwnerProfile()
    {
        var mainFeed = FeedData
        if(FeedData.SharedPost != nil)
        {
            mainFeed = FeedData.SharedPost!
        }
        
        let profile = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        profile.SetupForTimeLine((mainFeed.UserProfile?.Id?.stringValue)!)
        Parent.parent.navigationController?.pushViewController(profile, animated: true)
    }
    // takes you to the profile of the post sharer
    func toPostSharerProfile()
    {
        let profile = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        profile.SetupForTimeLine((FeedData.UserProfile?.Id?.stringValue)!)
        Parent.parent.navigationController?.pushViewController(profile, animated: true)
    }
    func toLikers()
    {
//        let likers = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
//        likers.LikersMod(0, LikeType: true, ObjId: (FeedData.Id?.integerValue)!)
//        likers.title = "Likers"
//        Parent.parent.navigationController?.pushViewController(likers, animated: true)
        
        let commentsController = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .Likers
        commentsController.InitMod( 0, ObjId: (FeedData.Id?.integerValue)!, PostType: (FeedData.PostType?.integerValue)!, isItPrivate : FeedData.CommentPrivacy)
        
        Parent.parent.navigationController?.pushViewController(commentsController, animated: true)
        
    }
    func toDisLikers()
    {
//        let likers = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
//        likers.LikersMod(0, LikeType: false, ObjId: (FeedData.Id?.integerValue)!)
//        likers.title = "Dislikers"
//        Parent.parent.navigationController?.pushViewController(likers, animated: true)
//        
        let commentsController = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
         commentsController.MyScreenMod = .DisLikers
        commentsController.InitMod( 0, ObjId: (FeedData.Id?.integerValue)!, PostType: (FeedData.PostType?.integerValue)!,isItPrivate : FeedData.CommentPrivacy)
        
        Parent.parent.navigationController?.pushViewController(commentsController, animated: true)
    }
    
    @IBAction func OpsionsClicked(sender: AnyObject)
    {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let Edit = UIAlertAction(title: "Edit Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            let editscreen = self.Parent.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost
            editscreen.modFeedFeedobj = self.FeedData;
            if self.Parent.Mode == .GroupPosts {
             editscreen.isItPrivate = self.Parent.ParentGroup.privacy // if group private then all posts private by default
            }
            editscreen.FromScreen = .EditPost
            editscreen.Editblock = { ()in
                  VcMasterNewsFeed.RefrehFeed(self.Parent, PostID: self.FeedData.Id!.integerValue,feedmaster: self.Parent , callback: { (feed) in })
//                self.Parent?.parent?.navigationController?.popToViewController(self.Parent, animated: true)
            }
            self.Parent?.parent?.navigationController?.pushViewController(editscreen, animated: true)
         
        })
        
        let copy = UIAlertAction(title: "Copy Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let pb = UIPasteboard.generalPasteboard()
            
            if self.FeedData.SharedPost == nil {
                pb.string = self.FeedData.Text!
            }
            else  {
                pb.string = self.FeedData.SharedPost?.Text!
            }
            
            self.Parent.view.makeToast("Copied.")
        })
        let save = UIAlertAction(title: "Save Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            TcNewsFeed.SavePost(self, data: self.FeedData, completion: { (success) in
            })
        })
        
        let delete = UIAlertAction(title: "Delete Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let alert = UIAlertController(title: "Confirm", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            let ok = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Default , handler:
                {(action: UIAlertAction) -> Void in
                    TcNewsFeed.DeletePost(self.Parent, for: self.FeedData, completion: { (success) in
                        if success
                        {self.Parent.HidePost(self.FeedData)}
                    })
            })
            let cancel = UIAlertAction(title: "Cancel", style: .Default, handler: {(action: UIAlertAction) -> Void in
                alert.dismissViewControllerAnimated(true, completion: { _ in })
            })
            alert.addAction(ok)
            alert.addAction(cancel)
            self.Parent.presentViewController(alert, animated: true, completion: { _ in })
            
        })
        
        let UnsavePost = UIAlertAction(title: "UnSave Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            self.UnSavePost()
        })

        let postStatistic = UIAlertAction(title: "Post Result", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
           
            let statistic = self.Parent.storyboard?.instantiateViewControllerWithIdentifier("VcPostStatistics") as! VcPostStatistics
            if self.Parent.Mode == FeedMode.GroupPosts
            {
                statistic.come = .group
                statistic.groupId = Int(self.Parent.GroupID!)!
                statistic.postId = (self.FeedData.Id?.integerValue)!
                
            }
            else{
                 statistic.postId = (self.FeedData.Id?.integerValue)!
               statistic.come = .post
            }
            
    
            self.Parent.parent!.navigationController?.pushViewController(statistic, animated: true)
            
        })
        
        let Report = UIAlertAction(title: "Report Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let report = self.Parent.storyboard?.instantiateViewControllerWithIdentifier("VcReport") as! VcReport
            report.come = .Post
            report.feed = self.FeedData
            self.Parent.parent!.navigationController?.pushViewController(report, animated: true)
        })
        
        let Hide = UIAlertAction(title: "Hide Post", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            TcNewsFeed.HidePost_showModeratorPasswordAlert(self.Parent, for: self.FeedData, completion: { (success) in
                self.Parent.HidePost(self.FeedData)})
            
        })
        let cancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        if Parent.Mode == FeedMode.SavedPosts
        {
            optionMenu.addAction(UnsavePost);
        }
        
        if FeedData.UserProfile?.Id == User?.Id && FeedData.SharedPost == nil
        {
            optionMenu.addAction(Edit)
        }
        optionMenu.addAction(copy)
        optionMenu.addAction(save)
        
        
        
        if(FeedData.UserProfile?.Id == User?.Id)
        {
            if self.Parent.Mode != FeedMode.GroupPosts
            {
                optionMenu.addAction(postStatistic)
            }
            optionMenu.addAction(delete)
           
        }
        else
        {
            optionMenu.addAction(Report)
        }
        if self.Parent.Mode == FeedMode.GroupPosts
        {
            
            if(FeedData.UserProfile?.Id == User?.Id || Parent.ParentGroup.isAdmin)
            {
                optionMenu.addAction(postStatistic)
            }
        }
        
        if(User!.AccountType == AccountType.Moderator.rawValue)
        { optionMenu.addAction(Hide)}
        optionMenu.addAction(cancel)
        
        Parent.presentViewController(optionMenu, animated: true, completion: nil)
    }
    func UnSavePost()
    {
        let RequestParameters : NSDictionary =
            [
                "PostId":FeedData.Id!,
                "UserId":User!.Id!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.RemoveFromMySavedPosts, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.Parent.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.Parent.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let FeedsResponse = ModResponse(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            // error
                        }
                        else
                        {
                            
                            self.Parent.HidePost(self.FeedData)
                            self.Parent.view.makeToast("Post Unsaved")
                        }
                    }
                }
                
            }
            , callbackDictionary: nil)
        
    }
    static func DeletePost(target:UIViewController,for feed: ModFeedFeedobj,completion:((success:Bool)->())?)
    {
        MBProgressHUD.showHUDAddedTo(target.view, animated: true).label.text = "Deleting..."
        let RequestParameters : NSDictionary =
            [
                "LevelId":0,
                "PostId":feed.Id!,
                "UserId":User!.Id!
        ]
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.DeletePostandCommment, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                MBProgressHUD.hideHUDForView(target.view, animated: true)
                var success = false;
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            target.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            target.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let FeedsResponse = ModResponse(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            // error
                            target.view.makeToast("Couldnt delete post")
                        }
                        else
                        {
                            success = true
                            target.view.makeToast("Post deleted")
                            
                        }
                    }
                }
                if completion != nil
                {completion!(success: success)}
            }
            , callbackDictionary: nil)
    }
    static func HidePost_showModeratorPasswordAlert(target:UIViewController,for feed: ModFeedFeedobj,completion:((success:Bool)->())?)
    {
        let alert = UIAlertController(title: "Password required", message: "Please enter your password", preferredStyle: UIAlertControllerStyle.Alert)
        let ok = UIAlertAction(title: "hide", style: UIAlertActionStyle.Default , handler: {(action: UIAlertAction) -> Void in
            let passwordTextField = alert.textFields![0]
            if passwordTextField.text!.characters.count == 0
            {
                let alertView = UIAlertView(title: "Password Required", message: "Please enter your password to hide post", delegate: nil, cancelButtonTitle: "Ok", otherButtonTitles: "")
                alertView.show()
            }
            else
            {
                TcNewsFeed.HidePost(target, withPassword: passwordTextField.text!, data: feed, completion: { (success) in
                    if completion != nil
                    {completion!(success: success)}
                })
            }
        })
        let cancel = UIAlertAction(title: "Cancel", style: .Default, handler: {(action: UIAlertAction) -> Void in
            alert.dismissViewControllerAnimated(true, completion: { _ in })
        })
        alert.addAction(ok)
        alert.addAction(cancel)
        alert.addTextFieldWithConfigurationHandler({(textField: UITextField) -> Void in
            textField.placeholder = "Password"
            textField.secureTextEntry = true
        })
        
        target.presentViewController(alert, animated: true, completion: { _ in })
    }
    static func HidePost(target:UIViewController, withPassword:String, data:ModFeedFeedobj,completion:((success:Bool)->())?)
    {
        MBProgressHUD.showHUDAddedTo(target.view, animated: true).label.text = "Hiding..."
        
        let RequestParameters : NSDictionary =
            [
                "LevelId":0,
                "ModeratorPassword":withPassword,
                "PostId":data.Id!,
                "UserId":User!.Id!
        ]
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.HidePostorComment, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                MBProgressHUD.hideHUDForView(target.view, animated: true)
                var success = false
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            target.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            target.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let FeedsResponse = ModResponse(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            // error
                            target.view.makeToast("Couldnt hide post")
                        }
                        else
                        {
                            success = true
                            
                            target.view.makeToast("Post Hidden")
                            
                        }
                    }
                }
                if completion != nil
                {completion!(success: success)}
             
            }
            , callbackDictionary: nil)
    }
    
    func Like()
    {
        LikeDislike(true)
    }
    func Dislike()
    {
       LikeDislike(false)
    }
    var likeing = false
    var Dislikeing = false
    func LikeDislike(like:Bool)
    {
        if Dislikeing || likeing
        {return}
        
        if (like)
        {
            likeing = true
        }
        else
        {
            Dislikeing = true
        }
        let RequestParameters : NSDictionary =
            [
                "LevelId":0,
                "LikeType":like,
                "LikeValue":like ? !Liked : !Disliked,
                "PostId":FeedData.Id!,
                "UserId":User!.Id!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.LikeDislikePost, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.Parent.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.Parent.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let FeedsResponse = ModResponse(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            // error
                        }
                        else
                        {
                            if self.likeing
                            {
                                self.Liked = !self.Liked
                                self.FeedData.NoOfLikes! = self.FeedData.NoOfLikes!.integerValue + (self.Liked ? 1 : -1)
                                self.LikesLbl.text = "\(self.FeedData.NoOfLikes!) Yes"
                                
                                if self.Liked && self.Disliked
                                {
                                    self.Disliked = false;
                                    self.FeedData.NoOfDislikes! = self.FeedData.NoOfDislikes!.integerValue-1
                                    self.DislikesLbl.text = "\(self.FeedData.NoOfDislikes!) No"
                                }
                            }
                            else if self.Dislikeing
                            {
                                self.Disliked = !self.Disliked
                                self.FeedData.NoOfDislikes! = self.FeedData.NoOfDislikes!.integerValue + (self.Disliked ? 1 : -1)
                                self.DislikesLbl.text = "\(self.FeedData.NoOfDislikes!) No"
                                
                                if self.Liked && self.Disliked
                                {
                                    self.Liked = false;
                                    self.FeedData.NoOfLikes! = self.FeedData.NoOfLikes!.integerValue-1
                                    self.LikesLbl.text = "\(self.FeedData.NoOfLikes!) Yes"
                                }
                            }

                        }
                    }
                }
                
                self.Dislikeing = false
                self.likeing = false
            }
            , callbackDictionary: nil)
    }
    static func SavePost(target:UIView,data:ModFeedFeedobj,completion:((success:Bool)->())?)
    {
        MBProgressHUD.showHUDAddedTo(target, animated: true).label.text = "Saving..."
        let RequestParameters : NSDictionary =
            [
                "PostId":data.Id!,
                "UserId":User!.Id!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.AddToMySavedPost, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                MBProgressHUD.hideHUDForView(target, animated: true)
                var success = false;
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            target.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            target.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let FeedsResponse = ModResponse(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            // error
                        }
                        else
                        {
                            success = true;
                          target.makeToast("Post Saved")
                           
                        }
                    }
                    
                    
                }
                
                if completion != nil
                {completion!(success: success)}
                
                
            }
            , callbackDictionary: nil)
    }
    
    func Share()
    {
        let alert = UIAlertController(title: "Confirm", message: "Are you sure you want to share", preferredStyle: UIAlertControllerStyle.Alert)
        let ok = UIAlertAction(title: "Share", style: UIAlertActionStyle.Default , handler:
            {(action: UIAlertAction) -> Void in
                print("clicked share")
                TvCreatePost.SharePost(self.FeedData,PostToType:(self.Parent.Mode == .GroupPosts ? 2 : 1)) { (IsSuccess, Description) in
                    
                    self.Parent.view.makeToast(Description)
                    if (IsSuccess)
                    {
                        self.Parent.PagesCount = 1
                        self.Parent.GetNewsFeeds(self.Parent.PagesCount, FromPagerOrRefresher: false)
                    }
                    
                }
                
        })
        let cancel = UIAlertAction(title: "Cancel", style: .Default, handler: {(action: UIAlertAction) -> Void in
            alert.dismissViewControllerAnimated(true, completion: { _ in })
        })
        alert.addAction(ok)
        alert.addAction(cancel)
        Parent.presentViewController(alert, animated: true, completion: { _ in })
        
    }
   
    func PlayVideo()
    {
        var mainFeed = FeedData
        if(FeedData.SharedPost != nil)
        {
            mainFeed = FeedData.SharedPost!
        }
        
        let av = AVPlayerViewController()//"https://youtu.be/zQciMy_Pr0Q"
        //let url = NSURL(string: "http://www.ebookfrenzy.com/ios_book/movie/movie.mov")
        let url = NSURL(string: mainFeed.AttachedFiles![0].URL!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
        let player = AVPlayer(URL: url!)
        av.player = player
        
        Parent.presentViewController(av, animated: true, completion: nil)
        player.play()
    }
    func OpenGallery()
    {
        var mainFeed = FeedData
        if(FeedData.SharedPost != nil)
        {
            mainFeed = FeedData.SharedPost!
        }
        NewsFeedHelper.imageViewer(mainFeed.AttachedFiles!,caption: mainFeed.Text!, presnter: Parent, animatefrom: BodyView)
        
    }
    var url=""
    func LinkPreviewClick()
    {
       AppDelegate.attemptOpenURL(url)
    }
    func OpenFasionPost()
    {
        var mainFeed = FeedData
        if(FeedData.SharedPost != nil)
        {
            mainFeed = FeedData.SharedPost!
        }
        let fasionPostView = Parent.storyboard?.instantiateViewControllerWithIdentifier("TvFashionPost") as! TvFashionPost
        fasionPostView.mainFeed = mainFeed
        fasionPostView.Parent = Parent;
        fasionPostView.willExit = { () in
            
            self.Liked = mainFeed.Liked
            self.Disliked = mainFeed.Disliked
            
            self.LikesLbl.text = "\(self.FeedData.NoOfLikes!) Yes"
            
            self.DislikesLbl.text = "\(self.FeedData.NoOfDislikes!) No"
          
        }
        
        Parent?.parent?.navigationController?.pushViewController(fasionPostView, animated: true)
    }
    func SetData(parentController:VcMasterNewsFeed,feed: ModFeedFeedobj)
    {
        FeedData = feed
        Parent = parentController
        for var subvivew in BodyView.subviews
        {
            subvivew.removeFromSuperview()
        }
        BodyHeight.constant = 0;
        
        var shared = false;
        var mainFeed = feed
        
        if(feed.SharedPost != nil)
        {
            ShareBarHeight.constant = ShareBarMaxHeight;
            mainFeed = feed.SharedPost!
            shared = true
            
            SharerName.text = feed.UserProfile?.FullName
            PostOwnerName.text = mainFeed.UserProfile?.FullName
            ShareBar_Datetime.text = feed.Time
        }
        else
        {
            //ShareBar.hidden = true
            ShareBarHeight.constant = 0
        }
        personNameLbl.text = mainFeed.UserProfile?.FullName
        if mainFeed.ToUserProfile != nil && mainFeed.ToUserProfile?.Id != mainFeed.UserProfile?.Id
        {
            ToImage.hidden = false
            ToPersonName.text = mainFeed.ToUserProfile!.FullName
        }
        else
        {
             ToImage.hidden = true
             ToPersonName.text = ""
        }
        personalImage.kf_setImageWithURL(NSURL(string: (mainFeed.UserProfile?.ProfilePicture!)!));
        Datetime.text = mainFeed.Time!+"."
        IsFashionPost = mainFeed.PostType == 2
        Liked = feed.Liked
        Disliked = feed.Disliked
        
        CommentsLbl.text = "\(feed.NoOfComments!) Comment"
        LikesLbl.text = "\(feed.NoOfLikes!) Yes"
        DislikesLbl.text = "\(feed.NoOfDislikes!) No"
        // SharesLbl.text = "\(0) Share"
        
        BodyView.gestureRecognizers?.removeAll()
        TextCont.gestureRecognizers?.removeAll()
        if IsFashionPost
        {
            BodyView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OpenFasionPost)))
            TextCont.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OpenFasionPost)))

        }
//      personalImage.kf_setImageWithURL(NSURL(string: (mainFeed.UserProfile?.ProfilePicture) ?? ""))
        let piURL = mainFeed.UserProfile?.ProfilePicture == nil ? nil : NSURL(string: (mainFeed.UserProfile?.ProfilePicture!)!)
        personalImage.kf_setImageWithURL(piURL,placeholderImage: UIImage(named: "Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        
        if(mainFeed.Text?.characters.count > 0)
        { 
            //"long text sample long text sample long text sample long text sample long text sample "
          
            PostText.automaticLinkDetectionEnabled = true
            //  PostText.tag
            let labelText = mainFeed.Text
            if mainFeed.TaggedUsers != nil
            {
//                for  userinfo:ModFeedUserInfo in mainFeed.TaggedUsers!
//                {
//                    let replacedText = "(@\(userinfo.UserName))\(userinfo.FullName)"
//                    let tagText = "@\(userinfo.UserName)"
//                    labelText = labelText?.stringByReplacingOccurrencesOfString(tagText, withString: replacedText)
//                }
            }
            PostText.text = labelText;
            PostText.linkDetectionTypes = [KILinkTypeOption.Hashtag , .URL, .UserHandle]
            // PostText.setAttributes([atrib], forLinkType: .Hashtag)
            PostText.userHandleLinkTapHandler = {(label:KILabel,string:String,range:NSRange) in
                
                var selectedUser: ModFeedUserInfo? = nil
                for user: ModFeedUserInfo in mainFeed.TaggedUsers!
                {
                    if string.containsString(user.UserName!)//string.containsString(user.FullName!)
                    {
                        selectedUser = user
                    }
                }
                if selectedUser != nil
                {
                   // self.Parent.view.makeToast("hello \(selectedUser?.FullName)")
                    
//                    let searchMasterViewController = self.Parent.storyboard!.instantiateViewControllerWithIdentifier("VcSearch") as! VcSearch
//                    searchMasterViewController.initWith((selectedUser?.FullName)!, posts: false)
//                    self.Parent.parent.navigationController!.pushViewController(searchMasterViewController, animated: true)
                    
                    let profile = self.Parent.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
                    profile.SetupForTimeLine((selectedUser!.Id?.stringValue)!)
                    self.Parent.parent.navigationController?.pushViewController(profile, animated: true)
                }
            }
            
            PostText.hashtagLinkTapHandler = {(label: KILabel, string: String, range: NSRange) -> Void in
                let searchMasterViewController = self.Parent.storyboard!.instantiateViewControllerWithIdentifier("VcSearch") as! VcSearch
                searchMasterViewController.initWith(string, posts: true)
                self.Parent.parent.navigationController!.pushViewController(searchMasterViewController, animated: true)
//                 self.Parent.view.makeToast("hash tag \(string)")
            }
            PostText.urlLinkTapHandler = {(label: KILabel, string: String, range: NSRange) -> Void in
                // Open URLs
                AppDelegate.attemptOpenURL(string)
                
            }
            if (mainFeed.PostCType == PostContentType.Text_emotions.rawValue)
            {
                BodyHeight.constant = 255
                let rect = CGRect(x: 0 , y: 0 , width:BodyView.frame.size.width , height: BodyHeight.constant)
                let linkView = UiLinkPreview.linkPreview(mainFeed,frame: rect)
                if linkView != nil
                {
                    linkView?.lblUrl.text
                    //    BodyHeight.constant = (linkView?.frame.height)!
                    BodyView.addSubview(linkView!)
                    url=linkView!.fullurl
                    if !IsFashionPost
                    {  linkView!.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(LinkPreviewClick)))
                    }
                }
                else {BodyHeight.constant = 0}
            }
        }
        else
        { PostText.text = "" }
        
        
        if (mainFeed.PostCType == PostContentType.Text_emotions.rawValue)
        {
            // i thinkk we dont realy need to write any thing here
        }
        else if mainFeed.PostCType == PostContentType.Text_Images.rawValue
        {
            if(mainFeed.AttachedFiles != nil)
            {
                BodyHeight.constant = 300
                let rect = CGRect(x: 0 , y: 0 , width:BodyView.frame.size.width , height: BodyHeight.constant)
                let multiimageview = MultiImageCellView.imagesView(forAttachedFiles: mainFeed.AttachedFiles!, withFrame: rect)
                
                BodyView.addSubview(multiimageview)
                if !IsFashionPost
                {
                    multiimageview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OpenGallery)))
                }
            }
        }
        else if mainFeed.PostCType == PostContentType.Text_Videos.rawValue
        {
            
            if mainFeed.AttachedFiles != nil
            {
                BodyHeight.constant = 200
                let bf = CGRect(x: 0 , y: 0 , width:BodyView.frame.size.width , height: BodyHeight.constant)
                
                let videoContainerView = VedioThumbnail.videoViewWithAttachedFile(mainFeed.AttachedFiles![0], frame: bf) { }
                BodyView.addSubview(videoContainerView)
                if !IsFashionPost
                {
                    videoContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PlayVideo)))
                }
            }
        }
        
    }
    
}
