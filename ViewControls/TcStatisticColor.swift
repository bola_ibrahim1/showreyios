//
//  TcCreatePostCell.swift
//  ShowRey
//
//  Created by M-Hashem on 11/3/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class TcStatisticColor: UITableViewCell {

    @IBOutlet weak var lblNoAns: UILabel!
    
    @IBOutlet weak var imgNoAns: UIImageView!
    @IBOutlet weak var imgNo: UIImageView!
    @IBOutlet weak var imgYes: UIImageView!
   
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var ShadeView: UIView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
       
    }

    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        // radus
        
        imgYes.layer.cornerRadius = 10.0
        imgNo.layer.cornerRadius = 10.0
        imgNoAns.layer.cornerRadius = 10.0
        
        container.layer.cornerRadius = 3;
        container.clipsToBounds = true;
        // shadow
        
        Styles.Shadow(self, target: container, Radus: 1,shadowview: ShadeView)
    }
    
}
