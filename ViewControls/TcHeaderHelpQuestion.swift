//
//  TcHeaderHelpQuestion.swift
//  ShowRey
//
//  Created by Appsinnovate on 1/28/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit

class TcHeaderHelpQuestion: UITableViewCell {

    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var headerQuestion: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCell(question:String){
        headerQuestion.text = question
    }
}
