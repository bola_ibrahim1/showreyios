//
//  VcTerms.swift
//  ShowRey
//
//  Created by User on 11/8/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class VcTerms: UIViewController {
    
    var terms:Int!
    var about:Bool = false

    @IBOutlet weak var txtView: UITextView!
    
    @IBOutlet weak var lblType: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if terms == 1{
            
            txtView.text = AppConfiguration?.ConfigString.Help
             self.title = "Help"
        }

        if terms == 2{
             txtView.text = AppConfiguration?.ConfigString.TermandCond
            self.title = "Terms & Conditions"
        }else if terms == 3{
            
            txtView.text = AppConfiguration?.ConfigString.PrivacyPolicy
            self.title = "Privacy Policy"
        }
        
        
       txtView.scrollRangeToVisible(NSMakeRange(0, 1))
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        txtView.setContentOffset(CGPointZero, animated: false)
       // txtView.scrollRangeToVisible(NSMakeRange(0, 1))
         self.automaticallyAdjustsScrollViewInsets = false
    }
    override func viewWillAppear(animated: Bool) {
       // self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    override func viewWillDisappear(animated: Bool) {
        
        //    self.navigationController?.setNavigationBarHidden(false, animated: true)
       
    }

    
    
    @IBAction func exitAction(sender: AnyObject) {
        
        navigationController!.popViewControllerAnimated(true)
        
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
