//
//  VTapItem.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 10/10/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class VTapItem: UIView,ImageTab
{
    
    @IBOutlet weak var NotificationCon: UIView!
    
    @IBOutlet weak var NotificationsNum: UILabel!
    @IBOutlet weak var Icon: UIImageView!
    
    //    var Icon: UIImageView!
    //    {
    //        get{return icon}
    //    }
    func setNum(num:Int)
    {
        UIApplication.sharedApplication().registerUserNotificationSettings(UIUserNotificationSettings(forTypes:
            UIUserNotificationType.Badge, categories: nil
            ))
        UIApplication.sharedApplication().applicationIconBadgeNumber = num
        if(num == 0)
        {
            NotificationCon.hidden = true;
        }
        else
        {
            var num_ = num
            if num > 999 {num_ = 999}
            NotificationCon.hidden = false;
            NotificationsNum.text = String(num_);
        }
    }    
    
    
}
