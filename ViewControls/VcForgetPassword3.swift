//
//  VcForgetPassword3.swift
//  ShowRey
//
//  Created by Appsinnovate on 10/2/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import PureLayout
import EGFloatingTextField
import MBProgressHUD
import SwiftyJSON

class VcForgetPassword3: UIViewController {
    
    @IBOutlet weak var LblErrorConfirmPass: UILabel!
    @IBOutlet weak var TxtConfirmPass:  EGFloatingTextField!
    @IBOutlet weak var LblErrorNewPass: UILabel!
    @IBOutlet weak var TxtNewPass:  EGFloatingTextField!
    var email : String = ""
    override func viewDidLoad() {
       super.viewDidLoad()
        self.navigationItem.title = "Forget password"
        
        TxtNewPass.floatingLabel = true
        TxtNewPass.setPlaceHolder(" New Password")
        TxtNewPass.validationType = .Password
        TxtNewPass.errorLabel = LblErrorNewPass
        TxtNewPass.customerrorMessage = "Invalid password"
        
        TxtConfirmPass.floatingLabel = true
        TxtConfirmPass.setPlaceHolder(" Confirm Password")
        TxtConfirmPass.validationType = .ConfirmPassword
        TxtConfirmPass.errorLabel = LblErrorConfirmPass
        TxtConfirmPass.customerrorMessage = "Enter the same password"
        
        
    

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BtnConfimPass3(sender: AnyObject) {
        let RequestParameters : NSDictionary = [
            "NewPassword" : TxtNewPass.text!,
            "Email" : email
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        NetworkHelper.RequestHelper(nil, service: WSMethods.ForgetPasswordStp3, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: jsonCallBack2)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
}
    func jsonCallBack2(response: (JSON: JSON, NSError: NSError?))
    {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    self.view.makeToast("No Internet connection")
                }
                else
                {
                    self.view.makeToast("An error has been occurred")
                    print("error")
                }
                return
            }
        }
        else
        {
            let resultResponse = response.JSON["ResultResponse"].stringValue
            if( resultResponse == "0"){
                
                let thirdView = self.storyboard?.instantiateViewControllerWithIdentifier("VcLogin") as! VcLogin
                self.navigationController?.pushViewController(thirdView, animated: true)
                print("Success")
            }else{
                self.view.makeToast("An error has been occurred")
            }
            
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
