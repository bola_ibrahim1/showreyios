//
//  UiLinkPreview.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/2/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class UiLinkPreview: UIView {
    
    @IBOutlet weak var imgLinkPreview: UIImageView!

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDescription: UILabel!
    
    @IBOutlet weak var lblUrl: UILabel!
    var fullurl = ""
    
    ///////////////////StaticMethodLinkPreview//////////////
    
    static func linkPreview (x: ModFeedFeedobj! =  nil , modCommentsFollowersobj: ModCommentsFollowersobj! =  nil ,prameterNumber:Int = 1 ,frame:CGRect)-> UiLinkPreview?
    {
      
      let linkPreviewData = UiLinkPreview.fromNib("LinkPreview")
        linkPreviewData.frame = frame
        
         var isBool : Bool = false
        if (prameterNumber == 1 )
        {
        if x.LinkPreview != nil
        {
            UiLinkPreview.FillParsedData(linkPreviewData, data: x.LinkPreview!)
            return linkPreviewData
        }
        
       isBool = httparse.LinkView(x.Text!)
        { (httparse) in
            x.LinkPreview = httparse
            UiLinkPreview.FillParsedData(linkPreviewData, data: httparse)
          
        }
            
        }
        else
        {
            if modCommentsFollowersobj.LinkPreview != nil
            {
                UiLinkPreview.FillParsedData(linkPreviewData, data: modCommentsFollowersobj.LinkPreview!)
                return linkPreviewData
            }
            
            isBool = httparse.LinkView(modCommentsFollowersobj.Comment!)
            { (httparse) in
                modCommentsFollowersobj.LinkPreview = httparse
                UiLinkPreview.FillParsedData(linkPreviewData, data: httparse)
                
            }
        }
        if isBool == false {
            return nil
        }
        else
        {
           return linkPreviewData
        }        
    }
    static func FillParsedData(linView:UiLinkPreview,data:httparse)
    {
        
        linView.lblTitle.text =  data.HtmlTitle
        linView.lblDescription.text = data.HtmlDescription
        linView.imgLinkPreview.image = data.HtmlPhoto
        linView.lblUrl.text = data.HtmlUrl
        linView.fullurl = data.Fullurl;
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
