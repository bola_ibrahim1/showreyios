//
//  VcStackBar.swift
//  ShowRey
//
//  Created by User on 1/23/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit


class VcStackBar: UIViewController, HorizontalStackBarChartDataSource, HorizontalStackBarChartDelegate {

    
    let header_height = 65
    
    let arrcolor: [UIColor] = [UIColor(hex: "#6716B1"), UIColor(hex: "#A25EE1"), UIColor(hex: "#BDBDBD")]
    let arrValue: [NSNumber] = [6,2,2]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createHorizontalStackChart()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Mark: - CreateHorizontalChart
    
    func createHorizontalStackChart() {
        let chartView = HorizontalStackBarChart(frame: CGRectMake(0, CGFloat(header_height), self.view.frame.size.width, 80))
        chartView.dataSource = self
        chartView.delegate = self
        chartView.showLegend = false
        chartView.showValueOnBarSlice = false
        chartView.legendViewType = LegendTypeHorizontal
        chartView.showCustomMarkerView = true
        chartView.drawStackChart()
        self.view.addSubview(chartView)
    }
    
    //Mark: - HorizontalStackBarChartDataSource
    
    func numberOfValuesForStackChart() -> Int {
         return 3
    }
    
    func colorForValueInStackChartWithIndex(index: Int) -> UIColor! {
//        let aRedValue = arc4random() % 255
//        let aGreenValue = arc4random() % 255
//        let aBlueValue = arc4random() % 255
//        let randColor = UIColor(red: CGFloat(aRedValue)  / 255.0, green: CGFloat(aGreenValue) / 255.0, blue: CGFloat(aBlueValue) / 255.0, alpha: 1.0)
        return arrcolor[index]//randColor
    }
    
    func titleForValueInStackChartWithIndex(index: Int) -> String! {
        return ""
       // return "data \(Int(index))"
    }
    
    func valueInStackChartWithIndex(index: Int) -> NSNumber! {
        return arrValue[index]//Int(arc4random() % 100)
    }
    
    func customViewForStackChartTouchWithValue(value: NSNumber!) -> UIView! {
        let view = UIView()
        view.backgroundColor = UIColor.whiteColor()
        view.layer.cornerRadius = 4.0 //F
        view.layer.borderWidth = 1.0 //F
        view.layer.borderColor = UIColor.lightGrayColor().CGColor
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowRadius = 2.0 //F
        view.layer.shadowOpacity = 0.3 //F
        let label = UILabel()
        label.font = UIFont.systemFontOfSize(12)
        label.textAlignment = .Center
        label.text = "\(value.integerValue * 10) %"
        label.frame = CGRectMake(0, 0, 50, 30)
        view.addSubview(label)
        view.frame = label.frame
        return view
    }
    
    
    //Mark: - HorizontalStackBarChartDelegate
    func didTapOnHorizontalStackBarChartWithValue(value: String!) {
        print("Horizontal Stack Chart: \(value)")
    }
    
    //Mark: - CreateHorizontalChart
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
