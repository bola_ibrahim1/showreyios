//
//  VcTest.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 9/21/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD
import Toast_Swift
import SwiftyJSON
import AVKit
import AVFoundation
import Social
import MobileCoreServices


class VcTest: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    let imagePickerController = UIImagePickerController()
    var videoURL: NSURL? = NSURL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
    var av: AVPlayerViewController!
    
    lazy var thumb:UIImageView = {
        
        let imageView: UIImageView = UIImageView(frame: CGRect(x: 0 , y: (self.view.frame.size.height - 54 * 7.5) / 2.0, width: self.view.frame.size.width, height: 54 * 5))
        imageView.autoresizingMask = UIViewAutoresizing.FlexibleLeftMargin
        imageView.autoresizingMask = UIViewAutoresizing.FlexibleRightMargin
        imageView.hidden = false
        imageView.layer.masksToBounds = true
        imageView.layer.rasterizationScale = UIScreen.mainScreen().scale
        imageView.layer.shouldRasterize = true
        imageView.clipsToBounds = true
        imageView.image = UIImage(named: "back")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(imageTapped(_:)))
        imageView.userInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        
        return imageView
    }()
    
    
    @IBOutlet weak var shade: UIView!
    @IBOutlet weak var content: UIView!
    @IBOutlet weak var testlabel: UILabel!
    
    func imageTapped(img: AnyObject)
    {
        // image action
        
        print("img clicked")
        
        
        thumb.hidden = true
        
        let player = AVPlayer(URL: videoURL! as  NSURL)
        av = AVPlayerViewController()
        av.player = player
        av.view.frame =  CGRect(x: 0 , y: (self.view.frame.size.height - 54 * 7.5) / 2.0, width: self.view.frame.size.width, height: 54 * 5)
        self.addChildViewController(av)
        self.view.addSubview(av.view)
        av.didMoveToParentViewController(self)
        // av.player?.play()
        // AVCaptureResolvedPhotoSettings

        
        
    }
    
    
    override func viewDidLoad()
    {
        testlabel.text = "this is a longer label"
        testlabel.sizeToFit()
        super.viewDidLoad()

        content.layer.cornerRadius = 10
        
        content.clipsToBounds = true;
        shade.backgroundColor = UIColor.clearColor()
        Styles.Shadow(self.view, target: content, Radus: 10, shadowview: shade)
        // self.view.makeToast("This is a piece of toast")
        // Do any additional setup after loading the view.
        
        
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(animated: Bool) {
     
     //  self.view.addSubview(thumb)
        
    }

    
    override func viewWillDisappear(animated: Bool) {
        av.player = nil
    }
    
    
    @IBAction func pickVideo(sender: AnyObject) {
        
        imagePickerController.sourceType = .PhotoLibrary
        imagePickerController.delegate = self
        imagePickerController.mediaTypes = ["public.movie"] //"public.image",
        
        presentViewController(imagePickerController, animated: true, completion: nil)
        
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        videoURL = info["UIImagePickerControllerReferenceURL"] as? NSURL
        
        print(videoURL)
        
        imagePickerController.dismissViewControllerAnimated(true, completion: nil)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func dsad(sender: AnyObject) {
        
        let RequestParameters : NSDictionary = [
            "ProfileUserId" : 16,
            "UserId" : 16
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetProfileInformation, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
             , responseType: ResponseType.DictionaryJson , callbackString: jsonCallBack, callbackDictionary: jsonCallBack2)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
    }
 
    func jsonCallBack(response: (Response<String, NSError>))
    {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if(response.result.isFailure)
        {
            if let networkError = response.result.error {
                if (networkError.code == -1009) {
                    self.view.makeToast("No Internet connection")
                }
                else
                {
                    self.view.makeToast("An error has been occurred")
                    print("error")
                }
                return
            }
        }
        
        if(response.result.isSuccess)
        {  //self.view.makeToast("No Internet Connection")
            if let JSON = response.result.value {
               // print("JSON: \(JSON)")
                let Response =  ModProfileInformationResponse(json:JSON)
                
                if (Response.ResultResponse != "0"){
                    self.view.makeToast("An error has been occurred")
                }
                
                print(Response.Profileobj)
                print(Response.Description)
                print(Response.ResultResponse)
                
            }
        }
    }
    
    
    
    

    //////////// Not Its in case of string Dictionary *******************************
    func jsonCallBack2(response: (JSON: JSON, NSError: NSError?))
    {
      MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    self.view.makeToast("No Internet connection")
                }
                else
                {
                    self.view.makeToast("An error has been occurred")
                    print("error")
                }
                return
            }
        }
        else
        {
        
        
        let userName = response.JSON["ResultResponse"].stringValue
        }
        
        
        
         }


}
