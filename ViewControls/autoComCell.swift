//
//  autoComCell.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 10/31/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MJAutoComplete

class autoComCell: MJAutoCompleteCell {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var photoUser: UIImageView!
    @IBOutlet weak var FollowBtn: UIButton!
 
    var followtype : Bool = false
    var followerID : String = "-1";
    var context: [NSObject : AnyObject]!
    var parent:TvCreatePost!
    var parentComment:VcComments!
    var notChangeArr = [String]()
    ///////////////////SwiftMethodConverting////////////////////
    func CustomautoCompleteItem(autoCompleteItem: MJAutoCompleteItem) {
        
        super.autoCompleteItem = autoCompleteItem;
        self.textLabel!.hidden = true;
        
        context = autoCompleteItem.context
        
        followerID = (string:(context["Id"] as! String))
        
        
        FollowBtn.layer.cornerRadius = 15
        FollowBtn.layer.borderWidth = 1
        FollowBtn.layer.borderColor = UIColor(hex: "#b379e7").CGColor
        
        lblUserName.text = (string: (context["UserName"] as! String))
        
        lblFullName.text = (string: (context["FullName"] as! String))
        
        photoUser.layer.cornerRadius = photoUser.frame.size.width / 2;
        photoUser.clipsToBounds = true;
        
        
        let url = NSURL(string:(context["ProfilePicture"] as! String))
        
        photoUser.kf_setImageWithURL(url,placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        
        if parent != nil {
            
            if parent.enterAutoComplete == false{
                
                if notChangeArr.count != 0 {
                    
                    if notChangeArr.contains(followerID) {
                        
                        return
                    }
                }
            }else {
                
                notChangeArr.removeAll()
                
                if ( (string:(context["FollowStatus"] as! String)) == "1" ) //followed
                {
                    followtype = false // unfollow
                    FollowBtn.setTitle("Followed", forState: .Normal)
                    FollowBtn.setTitleColor(UIColor(hex: "#ffffff"), forState: UIControlState.Normal)
                    FollowBtn.backgroundColor = UIColor(hex: "#b379e7")
                }
                else if ( (string:(context["FollowStatus"] as! String)) == "0" ) //Follow +
                {
                    followtype = true // follow
                    FollowBtn.setTitle("Follow +", forState: .Normal)
                    
                    FollowBtn.setTitleColor(UIColor(hex: "#b379e7"), forState: UIControlState.Normal)
                    FollowBtn.backgroundColor = UIColor(hex: "#ffffff")
                }
                else if ((string:(context["FollowStatus"] as! String)) == "2" ) //other +
                {
                    FollowBtn.hidden = true
                }
                
                
            }

            
        }else if parentComment != nil {
            if parentComment.enterAutoComplete == false{
                
                if notChangeArr.count != 0 {
                    
                    if notChangeArr.contains(followerID) {
                        
                        return
                    }
                }
            }else {
                
                notChangeArr.removeAll()
                
                if ( (string:(context["FollowStatus"] as! String)) == "1" ) //followed
                {
                    followtype = false // unfollow
                    FollowBtn.setTitle("Followed", forState: .Normal)
                    FollowBtn.setTitleColor(UIColor(hex: "#ffffff"), forState: UIControlState.Normal)
                    FollowBtn.backgroundColor = UIColor(hex: "#b379e7")
                }
                else if ( (string:(context["FollowStatus"] as! String)) == "0" ) //Follow +
                {
                    followtype = true // follow
                    FollowBtn.setTitle("Follow +", forState: .Normal)
                    
                    FollowBtn.setTitleColor(UIColor(hex: "#b379e7"), forState: UIControlState.Normal)
                    FollowBtn.backgroundColor = UIColor(hex: "#ffffff")
                }
                else if ((string:(context["FollowStatus"] as! String)) == "2" ) //other +
                {
                    FollowBtn.hidden = true
                }
                
                
            }

            
        }
        
        
        
       
        
    }
    
    
    @IBAction func FollowAction(sender: AnyObject) {
        
        var RequestParameters : NSDictionary
        if parent != nil {
            parent.enterAutoComplete = false
        }
        if parentComment != nil {
            parentComment.enterAutoComplete = false
        }
        notChangeArr.append(followerID)
        
        if ( followtype == false) //followed
        {
            
            FollowBtn.setTitle("Follow +", forState: .Normal)
            FollowBtn.setTitleColor(UIColor(hex: "#b379e7"), forState: UIControlState.Normal)
            FollowBtn.setTitleColor(UIColor(hex: "#b379e7"), forState: UIControlState.Highlighted)
            FollowBtn.backgroundColor = UIColor(hex: "#ffffff")
            context["FollowStatus"] = NSNumber(int: 0)
            RequestParameters =
                [   "UserId":User!.Id!,
                    "FollowType":false,
                    "FollowerId":followerID,
                    "FollowerType":1,
            ]
        }
        else //Follow +
        {
            FollowBtn.setTitle("Followed", forState: .Normal)
            FollowBtn.setTitleColor(UIColor(hex: "#ffffff"), forState: UIControlState.Normal)
            FollowBtn.setTitleColor(UIColor(hex: "#ffffff"), forState: UIControlState.Highlighted)
            FollowBtn.backgroundColor = UIColor(hex: "#b379e7")
            context["FollowStatus"] = NSNumber(int: 1)
            RequestParameters =
                [   "UserId":User!.Id!,
                    "FollowType":true,
                    "FollowerId":followerID,
                    "FollowerType":1,
            ]
        }
        
        followtype = !followtype
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service:WSMethods.FollowUser, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in}, callbackDictionary: nil)
        
    }
    
  /*  - (void)setAutoCompleteItem:(MJAutoCompleteItem *)autoCompleteItem
    {
    // I bet you didn't know this was possible :p
    super.autoCompleteItem = autoCompleteItem;
    /* Superclass will set the text label to displayString, we don't want that. */
    self.textLabel.hidden = YES;
    
//    self.UserImage.layer.cornerRadius = self.UserImage.frame.size.width/2;
//    self.UserImage.layer.borderWidth = 1.0;
//    self.UserImage.layer.borderColor = RED_UICOLOR.CGColor;
//    self.UserImage.clipsToBounds = YES;
//    
    NSDictionary *context = autoCompleteItem.context;
    
    
    if ([context objectForKey:@"UserName"] != nil )
    {
    lblUserName.text = [NSString stringWithString:[context objectForKey:@"UserName"]];
    
    
    
   
//    NSString *myfullurl = [NSString stringWithString:[context objectForKey:@"ProfilePicture"]];
//    self.UserImage.hidden = false;
//    
//    NSURL *url = [[NSURL alloc] initWithString:myfullurl];
    
    /*
     SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
     [downloader downloadImageWithURL:url
     options:0
     progress:^(NSInteger receivedSize, NSInteger expectedSize) {
     // progression tracking code
     }
     completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
     if (image && finished) {
     self.UserImage.image = image;
     
     }}];
     */
    
    }
    else
    {
    lblUserName.text = [NSString stringWithString:[context objectForKey:@"Name"]];
   // _LabFullName.text = [NSString stringWithString:[context objectForKey:@"Hashtag"]];
   // self.UserImage.hidden = true;
    
    
    }
    
    }*/

    
    
    
}
