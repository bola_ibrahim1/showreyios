//
//  SingletoneClass.swift
//  Dr_Greiche
//
//  Created by Ahmed Sayed on 4/24/16.
//  Copyright © 2016 Orchtech. All rights reserved.
//

import UIKit

class SingletoneClass: NSObject
{
    static let sharedInstance = SingletoneClass()
    var showMoreButton : UIButton!
}
