//
//  VcCommentsOnlyContainer.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 6/20/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit

class VcCommentsOnlyContainer: UIViewController {

    var ObjId = -1
    var PostType = -1
    var isWhisper = false
    var IsItSpecialOffer = false
    
    var firstTime:Bool = true
    var IsitReplay = false // for replay mod
    var ScreenMod = 0 //all 1 whisper
    var enterAutoComplete = true
    var isItPrivate: Bool = false
        var parentView: TvFashionPost!
    
    
    
    @IBOutlet weak var commentScroll: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        var All : VcComments = VcComments()
        let storyboard = AppDelegate.storyboard
        
        All = storyboard.instantiateViewControllerWithIdentifier("VcComments") as! VcComments
        All.ObjId = ObjId
        All.IsItSpecialOffer = IsItSpecialOffer
        All.isItPrivate = false
        

        
        All.PostType = PostType
        All.ScreenMod = 0
         All.parentViewforNotifcation = parentView
        
     
        commentScroll.contentSize = CGSizeMake(self.view.frame.size.width, (self.view.frame.size.height + 300))
        commentScroll.addSubview(All.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
