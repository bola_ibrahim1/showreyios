//
//  CcEditGroup.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/20/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class CcEditGroup: UICollectionViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBOutlet weak var profileName: UILabel!
    
}
