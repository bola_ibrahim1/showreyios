//
//  TvAboutUs.swift
//  ShowRey
//
//  Created by User on 11/9/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Kingfisher
import MessageUI

class TvAboutUs: UITableViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var imgDeveloper: UIImageView!
    @IBOutlet weak var txtDeveloper: UILabel!
    
    @IBOutlet weak var imgCompany: UIImageView!
  
    @IBOutlet weak var txtCompany: UILabel!

    @IBOutlet weak var sideMenu: UIButton!
    @IBOutlet weak var companyCall: UIButton!
    @IBOutlet weak var companyMail: UIButton!
    @IBOutlet weak var companySite: UIButton!
    @IBOutlet weak var devCall: UIButton!
    @IBOutlet weak var devMail: UIButton!
    @IBOutlet weak var devSite: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "About Us"
        
        navigationController!.navigationBar.barTintColor = UIColor(hex: "#b379e7")
       
        
        let button = UIButton()
        button.frame = CGRectMake(0, 0, 21, 31)
        button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
        button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
        let barButton = UIBarButtonItem()
        barButton.customView = button
        navigationItem.leftBarButtonItem = barButton
        
        companyCall.setImage(UIImage(named: "Group-8-showrey"), forState: .Highlighted)
        companyMail.setImage(UIImage(named: "Group-10-showrey"), forState: .Highlighted)
        companySite.setImage(UIImage(named: "Group-9-showrey"), forState: .Highlighted)
        devCall.setImage(UIImage(named: "Group-8-mobileznation"), forState: .Highlighted)
        devMail.setImage(UIImage(named: "Group-10-mobileznation"), forState: .Highlighted)
        devSite.setImage(UIImage(named: "Group-9-mobileznation"), forState: .Highlighted)
        
//        let backgroundImage = UIImage(named: "bg_showrey")
//        let imageView = UIImageView(image: backgroundImage)
//        self.tableView.backgroundView = imageView
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
       
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        //self.navigationController?.setNavigationBarHidden(true, animated: true)
        
    }
    
   
    
    
    
    var tablereloaded = false
    override func viewDidLayoutSubviews() {
        
        let url = NSURL(string: (AppConfiguration?.AboutDeveloper.Logo)!)
        let urlCom = NSURL(string: (AppConfiguration?.AboutCompany.Logo)!)
        
        imgDeveloper.kf_setImageWithURL(urlCom, placeholderImage: UIImage(named:"placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        
        imgCompany.kf_setImageWithURL(url, placeholderImage: UIImage(named:"placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
    
        txtDeveloper.text = AppConfiguration?.AboutCompany.Description
        txtDeveloper.sizeToFit()
        txtDeveloper.adjustsFontSizeToFitWidth = true
        
        txtCompany.text =  AppConfiguration?.AboutDeveloper.Description
        txtCompany.sizeToFit()
        txtCompany.adjustsFontSizeToFitWidth = true
        if !tablereloaded
        {
            tablereloaded = true
            tableView.reloadData()
        }
        
    }
    override func viewDidAppear(animated: Bool) {
        // tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        
        
        if indexPath.row == 0 {
            let h = txtDeveloper.bounds.size.height + 174
            
            return h
        }else if indexPath.row == 3 {
            
           // print(txtCompany.bounds.size.height)
            return (txtCompany.bounds.size.height + 174)

        }else if indexPath.row == 1 {
            
            return 50
        }else if indexPath.row == 4 {
            
            return 80
        }else{
            
            return 71
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor = .clearColor()
    }

    @IBAction func termscon(sender: AnyObject) {
        
        let view = self.storyboard?.instantiateViewControllerWithIdentifier("VcTerms") as! VcTerms
        self.navigationController?.pushViewController(view, animated: true)
        view.terms = 2
        view.about = true
        
    }
    
    @IBAction func privacyPolicy(sender: AnyObject) {
        
        let view = self.storyboard?.instantiateViewControllerWithIdentifier("VcTerms") as! VcTerms
        self.navigationController?.pushViewController(view, animated: true)
        view.terms = 3
        view.about = true
        
    }
    
    @IBAction func companyCall(sender: AnyObject) {
        VcHome.callNumber((AppConfiguration?.AboutCompany.Phone)!)
        
    }
    
    @IBAction func companyMail(sender: AnyObject) {
        let mailComposeViewController = configuredMailComposeViewController((AppConfiguration?.AboutCompany.Email)!)
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }

    }
    
    @IBAction func companySite(sender: AnyObject) {
        
        AppDelegate.attemptOpenURL((AppConfiguration?.AboutCompany.Website)!)
    }
    
    @IBAction func devCall(sender: AnyObject) {
        VcHome.callNumber((AppConfiguration?.AboutDeveloper.Phone)!)
    }
    
    @IBAction func devMail(sender: AnyObject) {
        
        let mailComposeViewController = configuredMailComposeViewController((AppConfiguration?.AboutDeveloper.Email)!)
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }

    }
    @IBAction func devSite(sender: AnyObject) {
        AppDelegate.attemptOpenURL((AppConfiguration?.AboutDeveloper.Website)!)
    }
    
    
    func configuredMailComposeViewController(mail:String) -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([mail])
        mailComposerVC.setSubject("")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        switch result.rawValue {
        case MFMailComposeResult.Cancelled.rawValue:
            self.view.makeToast("Email is cancelled")
            break
        case MFMailComposeResult.Sent.rawValue:
            self.view.makeToast("Email sent")
            break
        default:
            break
        }
        controller.dismissViewControllerAnimated(true, completion: { _ in })
    }

    
    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
