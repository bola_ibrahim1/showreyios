//
//  VcHelp.swift
//  ShowRey
//
//  Created by User on 1/17/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit
import MessageUI
import MBProgressHUD
import Crashlytics


class VcHelp: UIViewController, UISearchBarDelegate, MFMailComposeViewControllerDelegate, UITableViewDelegate, UITableViewDataSource
{

    
    var search:Bool = false
    var previouslySelectedHeaderIndex: Int?
    var selectedHeaderIndex: Int?
    var selectedItemIndex: Int?
    let cells = SwiftyAccordionCells()
    var searchHelpInfo = [HelpCenterObj]()
    var searchedArr = [HelpCenterObj]()
    var PagesCount = 1

    @IBOutlet weak var tableHelpInfo: UITableView!
    @IBOutlet weak var btnEmail: UIButton!
    var sampleTableClaas : SampleTableViewController!
    @IBOutlet weak var searchHelp: UISearchBar!
    
 
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Help Us"
        

        searchHelp.returnKeyType = UIReturnKeyType.Done
        navigationController!.navigationBar.barTintColor = UIColor(hex: "#b379e7")
        self.tableHelpInfo.rowHeight = UITableViewAutomaticDimension
        self.tableHelpInfo.estimatedRowHeight = 200
        let nib = UINib(nibName: "TcHeaderHelpQuestion", bundle: nil)
        tableHelpInfo.registerNib(nib, forCellReuseIdentifier: "TcHeaderHelpQuestion")
        
        let nib2 = UINib(nibName: "TcDetailHelpAnswer", bundle: nil)
        tableHelpInfo.registerNib(nib2, forCellReuseIdentifier: "TcDetailHelpAnswer")
        
        let button = UIButton()
        button.frame = CGRectMake(0, 0, 21, 31)
        button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
        button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
        let barButton = UIBarButtonItem()
        barButton.customView = button
        navigationItem.leftBarButtonItem = barButton
        // Do any additional setup after loading the view.
        btnEmail.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
       // btnEmail.titleLabel!.font = UIFont(name: "Monsterrat-Regular", size: 17)!
        // you probably want to center it
        btnEmail.titleLabel?.textAlignment = .Center
        // if you want to
        btnEmail.setTitle(" Still Need Help!\n Send Email", forState: .Normal)
        
        
        getSearchableHelp("",index: self.PagesCount, FromPagerOrRefresher: false)
        
        tableHelpInfo.addInfiniteScrollWithHandler { (UITableView) in
            
            self.getSearchableHelp("",index: self.PagesCount, FromPagerOrRefresher: true)
        }
        
//        self.tableViewTest.rowHeight = UITableViewAutomaticDimension
//        self.tableViewTest.estimatedRowHeight = 50
      
    }
   
  
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUp(){
        

        
        cells.removeAll();
        if search == true {
            
            for row in self.searchedArr{
                self.cells.append(SwiftyAccordionCells.HeaderItem(value: row.Questions!))
                self.cells.append(SwiftyAccordionCells.Item(value: row.Answer!))
              
            }
            
        }else {
            for row in self.searchHelpInfo{
                self.cells.append(SwiftyAccordionCells.HeaderItem(value: row.Questions!))
                self.cells.append(SwiftyAccordionCells.Item(value: row.Answer!))
            }
        }
        
       
          self.tableHelpInfo.reloadData()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if search == true {
            return self.searchedArr.count > 0 ? (self.searchedArr.count * 2) : 0
        }else{
        return self.searchHelpInfo.count > 0 ? (self.searchHelpInfo.count * 2) : 0
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
         if search == true {
            
            
            if(indexPath.row == (self.searchedArr.count * 2) && self.searchedArr.count > 0)
            {
                return UITableViewCell();
            }
            let item = self.cells.items[indexPath.row]
            
            
            if let cellHedaer: TcDetailHelpAnswer = tableView.dequeueReusableCellWithIdentifier("TcDetailHelpAnswer") as? TcDetailHelpAnswer {
                cellHedaer.setCell(searchedArr[indexPath.row/2].Answer!)
                
                // cell.textLabel?.text = value
                
                
                if item as? SwiftyAccordionCells.HeaderItem != nil {
                    let cellItem: TcHeaderHelpQuestion = tableView.dequeueReusableCellWithIdentifier("TcHeaderHelpQuestion") as! TcHeaderHelpQuestion
                    
                    cellItem.setCell(searchedArr[indexPath.row/2].Questions!)
                    
                    cellItem.selectionStyle = UITableViewCellSelectionStyle.None;
                    return cellItem
                }
                //  cellHedaer.selectionStyle = nil
                //   UITableViewCellSelectionStyleNone;
                
                
                cellHedaer.selectionStyle = UITableViewCellSelectionStyle.None
                
                
                return cellHedaer
            }

            
         }else{
        
        
        if(indexPath.row == (self.searchHelpInfo.count * 2) && self.searchHelpInfo.count > 0)
        {
            return UITableViewCell();
        }
        let item = self.cells.items[indexPath.row]
        
        
        if let cellHedaer: TcDetailHelpAnswer = tableView.dequeueReusableCellWithIdentifier("TcDetailHelpAnswer") as? TcDetailHelpAnswer {
            cellHedaer.setCell(searchHelpInfo[indexPath.row/2].Answer!)
            
            // cell.textLabel?.text = value
            
            
            if item as? SwiftyAccordionCells.HeaderItem != nil {
                let cellItem: TcHeaderHelpQuestion = tableView.dequeueReusableCellWithIdentifier("TcHeaderHelpQuestion") as! TcHeaderHelpQuestion
                
                cellItem.setCell(searchHelpInfo[indexPath.row/2].Questions!)
                
                    cellItem.selectionStyle = UITableViewCellSelectionStyle.None;
                    return cellItem
            }
            //  cellHedaer.selectionStyle = nil
            //   UITableViewCellSelectionStyleNone;
          
            
            cellHedaer.selectionStyle = UITableViewCellSelectionStyle.None
            
                      
            return cellHedaer
            }
        }
        
        
        /*
        if let cellHedaer: TcHeaderHelpQuestion = tableView.dequeueReusableCellWithIdentifier("TcHeaderHelpQuestion") as? TcHeaderHelpQuestion {
            cellHedaer.setCell(searchHelpInfo[indexPath.row/2].Questions!)
            
            // cell.textLabel?.text = value
            
            if item as? SwiftyAccordionCells.HeaderItem != nil {
                let cellItem: TcDetailHelpAnswer = tableView.dequeueReusableCellWithIdentifier("TcDetailHelpAnswer") as! TcDetailHelpAnswer
                
                cellItem.setCell(searchHelpInfo[indexPath.row/2].Answer!)
                
                return cellItem
            }
            //  cellHedaer.selectionStyle = nil
            //   UITableViewCellSelectionStyleNone;
            //  cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cellHedaer.selectionStyle = UITableViewCellSelectionStyle.None
            
            
            return cellHedaer
        }
        
        */
        
        
        return UITableViewCell()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
     //  return 70;
        
        
        
        if(indexPath.row == (self.searchHelpInfo.count * 2) && self.searchHelpInfo.count > 0)
        {
            
//            var attributes  = [NSFontAttributeName: UIFont(name: "Montserrat-Regular", size: 16)]
//            let labelSize : NSString =  self.searchHelpInfo[indexPath.row].Questions!
//            let rect = labelSize.boundingRectWithSize(CGSizeMake(180, 1000000), options: .UsesLineFragmentOrigin, attributes: [NSFontAttributeName: self], context: nil)
//            return  rect.height + 50 ;
         return  UITableViewAutomaticDimension ;
        }
        
        let item = self.cells.items[indexPath.row]
        
        if item is SwiftyAccordionCells.HeaderItem {
            
           return  UITableViewAutomaticDimension ;
            
        } else if (item.isHidden) {
            return 0
        } else {
            return UITableViewAutomaticDimension
            
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let item = self.cells.items[indexPath.row]
        
        if item is SwiftyAccordionCells.HeaderItem {
            if self.selectedHeaderIndex == nil {
                self.selectedHeaderIndex = indexPath.row
            } else {
                self.previouslySelectedHeaderIndex = self.selectedHeaderIndex
                self.selectedHeaderIndex = indexPath.row
            }
            
            if let previouslySelectedHeaderIndex = self.previouslySelectedHeaderIndex {
                self.cells.collapse(previouslySelectedHeaderIndex)
            }
            
            if self.previouslySelectedHeaderIndex != self.selectedHeaderIndex {
                self.cells.expand(self.selectedHeaderIndex!)
            } else {
                self.selectedHeaderIndex = nil
                self.previouslySelectedHeaderIndex = nil
            }
            
            self.tableHelpInfo.beginUpdates()
            self.tableHelpInfo.endUpdates()
            
        } else {
            if indexPath.row != self.selectedItemIndex {
                //        let cell = self.TableQuestions.cellForRowAtIndexPath(indexPath)
                
                // cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
                
                if let selectedItemIndex = self.selectedItemIndex {
                    let previousCell = self.tableHelpInfo.cellForRowAtIndexPath(NSIndexPath(forRow: selectedItemIndex, inSection: 0))
                    previousCell?.accessoryType = UITableViewCellAccessoryType.None
                }
                
                self.selectedItemIndex = indexPath.row
            }
        }
        
//        let selectedCell:UITableViewCell = tableView.cellForRowAtIndexPath(indexPath)!
//        selectedCell.contentView.backgroundColor = UIColor.whiteColor()

    }
    @IBAction func btnSendEmail(sender: AnyObject) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
//        var emailTitle: String = "Need Help"
//        // Email Content
//        var messageBody: String = ""
//        // To address
//        var toRecipents: [String] = [[(AppConfiguration?.AboutCompany.Email)!]]
//        var mc = MFMailComposeViewController()
//        mc.mailComposeDelegate = self
//        mc.setSubject(emailTitle)
//        mc.setMessageBody(messageBody, isHTML: false)
//        mc.setToRecipients(toRecipents)
//        self.presentViewController(mc, animated: true, completion: { _ in })
    }

    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients([(AppConfiguration?.AboutCompany.Email)!])
        mailComposerVC.setSubject("Need Help...")
        mailComposerVC.setMessageBody("", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let sendMailErrorAlert = UIAlertView(title: "Could Not Send Email", message: "Your device could not send e-mail.  Please check e-mail configuration and try again.", delegate: self, cancelButtonTitle: "OK")
        sendMailErrorAlert.show()
    }
    
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        switch result.rawValue {
        case MFMailComposeResult.Cancelled.rawValue:
             self.view.makeToast("Email is cancelled")
            break
        case MFMailComposeResult.Sent.rawValue:
            self.view.makeToast("Email sent")
            break
        default:
            break
        }
        controller.dismissViewControllerAnimated(true, completion: { _ in })
    }

    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        self.PagesCount = 1
        if searchText != "" {
             search = true
            getSearchableHelp(searchText,index: self.PagesCount, FromPagerOrRefresher: false)
        }
        else
        {search = false
        
            setUp()
        }
       
       
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func collapsetable(mytag:Int){
        let myindex = mytag
        cells.collapse(myindex)
        
        let item = self.cells.items[myindex]
        
        if item is SwiftyAccordionCells.HeaderItem {
            if self.selectedHeaderIndex == nil {
                self.selectedHeaderIndex = myindex
            } else {
                self.previouslySelectedHeaderIndex = self.selectedHeaderIndex
                self.selectedHeaderIndex = myindex
            }
            
            if let previouslySelectedHeaderIndex = self.previouslySelectedHeaderIndex {
                self.cells.collapse(previouslySelectedHeaderIndex)
            }
            
            if self.previouslySelectedHeaderIndex != self.selectedHeaderIndex {
                self.cells.expand(self.selectedHeaderIndex!)
            } else {
                self.selectedHeaderIndex = nil
                self.previouslySelectedHeaderIndex = nil
            }
            
            self.tableHelpInfo.beginUpdates()
            self.tableHelpInfo.endUpdates()
            
        } else {
            if (myindex) != self.selectedItemIndex {
                
                let cell = self.tableHelpInfo.cellForRowAtIndexPath(NSIndexPath(forRow: myindex, inSection: 0))
                cell?.accessoryType = UITableViewCellAccessoryType.Checkmark
                
                if let selectedItemIndex = self.selectedItemIndex {
                    let previousCell = self.tableHelpInfo.cellForRowAtIndexPath(NSIndexPath(forRow: selectedItemIndex, inSection: 0))
                    previousCell?.accessoryType = UITableViewCellAccessoryType.None
                }
                
                self.selectedItemIndex = myindex
            }
        }
        
    }
    

    func getSearchableHelp(searchStr : String,index:Int, FromPagerOrRefresher:Bool){
        
        if search == false && !FromPagerOrRefresher {
           
            let loadingNotification = MBProgressHUD.showHUDAddedTo(view, animated: true)
            loadingNotification.label.text = "Loading..."
            
        }
        let RequestParameters : NSDictionary = [
            "Keyword" : searchStr,
            "PageIndex":index,
            "PageSize":10
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetHelpCenter, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                self.tableHelpInfo.finishInfiniteScroll()
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response = ModHelp(json:JSON)
                        
                        if (Response.ResultResponse == "21"){
                            self.view.makeToast("An error has been occurred")
                            
                            
                        }
//                        else if (Response.ResultResponse == "6"){
//                          self.view.makeToast("No Content Exists")
//                        }
                        else
                        {
                            if(self.search == true){
                                self.searchedArr.removeAll()
                              self.searchedArr.appendContentsOf(Response.HelpCenterOb!);
                            }else{
                          
                                self.searchHelpInfo.appendContentsOf(Response.HelpCenterOb!);
                                self.PagesCount += 1
                            
                            }
                        
                            // for index in 0..<self.searchHelp.count {
                            // self.sections.append(Section(name: self.searchHelp[index].Questions!, items: [self.searchHelp[index].Answer!]))
                            //}
                            
                              self.setUp()
                          
                            
                            
                            
                        }
                        
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
