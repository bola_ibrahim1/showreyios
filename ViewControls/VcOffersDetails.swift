//
//  VcOffersDetails.swift
//  ShowRey
//
//  Created by M-Hashem on 11/25/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import FirebaseAnalytics
import MBProgressHUD
import Kingfisher
import FBSDKShareKit

class VcOffersDetails: UIViewController
{
    
    @IBOutlet weak var BodyView: UIView!
    @IBOutlet weak var CommentsLbl: UILabel!
    @IBOutlet weak var LikesLbl: UILabel!
    @IBOutlet weak var DislikeLbl: UILabel!
    
    @IBOutlet weak var LikeCon: UIView!
    @IBOutlet weak var YesLbl: UILabel!
    @IBOutlet weak var LikeImage: UIImageView!
    @IBOutlet weak var DislikeCon: UIView!
    @IBOutlet weak var NoLbl: UILabel!
    @IBOutlet weak var DislikeImage: UIImageView!
    @IBOutlet weak var ShareCon: UIView!
    
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var OrignalPrice: UILabel!
    @IBOutlet weak var Price: UILabel!
    @IBOutlet weak var DetailsLbl: UITextView!
    
    var Parent:UIViewController!
    var FeedData:ModFeedFeedobj!
    
    var _Liked:Bool = false
    var Liked:Bool
        {
        set
        {
            LikeImage.image = newValue ? UIImage(named: "Offers-(Like)") : UIImage(named: "Offers-(Like)-unselected")
            YesLbl.textColor = newValue ? UIColor(hex:"b379e7") : UIColor.lightGrayColor()
            _Liked = newValue
            (FeedData.SharedPost ?? FeedData)?.Liked = newValue
        }
        get{return _Liked}
    }
    @IBOutlet weak var outletofbtnowner: UIButton!
    @IBAction func actionOfOwner(sender: AnyObject)
    {
        if let userInfoAvailable = FeedData.UserProfile
        {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let profile = mainStoryboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
            profile.SetupForTimeLine("\(userInfoAvailable.Id ?? 0)")
            self.navigationController?.pushViewController(profile, animated: true)
        }
        else
        {
            self.view.makeToast("Owner info not available")
        }
    }
    var _Disliked:Bool = false
    var fromid = false
    var Disliked:Bool
        {
        set
        {
            DislikeImage.image = newValue ? UIImage(named: "Offers-(DisLike)") : UIImage(named: "Offers-(DisLike)-unselected")
            NoLbl.textColor = newValue ? UIColor(hex:"b379e7") : UIColor.lightGrayColor()
            _Disliked = newValue
            (FeedData.SharedPost ?? FeedData)?.Disliked = newValue
        }
        get{return _Disliked}
    }
    let barButton2 = UIBarButtonItem()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        LikeCon.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(VcOffersDetails.Like)));
        DislikeCon.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(VcOffersDetails.Dislike)))
        ShareCon.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(VcOffersDetails.Share)))
        
        LikesLbl.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(VcOffersDetails.toLikers)))
        DislikeLbl.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(VcOffersDetails.toDisLikers)))
        CommentsLbl.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(VcOffersDetails.CommentsClicked)))
        
        let button2 = UIButton()
        button2.frame = CGRectMake(0, 0, 10, 31)
        button2.setImage(UIImage(named: "Nav-Bar-(Options)"), forState: .Normal)
        //button2.backgroundColor = UIColor.whiteColor()
        button2.addTarget(self, action: #selector(OfferOptions), forControlEvents: .TouchUpInside)
        
        barButton2.customView = button2
        
        //        if(!fromid && FeedData.UserProfile?.Id == User?.Id)
        //        {
        
        navigationItem.rightBarButtonItem = barButton2
        //  }
        navigationItem.title = "Offer Details"
        //
        if !fromid
        {
        //    updateUiData();
        }
        FIRAnalytics.logEventWithName("Special offers", parameters: nil)
        
    }
  
    override func viewDidAppear(animated: Bool) {
        if !fromid
        {
            updateUiData();
        }
    }
    func loadFromID(id:String)
    {
        fromid = true
        CvOffers.RefreshOffer(self,PostID: Int(id)!, completion: { (succ) in
            
            self.FeedData = succ;
            //            if(self.FeedData.UserProfile?.Id == User?.Id)
            //            {
            self.navigationItem.rightBarButtonItem = self.barButton2
            //   }
            self.updateUiData();
            self.drawContainer()
        })
    }
    var loopBreakerCount = 0
    override func viewDidLayoutSubviews()
    {
        if loopBreakerCount > 2 {return}
        loopBreakerCount += 1
        super.viewDidLayoutSubviews()
        //        ImageView.hidden = true
        //        ImageView.kf_setImageWithURL(NSURL(string: FeedData.AttachedFiles?[0].URL ?? ""), placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        if !fromid
        {
            drawContainer();
        }
    }
    func drawContainer()
    {
        let rect = CGRect(x: 0 , y: 0 , width:BodyView.frame.size.width , height: BodyView.frame.size.height)
        if (FeedData.PostCType == PostContentType.Text_Videos.rawValue)
        {
            let videoContainerView = VedioThumbnail.videoViewWithAttachedFile(FeedData.AttachedFiles?[0], frame: rect) { }
            BodyView.addSubview(videoContainerView)
            videoContainerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(PlayVideo)))
            
        }
        else
        {
            if FeedData.AttachedFiles != nil
            {
                let multiimageview = MultiImageCellView.imagesView(forAttachedFiles: FeedData.AttachedFiles!, withFrame: rect) as! MultiImageCellView
                
                BodyView.addSubview(multiimageview)
                
                multiimageview.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(OpenGallery)))
            }
        }
    }
    
    ///hello
    func updateUiData()
    {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string:  (FeedData.OriginalPrice?.stringValue)!  + " SAR")
        attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
        print("Profile object by ispha 🤗 = \(FeedData.UserProfile)")
        outletofbtnowner.setTitle(FeedData.UserProfile?.FullName ?? "---", forState: .Normal)
        
        TitleLbl.text = FeedData.Title
 
        DetailsLbl.text = FeedData.Text

  
        Price.text = (FeedData.Price?.stringValue)! + " SAR"
        OrignalPrice.attributedText = attributeString
        
        LikesLbl.text = "\(FeedData.NoOfLikes!) Yes"
        DislikeLbl.text = "\(FeedData.NoOfDislikes!) No"
        CommentsLbl.text = "\(FeedData.NoOfComments!) Comment"
        Liked = FeedData.Liked
        Disliked = FeedData.Disliked
    }
    func OfferOptions()
    {
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let Edit = UIAlertAction(title: "Edit offer", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            let editscreen = self.Parent.storyboard?.instantiateViewControllerWithIdentifier("TvCreatePost") as! TvCreatePost
            editscreen.modFeedFeedobj = self.FeedData;
            editscreen.FromScreen = .EditSpecialOffers // edit offer
            editscreen.Editblock = { ()in
                
                CvOffers.RefreshOffer(self,PostID: self.FeedData.Id!.integerValue, completion: { (succ) in
                    self.FeedData = succ;
                    
                    if (self.Parent is CvOffers)
                    {
                        (self.Parent as! CvOffers).RefreshCellUI(self.FeedData);
                    }
                    self.updateUiData();
                })
                
            }
            self.Parent?.navigationController?.pushViewController(editscreen, animated: true)
            
        })
        
        let delete = UIAlertAction(title: "Delete offer", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let alert = UIAlertController(title: "Confirm", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            let ok = UIAlertAction(title: "Delete", style: UIAlertActionStyle.Default , handler:
                {(action: UIAlertAction) -> Void in
                    self.DeleteOffer()
            })
            let cancel = UIAlertAction(title: "Cancel", style: .Default, handler: {(action: UIAlertAction) -> Void in
                alert.dismissViewControllerAnimated(true, completion: { _ in })
            })
            alert.addAction(ok)
            alert.addAction(cancel)
            self.Parent.presentViewController(alert, animated: true, completion: { _ in })
            
        })
        
        let Report = UIAlertAction(title: "Report offer", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let report = self.Parent.storyboard?.instantiateViewControllerWithIdentifier("VcReport") as! VcReport
            report.come = .Offer
            report.feed = self.FeedData
            self.Parent!.navigationController?.pushViewController(report, animated: true)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        if(FeedData.UserProfile?.Id == User?.Id)
        {
            optionMenu.addAction(Edit)
            optionMenu.addAction(delete)
            
        }
        else
        {
            optionMenu.addAction(Report)
        }
        
        optionMenu.addAction(cancel)
        
        Parent.presentViewController(optionMenu, animated: true, completion: nil)
        
    }
    func DeleteOffer()
    {
        MBProgressHUD.showHUDAddedTo(view, animated: true).label.text = "Deleting..."
        let RequestParameters : NSDictionary =
            [
                "LevelId":0,
                "PostId":FeedData.Id!,
                "UserId":User!.Id!
        ]
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.DeleteSpecialOffersPostandCommment, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                var success = false;
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            // print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let FeedsResponse = ModResponse(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            // error
                            self.view.makeToast("Couldn't delete offer")
                        }
                        else
                        {
                            success = true
                            self.view.makeToast("Offer deleted")
                            if (self.Parent is CvOffers)
                            {
                                (self.Parent as! CvOffers).HidePost(self.FeedData)
                            }
                            self.Parent.navigationController?.popViewControllerAnimated(true)
                        }
                    }
                }
                //                if completion != nil
                //                {completion!(success: success)}
            }
            , callbackDictionary: nil)
        
        
    }
    func setData(parent:UIViewController, data:ModFeedFeedobj)
    {
        FeedData = data;
        Parent = parent;
        
    }
    func PlayVideo()
    {
        var mainFeed = FeedData
        if(FeedData.SharedPost != nil)
        {
            mainFeed = FeedData.SharedPost!
        }
        
        let av = AVPlayerViewController()//"https://youtu.be/zQciMy_Pr0Q"
        //let url = NSURL(string: "http://www.ebookfrenzy.com/ios_book/movie/movie.mov")
        if mainFeed.AttachedFiles != nil {
            let url = NSURL(string: (mainFeed.AttachedFiles?[0].URL!.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)!)
            let player = AVPlayer(URL: url!)
            av.player = player
            
            Parent.presentViewController(av, animated: true, completion: nil)
            player.play()
        }else {
            
            self.view.makeToast("Couldn't play content of this video")
        }
    }
    func OpenGallery()
    {
        var mainFeed = FeedData
        if(FeedData.SharedPost != nil)
        {
            mainFeed = FeedData.SharedPost!
        }
        NewsFeedHelper.imageViewer(mainFeed.AttachedFiles!,caption: mainFeed.Text!, presnter: Parent, animatefrom: BodyView)
        
    }
    func Like()
    {
        LikeDislike(true)
    }
    func Dislike()
    {
        LikeDislike(false)
    }
    func Share()
    {
        //        var url = NSURL(string: FeedData.AttachedFiles![0].URL!)
        //        let textOffer = FeedData!.Text
        //        var activityItems: [AnyObject]?
        //
        //        KingfisherManager.sharedManager.retrieveImageWithURL(url!, optionsInfo: nil, progressBlock: nil, completionHandler: { (image, error, cacheType, imageURL) -> () in
        //           // print(image)
        //
        //            if (image != nil) {
        //                activityItems = [textOffer as! AnyObject, image!]
        //            }else {
        //
        //                activityItems = [textOffer as! AnyObject]
        //            }
        //
        //        })
        //
        //        let activityController = UIActivityViewController(activityItems:
        //            activityItems!, applicationActivities: nil)
        //        self.presentViewController(activityController, animated: true, completion: nil)
        
        getDynamicLink()
        
    }
    
    func getDynamicLink(){
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
        let offerID : NSNumber = FeedData.Id!
        
        var imageUrl:String!
        
        if self.FeedData.PostCType == PostContentType.Text_Videos.rawValue {
            
            imageUrl = self.FeedData.AttachedFiles![0].Thumb!
            
        }else {
            
            imageUrl = self.FeedData.AttachedFiles![0].URL!
        }
        
        //  "https://yvf65.app.goo.gl/?link=http://rw-universal-links-final.herokuapp.com?offerID=\(offerID)&ibi=com.mobileznation.ShowRey&ius=fb1741708876094039&apn=com.mobileznation.showrey&amv=16&afl=https://www.google.com.eg"
        // "https://yvf65.app.goo.gl/?link=http://rw-universal-links-final.herokuapp.com?offerID=\(offerID)&ibi=com.mobileznation.ShowRey&ius=fb155466188278867&ifl=https://www.google.com.eg/?ios&apn=com.mobileznation.showrey&amv=1&afl=https://www.google.com.eg/?android"
        //"https://yvf65.app.goo.gl/?link=http://www.showrey.com?offerID=\(offerID)&ibi=com.mobileznation.ShowRey&ius=fb155466188278867&ifl=http://www.showrey.com&apn=com.mobileznation.showrey&amv=1&afl=http://www.showrey.com"
        
        let RequestParameters : NSDictionary = [
            "longDynamicLink" : "https://yvf65.app.goo.gl/?link=http://cms.mobileznation.com/ShowreyApp/LandingPage?Offer=offerID-\(offerID)&ibi=com.mobileznation.ShowRey&ius=fb155466188278867&ifl=\((AppConfiguration?.AboutCompany.Website)!)&apn=com.mobileznation.showrey&amv=1&afl=\((AppConfiguration?.AboutCompany.Website)!)&st=\(self.FeedData!.Title)&sd=\(self.FeedData!.Text)&si=\(imageUrl)"
            
            
        ]
        
        var FinalDictionaryString = ""
        
        let data = try! NSJSONSerialization.dataWithJSONObject(RequestParameters, options: [])
        FinalDictionaryString = (NSString(data: data, encoding: NSUTF8StringEncoding) as? String)!
        
        NetworkHelper.RequestHelper("https://firebasedynamiclinks.googleapis.com", service: "/v1/shortLinks?key=AIzaSyBeRStwuMghujfa8NlUn2ALQErh2pUfBoA", hTTPMethod: Method.post, parameters: nil, httpBody: FinalDictionaryString
            , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: { (JSON, NSError) in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if((NSError) != nil)
                {
                    if let networkError = NSError {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    
                    
                    if (!JSON["shortLink"])
                    {
                        let shortLink =   JSON["shortLink"].rawString()
                        
                        let activityItems = [NSURL(string: shortLink!)!]
                        
                        let activityController = UIActivityViewController(activityItems:
                            activityItems, applicationActivities: nil)
                        //activityController.excludedActivityTypes = [UIActivityTypePostToFacebook]
                        self.presentViewController(activityController, animated: true, completion: nil)
                        
                        
                        //                        // 1
                        //                        let optionMenu = UIAlertController(title: "Share Via", message: nil, preferredStyle: .ActionSheet)
                        //
                        //                        // 2
                        //                        let face = UIAlertAction(title: "Facebook", style: .Default, handler: {
                        //                            (alert: UIAlertAction!) -> Void in
                        //
                        //
                        //                            let content = FBSDKShareLinkContent()
                        //
                        //                            content.contentURL = NSURL(string: shortLink!)
                        //                            content.contentTitle = self.FeedData!.Title
                        //
                        //                            if self.FeedData.PostCType == PostContentType.Text_Videos.rawValue {
                        //
                        //                                content.imageURL = NSURL(string: self.FeedData.AttachedFiles![0].Thumb! )
                        //
                        //                            }else {
                        //
                        //                                content.imageURL = NSURL(string: self.FeedData.AttachedFiles![0].URL!)
                        //                            }
                        //
                        //                            content.contentDescription = self.FeedData.Text
                        //
                        //                            let dialog = FBSDKShareDialog()
                        //                            dialog.fromViewController = self
                        //                            dialog.shareContent = content
                        //                            dialog.mode = FBSDKShareDialogMode.Native
                        //
                        //                            if (!dialog.canShow()) {
                        //                                // fallback presentation when there is no FB app
                        //                                dialog.mode = FBSDKShareDialogMode.FeedBrowser
                        //                            }
                        //                            dialog.show()
                        //
                        //
                        //                        })
                        //
                        //                        let other = UIAlertAction(title: "Other", style: .Default, handler: {
                        //                            (alert: UIAlertAction!) -> Void in
                        //
                        //                            var activityItems: [AnyObject]?
                        //
                        //                            if self.FeedData.PostCType == PostContentType.Text_Videos.rawValue {
                        //
                        //                                 activityItems = [self.FeedData!.Title!, self.FeedData.Text!, NSURL(string: shortLink!)!, NSURL(string: self.FeedData.AttachedFiles![0].Thumb! )!]
                        //
                        //                            }else {
                        //
                        //                                activityItems = [self.FeedData!.Title!, self.FeedData.Text!, NSURL(string: shortLink!)!, NSURL(string: self.FeedData.AttachedFiles![0].URL!)!]
                        //                            }
                        //
                        //
                        //                            let activityController = UIActivityViewController(activityItems:
                        //                                activityItems!, applicationActivities: nil)
                        //                            activityController.excludedActivityTypes = [UIActivityTypePostToFacebook]
                        //                            self.presentViewController(activityController, animated: true, completion: nil)
                        //
                        //                        })
                        //
                        //                        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
                        //                            (alert: UIAlertAction!) -> Void in
                        //                        })
                        //
                        //
                        //                        // 4
                        //                        optionMenu.addAction(face)
                        //                        optionMenu.addAction(other)
                        //                        optionMenu.addAction(cancelAction)
                        //
                        //                        // 5
                        //                        self.presentViewController(optionMenu, animated: true, completion: nil)
                        
                    }else {
                        
                        print("Error")
                    }
                }
                
                
        })
    }
    
    
    
    func toLikers()
    {
        let likers = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        likers.OffersLikersMod(0, LikeType: true, ObjId: (FeedData.Id?.integerValue)!)
        likers.title = "Likers"
        Parent.navigationController?.pushViewController(likers, animated: true)
    }
    func toDisLikers()
    {
        let likers = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        likers.OffersLikersMod(0, LikeType: false, ObjId: (FeedData.Id?.integerValue)!)
        likers.title = "Dislikes"
        Parent.navigationController?.pushViewController(likers, animated: true)
    }
    func CommentsClicked()
    {
        let commentsController = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
        commentsController.MyScreenMod = .All
        commentsController.InitMod(0, ObjId: (FeedData.Id?.integerValue)!, PostType: 1,IsItSpecialOffer: true ,isItPrivate : FeedData.CommentPrivacy)
        
        Parent.navigationController?.pushViewController(commentsController, animated: true)
        commentsController.Commentblock = { (id) in
            // self.Parent.RefrehFeed(id)
        }
        
    }
    
    var likeing = false
    var Dislikeing = false
    func LikeDislike(like:Bool)
    {
        if Dislikeing || likeing
        {return}
        
        if (like)
        {
            likeing = true
        }
        else
        {
            Dislikeing = true
        }
        let RequestParameters : NSDictionary =
            [
                "LevelId":0,
                "LikeType":like,
                "LikeValue":like ? !Liked : !Disliked,
                "PostId":FeedData.Id!,
                "UserId":User!.Id!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.LikeDislikeSpecialOffers, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.Parent.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.Parent.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let FeedsResponse = ModResponse(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            // error
                        }
                        else
                        {
                            if self.likeing
                            {
                                self.Liked = !self.Liked
                                self.FeedData.NoOfLikes! = self.FeedData.NoOfLikes!.integerValue + (self.Liked ? 1 : -1)
                                self.LikesLbl.text = "\(self.FeedData.NoOfLikes!) Yes"
                                
                                if self.Liked && self.Disliked
                                {
                                    self.Disliked = false;
                                    self.FeedData.NoOfDislikes! = self.FeedData.NoOfDislikes!.integerValue-1
                                    self.DislikeLbl.text = "\(self.FeedData.NoOfDislikes!) No"
                                }
                            }
                            else if self.Dislikeing
                            {
                                self.Disliked = !self.Disliked
                                self.FeedData.NoOfDislikes! = self.FeedData.NoOfDislikes!.integerValue + (self.Disliked ? 1 : -1)
                                self.DislikeLbl.text = "\(self.FeedData.NoOfDislikes!) No"
                                
                                if self.Liked && self.Disliked
                                {
                                    self.Liked = false;
                                    self.FeedData.NoOfLikes! = self.FeedData.NoOfLikes!.integerValue-1
                                    self.LikesLbl.text = "\(self.FeedData.NoOfLikes!) Yes"
                                }
                            }
                            
                        }
                    }
                }
                
                self.Dislikeing = false
                self.likeing = false
            }
            , callbackDictionary: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
