//
//  kk.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/27/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class kk: UITableViewCell {
    
    
    @IBOutlet weak var imgCoverPhoto: UIImageView!
    @IBOutlet weak var imgProfilePicture: UIImageView!
    @IBOutlet weak var imgAccountVerfied: UIImageView!
    @IBOutlet weak var AddFollowerbtn: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblNumberOfQuestion: UILabel!
    @IBOutlet weak var lblNumberOfQuestionFiend: UILabel!
    @IBOutlet weak var lblNumberOfReplied: UILabel!
    @IBOutlet weak var lblNumberOfRepliedFriends: UILabel!
    @IBOutlet weak var lblNumberOfInvites: UILabel!
    @IBOutlet weak var lblNumberOfTitle: UILabel!
    var Parent:UIViewController!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgProfilePicture.layer.cornerRadius = imgProfilePicture.frame.width/2
        AddFollowerbtn.layer.cornerRadius = AddFollowerbtn.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func fdf(sender: AnyObject) {
        
        print("OptionMenu-----")
        // 1
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        // 2
        
        let savedPosts = UIAlertAction(title: "Saved Posts", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let savedposts = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
            savedposts.SetupForMySavedPosts()
            self.Parent.navigationController?.pushViewController(savedposts, animated: true)
            
            //            let editScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupEdit") as! VcGroupEdit
            //
            //            editScreen.getFollowees = self.groupParticipants
            //            editScreen.getSelectedFollowees = self.participantsId
            //            editScreen.gName = self.groupNameStr
            //            editScreen.navTitle = "Edit Group"
            //
            //            self.navigationController?.pushViewController(editScreen, animated: true)
            
        })
        
        optionMenu.addAction(savedPosts)
        
        let followingAndFollowers = UIAlertAction(title: "Following Followers", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let FollowContainer=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcFollowContainer")
            self.Parent.navigationController?.pushViewController(FollowContainer, animated: true)
            //  self.leaveGroup()
            
        })
        
        optionMenu.addAction(followingAndFollowers)
        
        
        let editProfile = UIAlertAction(title: "Edit Profile", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            //  self.deleteGroup()
            
            
        })
        
        optionMenu.addAction(editProfile)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        
        optionMenu.addAction(cancelAction)
        
        // 5
        
        Parent.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
}
