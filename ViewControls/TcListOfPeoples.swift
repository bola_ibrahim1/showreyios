//
//  TcListOfPeoples.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 11/19/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit



class TcListOfPeoples: UITableViewCell {
    
    @IBOutlet weak var PeoplePic: UIImageView!
    
    @IBOutlet weak var PeopleName: UILabel!
    
    @IBOutlet weak var PeopleFollowes: UILabel!
    
    @IBOutlet weak var FollowBtn: UIButton!
    
    
    var parentview : VcListOfPeoples!
    var myOnPeoples:ModFollowersobj = ModFollowersobj()
    
    var followtype : Bool = false
    var followerID : Int = -1;
    
    
    func setCell(OnPeoples:ModFollowersobj) {
        
        myOnPeoples = OnPeoples
        followerID = Int((OnPeoples.Id?.intValue)!)
        
        if (OnPeoples.FollowStatus == 1 ) //followed
        {
            FollowBtn.hidden = false
            
            followtype = false // unfollow
            FollowBtn.setTitle("Followed", forState: .Normal)
            FollowBtn.setTitleColor(UIColor(hex: "#ffffff"), forState: UIControlState.Normal)
            FollowBtn.backgroundColor = UIColor(hex: "#b379e7")
        }
        else if (OnPeoples.FollowStatus == 0 ) //Follow +
        {
            
            FollowBtn.hidden = false
            followtype = true // follow
            FollowBtn.setTitle("Follow +", forState: .Normal)
            
            FollowBtn.setTitleColor(UIColor(hex: "#b379e7"), forState: UIControlState.Normal)
            FollowBtn.backgroundColor = UIColor(hex: "#ffffff")
        }
        else if (OnPeoples.FollowStatus == 2 ) //other +
        {
            FollowBtn.hidden = true
        }
        
        FollowBtn.layer.cornerRadius = 15
        FollowBtn.layer.borderWidth = 1
        FollowBtn.layer.borderColor = UIColor(hex: "#b379e7").CGColor
        
        
        
        
        PeopleName.text = OnPeoples.FullName
        PeopleFollowes.text = "@\((OnPeoples.UserName)!)"
        
        PeoplePic.layer.cornerRadius = self.PeoplePic.frame.size.width / 2;
        PeoplePic.clipsToBounds = true;
        PeoplePic.userInteractionEnabled = true
        let str : String? = (OnPeoples.ProfilePicture)!
        let url = NSURL(string:str!)
        PeoplePic.kf_setImageWithURL(url, placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        
        
        
    }
    
    @IBAction func FollowAction(sender: AnyObject) {
        
        var RequestParameters : NSDictionary
        
        if ( followtype == false) //followed
        {
            
            FollowBtn.setTitle("Follow +", forState: .Normal)
            FollowBtn.setTitleColor(UIColor(hex: "#b379e7"), forState: UIControlState.Normal)
            FollowBtn.setTitleColor(UIColor(hex: "#b379e7"), forState: UIControlState.Highlighted)
            FollowBtn.backgroundColor = UIColor(hex: "#ffffff")
            myOnPeoples.FollowStatus = NSNumber(int: 0)
            RequestParameters =
                [   "UserId":User!.Id!,
                    "FollowType":false,
                    "FollowerId":followerID,
                    "FollowerType":1,
            ]
        }
        else //Follow +
        {
            FollowBtn.setTitle("Followed", forState: .Normal)
            FollowBtn.setTitleColor(UIColor(hex: "#ffffff"), forState: UIControlState.Normal)
            FollowBtn.setTitleColor(UIColor(hex: "#ffffff"), forState: UIControlState.Highlighted)
            FollowBtn.backgroundColor = UIColor(hex: "#b379e7")
            myOnPeoples.FollowStatus = NSNumber(int: 1)
            RequestParameters =
                [   "UserId":User!.Id!,
                    "FollowType":true,
                    "FollowerId":followerID,
                    "FollowerType":1,
            ]
        }
        
        followtype = !followtype
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service:WSMethods.FollowUser, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in}, callbackDictionary: nil)
        
        
        
        
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
