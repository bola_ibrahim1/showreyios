//
//  VcTagging.swift
//  ShowRey
//
//  Created by Radwa Khaled on 10/29/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import KILabel

class VcTagging: UIViewController {
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let label = KILabel(frame: CGRect(x: 20, y: 64, width: 280, height: 60))
        label.text = "Follow @krelborn or visit http://compiledcreations.com #shamelessplug"
  

        
        label.userHandleLinkTapHandler = { label, handle, range in
            NSLog("User handle \(handle) tapped")
        }
        
        // Attach a block to be called when the user taps a user handle
        label.userHandleLinkTapHandler = { label, handle, range in
            NSLog("User handle \(handle) tapped")
            print(handle)
            
            
        }
        
        
        // Attach a block to be called when the user taps a hashtag
        label.hashtagLinkTapHandler = { label, hashtag, range in
            NSLog("Hashtah \(hashtag) tapped")
            print(hashtag)
        }
        
        // Attach a block to be called when the user taps a URL
        label.urlLinkTapHandler = { label, url, range in
            NSLog("URL \(url) tapped")
            print(url)
        }
        
        view.addSubview(label)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
