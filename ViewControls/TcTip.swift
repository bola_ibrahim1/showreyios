//
//  TcTip.swift
//  ShowRey
//
//  Created by M-Hashem on 11/21/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import Hue

class TcTip: UITableViewCell
{
    @IBOutlet weak var TipTitle: UILabel!
    @IBOutlet weak var DateLbl: UILabel!
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(hex: "e4d3f7")
        selectedBackgroundView = bgColorView

    }
    
//    override func setSelected(selected: Bool, animated: Bool)
//    {
//        // super.setSelected(selected, animated: animated)
//        
//    //    backgroundColor = UIColor(hex: "e4d3f7")
//    }
    
    func setData(data:ModTip)
    {
        TipTitle.text = data.Title!
        DateLbl.text = data.Date!
        
    }
    
}
