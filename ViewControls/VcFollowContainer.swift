//
//  VcFollowContainer.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 11/20/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class VcFollowContainer: PagerController, PagerDataSource {
    
    
    var titles: [String] = []
    let screenSize = ((UIScreen.mainScreen().bounds.width))/2
    
    var feednews:VcMasterNewsFeed!
    
    var follorwes : VcFollorwes_Likers_Search = VcFollorwes_Likers_Search()
    var following : VcFollorwes_Likers_Search = VcFollorwes_Likers_Search()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Follow"
        
//        let button = UIButton()
//        button.frame = CGRectMake(0, 0, 21, 31)
//        button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
//        button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
//        let barButton = UIBarButtonItem()
//        barButton.customView = button
//        navigationItem.leftBarButtonItem = barButton
        
        
        self.dataSource = self
        
        
        let storyboard = AppDelegate.storyboard
        
        
        follorwes = storyboard.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        follorwes.vcFollowContainer = self
        following = storyboard.instantiateViewControllerWithIdentifier("VcFollorwes_Likers_Search") as! VcFollorwes_Likers_Search
        following.vcFollowContainer = self
        
        self.setupPager(["Following","Followers"], tabImages: nil, tabControllers: [following, follorwes])
        
        customizeTab()
        // currentIndex
        
        
    }
    override func changeActiveTabIndex(newIndex: Int) {
        super.changeActiveTabIndex(newIndex)
        
        if (newIndex == 0)
        {
            following.FolloingMod()
           
        }
        else
        {
            follorwes.FollorwesMod()
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customizeTab() {
        indicatorColor = UIColor(hex: "#b379e7")
        tabsViewBackgroundColor = UIColor.whiteColor()
        //(colorLiteralRed: 145 / 255, green: 78 / 255, blue: 233 / 255, alpha: 1)
        //(rgb: 0x00AA00)
        contentViewBackgroundColor = UIColor.grayColor().colorWithAlphaComponent(0.32)
        
        //startFromSecondTab = false
        centerCurrentTab = true
        tabLocation = PagerTabLocation.Top
        tabHeight = 49
        tabOffset = 0
        tabWidth = screenSize
        fixFormerTabsPositions = false
        fixLaterTabsPosition = false
        animation = PagerAnimation.During
        selectedTabTextColor = UIColor(colorLiteralRed: 145 / 255, green: 78 / 255, blue: 233 / 255, alpha: 1)
        tabsTextColor = UIColor.lightGrayColor()
        tabsTextFont = UIFont(name: "HelveticaNeue-Bold", size: 15)!
        // tabTopOffset = 10.0
        // tabsTextColor = .purpleColor()
        
    }
}
