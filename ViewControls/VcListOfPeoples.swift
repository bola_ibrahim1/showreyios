//
//  VcListOfPeoples.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 11/19/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//
import MBProgressHUD
import UIKit
import DZNEmptyDataSet
class VcListOfPeoples: UIViewController, UITableViewDelegate,UITableViewDataSource ,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate{
    var ListOfPeoplesArray = [ModFollowersobj]()
    
    var vcFollowContainer :VcFollowContainer!
    var vcCommentsContainer : VcCommentsContainer!
    var vcFollorwes_Likers_Search : VcFollorwes_Likers_Search!
    
    var refreshControl: UIRefreshControl!
   
    
    
    @IBOutlet weak var ListofpeopleTable: UITableView!
    
   var ReloadTablefromWebService : ((Bool) -> ())! = nil
    var PagesCount = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
  
        ListofpeopleTable!.emptyDataSetSource = self
        ListofpeopleTable!.emptyDataSetDelegate = self
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(VcListOfPeoples.handleRefresh), forControlEvents: UIControlEvents.ValueChanged)
        ListofpeopleTable.addSubview(refreshControl)
        
        ListofpeopleTable.addInfiniteScrollWithHandler {
            
            (UITableView) in
            
            self.handleRefresh(true)
        }
        
        ListofpeopleTable.registerClass(TcListOfPeoples.self, forCellReuseIdentifier: "TcListOfPeoples")
        ListofpeopleTable.registerNib(UINib(nibName: "TcListOfPeoples",bundle: nil), forCellReuseIdentifier: "TcListOfPeoples")
   
    }
    
    override func viewWillAppear(animated: Bool) {
        ListofpeopleTable.reloadData()
    }
    
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "No People", attributes: [NSFontAttributeName : UIFont.boldSystemFontOfSize(18)
            ,NSForegroundColorAttributeName:UIColor.grayColor()])
    }
    
    func ReloadTable()
    {
        ListofpeopleTable.finishInfiniteScroll()
        refreshControl.endRefreshing()
        ListofpeopleTable.reloadData()
        
    }
    
    
    func handleRefresh(AddPagetoInfinit: Bool = false) {
        
      if (ReloadTablefromWebService != nil)
      {
        ReloadTablefromWebService(AddPagetoInfinit)
        }
       
        
    }
    
     func handleInfiniteScroll(index:Int, FromPagerOrRefresher:Bool)
     {
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       
        return ListOfPeoplesArray.count
       
       
        
    }


    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let ListOfPeoplesCell = tableView.dequeueReusableCellWithIdentifier("TcListOfPeoples") as! TcListOfPeoples
        
         ListOfPeoplesCell.setCell(ListOfPeoplesArray[indexPath.row])
        
        if indexPath.row % 2 == 0 {
            
            ListOfPeoplesCell.backgroundColor = UIColor(hex: "#f7f7f7")
            
        }else {
            ListOfPeoplesCell.backgroundColor = UIColor.whiteColor()
        }
        ListOfPeoplesCell.selectionStyle = UITableViewCellSelectionStyle.None
        
        
        ListOfPeoplesCell.parentview = self
        return ListOfPeoplesCell
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 80
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        var profile :VcMasterNewsFeed!
        if ((vcFollowContainer) != nil)
        {
            profile = vcFollowContainer.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
            profile.SetupForTimeLine("\(ListOfPeoplesArray[indexPath.row].Id!.integerValue)")
            
            vcFollowContainer.navigationController?.pushViewController(profile, animated: true)
        }else if ((vcCommentsContainer) != nil) {
            profile = vcCommentsContainer.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
            profile.SetupForTimeLine("\(ListOfPeoplesArray[indexPath.row].Id!.integerValue)")
            
            vcCommentsContainer.navigationController?.pushViewController(profile, animated: true)
        }else
        {
            profile = self.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
            profile.SetupForTimeLine("\(ListOfPeoplesArray[indexPath.row].Id!.integerValue)")
            
            vcFollorwes_Likers_Search.rootParent.navigationController?.pushViewController(profile, animated: true)
        }
        
        
        
    }
    
    
}
