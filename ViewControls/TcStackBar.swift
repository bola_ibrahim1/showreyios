//
//  TcStackBar.swift
//  ShowRey
//
//  Created by User on 1/23/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit

class TcStackBar: UITableViewCell, HorizontalStackBarChartDataSource, HorizontalStackBarChartDelegate  {

    
    @IBOutlet weak var usersCount: UILabel!
    @IBOutlet weak var secView: UIView!
    @IBOutlet weak var lblNoAns: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblYes: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblUsersNoAns: UILabel!
    @IBOutlet weak var lblUsersNo: UILabel!
    @IBOutlet weak var lblUsersYes: UILabel!
    @IBOutlet weak var shadeView: UIView!
    @IBOutlet weak var cotentView: UIView!
    @IBOutlet weak var outeltofviewNo: UIView!
    
    @IBOutlet weak var outeltofviewYes: UIView!
    var dataForChart:[Int] = []
    
    let header_height = 65
    
    var arrcolor: [UIColor] = []
    let arrValue: [NSNumber] = [6,2,2]
   // let chartView:HorizontalStackBarChart! = nil
    var refresh:Bool = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if dataForChart.count != 0 {
            arrcolor = [UIColor(hex: "#6716B1"), UIColor(hex: "#A25EE1"), UIColor(hex: "#BDBDBD")]
            
        }else{
            containerView.backgroundColor = UIColor(hex: "#BDBDBD")
            arrcolor = [UIColor(hex: "#BDBDBD"), UIColor(hex: "#BDBDBD"), UIColor(hex: "#BDBDBD")]
        }
        
        containerView.layer.cornerRadius = containerView.frame.size.width / 60
        secView.layer.cornerRadius = containerView.frame.size.width / 60
        
        cotentView.layer.cornerRadius = 3;
        cotentView.clipsToBounds = true;
        // shadow
        
        Styles.Shadow(self, target: cotentView, Radus: 1,shadowview: shadeView)
        
        createHorizontalStackChart()
    }
    
    
    //Mark: - CreateHorizontalChart
    
    func createHorizontalStackChart() {
        if !refresh {
            setNeedsLayout()
            refresh = true
        }else {
        
            let chartView = HorizontalStackBarChart(frame: CGRectMake(0, 0, self.containerView.frame.size.width, self.containerView.frame.size.height))
            chartView.dataSource = self
            chartView.delegate = self
            chartView.showLegend = false
            chartView.showValueOnBarSlice = false
            chartView.legendViewType = LegendTypeHorizontal
            chartView.showCustomMarkerView = true
            chartView.drawStackChart()
            self.containerView.addSubview(chartView)
        }
    }
    
    
    
    
    
    //Mark: - HorizontalStackBarChartDataSource
    
    func numberOfValuesForStackChart() -> Int {
        return dataForChart.count
    }
    
    func colorForValueInStackChartWithIndex(index: Int) -> UIColor! {
       
        return arrcolor[index]
    }
    
    func titleForValueInStackChartWithIndex(index: Int) -> String! {
        return ""
    }
    
    func valueInStackChartWithIndex(index: Int) -> NSNumber! {
        return dataForChart[index]
    }
    
    func customViewForStackChartTouchWithValue(value: NSNumber!) -> UIView! {
        let view = UIView()
        view.backgroundColor = UIColor.whiteColor()
        view.layer.cornerRadius = 4.0
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.lightGrayColor().CGColor
        view.layer.shadowColor = UIColor.blackColor().CGColor
        view.layer.shadowRadius = 2.0
        view.layer.shadowOpacity = 0.3
        let label = UILabel()
        label.font = UIFont.systemFontOfSize(12)
        label.textAlignment = .Center
        label.text = "\(value.integerValue) %"
        label.frame = CGRectMake(0, 0, 50, 30)
        view.addSubview(label)
        view.frame = label.frame
        return view
    }
    
    
    //Mark: - HorizontalStackBarChartDelegate
    func didTapOnHorizontalStackBarChartWithValue(value: String!) {
        print("Horizontal Stack Chart: \(value)")
    }

    
}
