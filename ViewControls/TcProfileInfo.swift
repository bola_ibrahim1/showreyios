//
//  TcProfileInfo.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/12/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD
import Hue
import FirebaseAnalytics
class TcProfileInfo: UITableViewCell {

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var btnAddFollow: UIButton!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var Verfied: UIImageView!
    
    @IBOutlet weak var NumQuestioned: UILabel!
   // @IBOutlet weak var NumFriendsQuestioned: UILabel!
    @IBOutlet weak var NumReplied: UILabel!
   // @IBOutlet weak var NumFriendsReplied: UILabel!
    @IBOutlet weak var NumInvites: UILabel!
   // @IBOutlet weak var NumTitle: UILabel!
    
    
    var Parent:VcMasterNewsFeed!
    var Profile:ModProfileInformation!
    
    var followed_ = false
    var Followed:Bool
    {
        get{return followed_}
        set{
            if newValue == true
            {
                btnAddFollow.setTitle("Followed", forState: .Normal)
                btnAddFollow.backgroundColor = UIColor(hex: "b379e7")
                btnAddFollow.setTitleColor(UIColor(hex: "EFEFF4"), forState: .Normal)
            }
            else
            {
                btnAddFollow.setTitle("Follow+", forState: .Normal)
                btnAddFollow.backgroundColor = UIColor.whiteColor()
                btnAddFollow.setTitleColor(UIColor(hex: "b379e7"), forState: .Normal)
            }
            followed_ = newValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgProfile.layer.cornerRadius = imgProfile.frame.width/2
        btnAddFollow.layer.cornerRadius = btnAddFollow.frame.height/2
        btnAddFollow.layer.borderWidth = 1
        btnAddFollow.layer.borderColor = UIColor(hex: "b379e7").CGColor
    }
    
    func setup(parent_:VcMasterNewsFeed,ProfilID:String)
    {
        Parent = parent_
        if ProfilID == User?.Id?.stringValue
        {
            btnAddFollow.hidden = true
        }
        else
        {
            btnAddFollow.hidden = false
        }
    }
    
    func SetData(profile_: ModProfileInformation?)
    {
        if(User?.Id == profile_?.Id && leftSideMenu != nil)
        {
            User = profile_
            leftSideMenu.label.text = User!.FullName!
            leftSideMenu.imageView.kf_setImageWithURL(NSURL(string: profile_!.ProfilePicture!), placeholderImage: UIImage(named: "Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
        }
        Profile = profile_
        
        Followed = Profile.FollowStatus == 1
        self.imgProfile.kf_setImageWithURL(NSURL(string: profile_!.ProfilePicture!), placeholderImage: UIImage(named: "Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        self.imgCover.kf_setImageWithURL(NSURL(string: profile_!.CoverPage!), placeholderImage: UIImage(named: "Slide-Menu-BG"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
        self.lblUserName.text = profile_?.FullName!
        self.userNameLbl.text = "@" + (profile_?.UserName!)!
        self.NumQuestioned.text = profile_?.Questioned?.stringValue
       // self.NumFriendsQuestioned.text = profile_?.QFriends?.stringValue
        self.NumReplied.text = profile_?.Replied?.stringValue
       // self.NumFriendsReplied.text = profile_?.RFriends?.stringValue
        self.NumInvites.text = profile_?.Followers?.stringValue // .Invited?.stringValue
       // self.NumTitle.text = profile_?.L2Invited?.stringValue
        
//        Verfied.hidden = false
        if Profile.AccountType == AccountType.Standard.rawValue
        {
            Verfied.image = UIImage(named: "Slide-Profile-Indicator")
        }
        else if Profile.AccountType == AccountType.Moderator.rawValue
        {
            Verfied.image = UIImage(named: "Profile-(moderator-symbol)")
        }
        else if Profile.AccountType == AccountType.Verified.rawValue
        {
             Verfied.image = UIImage(named: "right")
        }
    }

    @IBAction func btnThreeDots(sender: AnyObject)
    {
        if Profile == nil
        {
            return
        }
        // 1
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        let PointsPop = UIAlertAction(title: "Points", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            UIAlertView(title: "Points", message: self.Profile.Points?.stringValue, delegate: nil, cancelButtonTitle: "Ok").show()
        })
        
        // 2
        
        let savedPosts = UIAlertAction(title: "My Saved Posts", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            FIRAnalytics.logEventWithName("MySavedPosts", parameters: nil)
            let savedposts = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
            savedposts.SetupForMySavedPosts()
            self.Parent.navigationController?.pushViewController(savedposts, animated: true)
            
            //            let editScreen = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupEdit") as! VcGroupEdit
            //
            //            editScreen.getFollowees = self.groupParticipants
            //            editScreen.getSelectedFollowees = self.participantsId
            //            editScreen.gName = self.groupNameStr
            //            editScreen.navTitle = "Edit Group"
            //
            //            self.navigationController?.pushViewController(editScreen, animated: true)
            
        })
        
        
        let followingAndFollowers = UIAlertAction(title: "Friends", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let FollowContainer=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcFollowContainer")
            self.Parent.navigationController?.pushViewController(FollowContainer, animated: true)
            //  self.leaveGroup()
            
        })
        
        
        
        
        let editProfile = UIAlertAction(title: "Edit Profile", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
            let editScreen=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcEditProfileOne") as! VcEditProfileOne
            editScreen.profileMaster = self.Parent
            self.Parent.navigationController?.pushViewController(editScreen, animated: true)
            
        })
        
        let Report = UIAlertAction(title: "Report", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let report = self.Parent.storyboard?.instantiateViewControllerWithIdentifier("VcReport") as! VcReport
            report.come = .User
            report.userId = self.Profile.Id?.stringValue
            self.Parent.parent!.navigationController?.pushViewController(report, animated: true)
            
        })
        
        let postStatistics = UIAlertAction(title: "Posts Results", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let postStat = self.Parent.storyboard?.instantiateViewControllerWithIdentifier("VcPostStatistics") as! VcPostStatistics
            
            postStat.come = .profile
            self.Parent.parent!.navigationController?.pushViewController(postStat, animated: true)
            
        })

        if User?.Id == Profile.Id
        {
            optionMenu.addAction(editProfile)
            optionMenu.addAction(followingAndFollowers)
            optionMenu.addAction(PointsPop)
            optionMenu.addAction(savedPosts)
            optionMenu.addAction(postStatistics)
        }
        else
        {
            
            optionMenu.addAction(Report)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
            
        })
        
        optionMenu.addAction(cancelAction)
        
        // 5
        
        Parent.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func FollowUser(sender: AnyObject)
    {
        let RequestParameters : NSDictionary =
            [
                "FollowType":!Followed,
                "FollowerId":Profile!.Id!,
                "FollowerType":1,
               
                "UserId":(User?.Id)!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        MBProgressHUD.showHUDAddedTo(self.Parent.view, animated: true).label.text = "Following..."
        NetworkHelper.RequestHelper(nil, service: WSMethods.FollowUser, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                MBProgressHUD.hideHUDForView(self.Parent.view, animated: true)
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.Parent.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.Parent.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let FeedsResponse = ModResponse(json:JSON)
                        
                        if (FeedsResponse.ResultResponse == "0")
                        {
                            self.Followed = !self.Followed
                            // success
                            //  self.Parent.view.makeToast("User Followed")
                        }
                        else
                        {
                            //error
                            
                          
                        }
                    }
                }
                
            }
            , callbackDictionary: nil)
    }
    

        
}
