//
//  VcMasterNewsFeed.swift
//  ShowRey
//
//  Created by M-Hashem on 10/21/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD
import DZNEmptyDataSet
import SSImageBrowser
import FirebaseAnalytics

enum FeedMode
{
    case NewsFeed
    case TimeLine
    case SavedPosts
    case GroupPosts
    case SearchPosts
    case SinglePost
}
enum cellType
{
    case Feed
    case Tip
    case Ad
}
var Advertises = [ModAdvertise]()
class VcMasterNewsFeed: UIViewController ,UITableViewDataSource, UITableViewDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate,SSImageBrowserDelegate
{
    
    @IBOutlet weak var FeedsTable: UITableView!
    var indexes = [(celltype:cellType,index:Int)]()
    var newsfeeds = [ModFeedFeedobj]()
    var Tips = [ModTip]()
    
    var PagesCount = 1
    let refresher = UIRefreshControl()
    var DoneFirstFeedLoad = false
    var parent:UIViewController!
    var ParentGroup:VcGroupPageWall!
    
    var Mode:FeedMode = .NewsFeed
    
    var profileCell:TcProfileInfo!
    
    var profileID:String?
    var GroupID:String?
    var GroupIdTypeId:String?
    var keyword:String?
    
    var singleFeedId:Int = 0
    
    var showCreatePosts = true
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        loadAds()
        
        FeedsTable.registerClass(TcProfileInfo.self, forCellReuseIdentifier: "TcProfileInfo")
        FeedsTable.registerNib(UINib(nibName: "TcProfileInfo",bundle: nil), forCellReuseIdentifier: "TcProfileInfo")
        
        FeedsTable.registerClass(TcNewsFeed.self, forCellReuseIdentifier: "feedCell")
        FeedsTable.registerNib(UINib(nibName: "TcNewsFeed",bundle: nil), forCellReuseIdentifier: "feedCell")
        
        FeedsTable.registerClass(TcNewsFeed.self, forCellReuseIdentifier: "CreatPostCell")
        FeedsTable.registerNib(UINib(nibName: "TcCreatePostCell",bundle: nil), forCellReuseIdentifier: "CreatPostCell")
        
        //TcFeadsTip
        FeedsTable.registerClass(TcFeadsTip.self, forCellReuseIdentifier: "TcFeadsTip")
        FeedsTable.registerNib(UINib(nibName: "TcFeadsTip",bundle: nil), forCellReuseIdentifier: "TcFeadsTip")
        
        FeedsTable.registerClass(TcAd.self, forCellReuseIdentifier: "TcAd")
        FeedsTable.registerNib(UINib(nibName: "TcAd",bundle: nil), forCellReuseIdentifier: "TcAd")
        
        
        FeedsTable.rowHeight = UITableViewAutomaticDimension
        FeedsTable.estimatedRowHeight = 140
        ///
        if Mode != .SinglePost
        {
            FeedsTable.emptyDataSetSource = self
            FeedsTable.emptyDataSetDelegate = self
            
            
            FeedsTable.addInfiniteScrollWithHandler { (UITableView) in
                
                self.GetNewsFeeds(self.PagesCount + 1, FromPagerOrRefresher: true)
            }
            
            GetNewsFeeds(self.PagesCount, FromPagerOrRefresher: false)
            
            if (Mode == .NewsFeed)
            {
                if TcFeadsTip.lastDismisedTip == nil ||  NSDate().addDays(-1) > TcFeadsTip.lastDismisedTip
                {
                    GetTip()
                }
            }
            refresher.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
            FeedsTable.addSubview(refresher)
            
            FIRAnalytics.logEventWithName("GetNewFeeds", parameters: nil)
        }
        else
        {
            VcMasterNewsFeed.RefrehFeed(self, PostID: singleFeedId, callback: { (feed) in
                if let feed = feed
                {
                    self.newsfeeds.removeAll(keepCapacity:  false)
                    self.newsfeeds.append(feed)
                    self.FeedsTable.reloadData()
                }
            })
            
        }
        FeedsTable.reloadData()
    }
    func configForNoParent(Title:String)
    {
        parent = self
        navigationItem.title = Title
        
        
    }
    func slideMenuCustomBtn()
    {
        let button = UIButton()
        button.frame = CGRectMake(0, 0, 21, 31)
        button.setImage(UIImage(named: "IconSlide"), forState: .Normal)
        button.addTarget(self, action: #selector(SSASideMenu.presentLeftMenuViewController), forControlEvents: .TouchUpInside)
        let barButton = UIBarButtonItem()
        barButton.customView = button
        navigationItem.leftBarButtonItem = barButton
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        if Mode == .NewsFeed {
            parent.title = "Wall"
        }
        
        if Mode == .TimeLine {
            
            LoadProfile(profileID!)
            // self.SetupForTimeLine((User?.Id!.stringValue)!)
        }
        
    }
    
    //# MARK: - setups
    func SetupForSingleFeed(feedID:Int)
    {
        Mode = .SinglePost
        singleFeedId = feedID;
        configForNoParent("Post Details");
    }
    func SetupForTimeLine(userProfileID:String)
    {
        Mode = .TimeLine
        configForNoParent("Profile Page")
        
        //slideMenuCustomBtn()
        
        profileID = userProfileID
        //        LoadProfile(profileID!)
    }
    func SetupForMySavedPosts()
    {
        showCreatePosts = false
        Mode = .SavedPosts
        configForNoParent("My Saved Posts")
    }
    func SetupForGroups(GroupID_:String,Type:String,parent_:UIViewController)
    {
        Mode = .GroupPosts
        parent = parent_
        GroupID = GroupID_
        GroupIdTypeId = Type;
    }
    func SetupForSearch(KeyWord_:String)
    {
        Mode = .SearchPosts
        keyword = KeyWord_
        showCreatePosts = false
    }
    
    func searchWord(word:String) {
        keyword = word
        PagesCount = 1
        GetNewsFeeds(self.PagesCount, FromPagerOrRefresher:false)
    }
    //
    override func viewDidLayoutSubviews() {
        if newsfeeds.count > 0
        {
            FeedsTable.reloadData();
        }
    }
    func refresh()
    {
        PagesCount = 1
        
        GetNewsFeeds(self.PagesCount, FromPagerOrRefresher:true)
    }
    func scrollToTop()
    {
        FeedsTable.setContentOffset(CGPointZero, animated:true)
    }
    func HidePost(feedobj:ModFeedFeedobj)
    {
        let indexToRemove = newsfeeds.indexOf(feedobj)
        if indexToRemove != nil
        {
            newsfeeds.removeAtIndex(indexToRemove!)
            FeedsTable.beginUpdates()
            FeedsTable.deleteRowsAtIndexPaths([NSIndexPath(forItem: indexToRemove!, inSection: 1)], withRowAnimation: .Fade)
            FeedsTable.endUpdates()
            RefreshIndexes()
        }
        //FeedsTable.reloadData()
    }
    func HideTip(tipObj:ModTip)
    {
        let indexToRemove = Tips.indexOf(tipObj)
        if indexToRemove != nil
        {
            if let rowindex = indexes.indexOf({ (indxOb:(celltype: cellType, index: Int)) -> Bool in
                return indxOb.celltype == cellType.Tip && indxOb.index == indexToRemove!
            })
            {
                Tips.removeAtIndex(rowindex)
                FeedsTable.beginUpdates()
                FeedsTable.deleteRowsAtIndexPaths([NSIndexPath(forItem: rowindex, inSection: 1)], withRowAnimation: .Fade)
                FeedsTable.endUpdates()
                RefreshIndexes()
            }
        }
        //FeedsTable.reloadData()
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "No Posts Found", attributes: [NSFontAttributeName : UIFont.boldSystemFontOfSize(18)
            ,NSForegroundColorAttributeName:UIColor.grayColor()])
    }
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        return newsfeeds.count == 0 && (DoneFirstFeedLoad||Mode == .SearchPosts)
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if Mode == .SinglePost
        {
            return 1
        }
        return 2
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if Mode == .SinglePost
        {
            return UITableViewAutomaticDimension
        }
        if(indexPath.section == 0)
        {
            if Mode == .TimeLine
            {
                if indexPath.row == 0
                {
                    return 272
                }
                else
                {
                    return 70
                }
            }
            else
            {
                return 70
            }
            
        }// create post
        else
        {
            let index = indexes[indexPath.row]
            
            if index.celltype == .Ad
            {
                return 200
            }
            
            return UITableViewAutomaticDimension
        
        }
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Mode == .SinglePost
        {
            return newsfeeds.count
        }
        if(section == 0)
        {
            let creatNum = showCreatePosts ? 1 : 0
            let profilecell = Mode == .TimeLine ? 1 : 0
            return creatNum + profilecell
        }// and profil cells will be in section 0 too
        return newsfeeds.count // + Tips.count;
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        if Mode == .SinglePost
        {
            let feedcell = tableView.dequeueReusableCellWithIdentifier("feedCell") as! TcNewsFeed
            feedcell.SetData(self,feed: newsfeeds[0])
            
            return feedcell
        }
        if(indexPath.section == 0)
        {
            if Mode == .TimeLine
            {
                if indexPath.row == 0
                {
                    profileCell = tableView.dequeueReusableCellWithIdentifier("TcProfileInfo") as! TcProfileInfo
                    profileCell.setup(self,ProfilID: profileID!)
                    return profileCell
                }
                else
                {
                    let CreatPostcell = tableView.dequeueReusableCellWithIdentifier("CreatPostCell") as! TcCreatePostCell
                    CreatPostcell.parent = self;
                    return CreatPostcell
                }
            }
            else
            {
                let CreatPostcell = tableView.dequeueReusableCellWithIdentifier("CreatPostCell") as! TcCreatePostCell
                CreatPostcell.parent = self;
                return CreatPostcell
            }
        }
        else
        {
            let index = indexes[indexPath.row]
            
            if index.celltype == .Feed
            {
                let feedcell = tableView.dequeueReusableCellWithIdentifier("feedCell") as! TcNewsFeed
                feedcell.SetData(self,feed: newsfeeds[index.index])
                
                return feedcell
            }
            else if index.celltype == .Tip
            {
                //                if Mode == .TimeLine
                //                {
                let tipCell = tableView.dequeueReusableCellWithIdentifier("TcFeadsTip") as! TcFeadsTip
                tipCell.SetData(self,data: Tips[index.index])
                
                return tipCell
                //                }
                //                else
                //                {
                //                    return UITableViewCell()
                //                }
            }
            else
            {
                let adcell = tableView.dequeueReusableCellWithIdentifier("TcAd") as! TcAd
                print(" 🤔index=\(index) with index = \(index.index) and object = \(Advertises[index.index])")
                adcell.Setup(Advertises[index.index])
                
                return adcell
            }
        }
    }
    func RefreshIndexes()
    {
        indexes.removeAll(keepCapacity: false)
        let FirstIsAd = true;
        let TipEvryNfeeds = 5,AdEvryNfeeds = AppConfiguration!.ConfigSettings!.NoOfItemBetweenAds!.integerValue;
        var FeedsPassedPerTip = 0,FeedsPassedPerAd = 0
        var TipsPassed = 0,AdsPassed = 0
        var FeedsPassed = 0
        
        let addsCount = (newsfeeds.count / AdEvryNfeeds)
        
        for _ in 0 ... (newsfeeds.count +  addsCount)//(newsfeeds.count + Tips.count + addsCount)
        {
            if (FeedsPassedPerTip == TipEvryNfeeds || (FirstIsAd && FeedsPassedPerTip == 0 && Tips.count > 0)) && Tips.count > TipsPassed
            {
                //indexes.append((celltype: .Tip, index: TipsPassed))
                FeedsPassedPerTip = 0
                TipsPassed += 1
            }
            else if FeedsPassedPerAd == AdEvryNfeeds && Advertises.count > AdsPassed
            {
                indexes.append((celltype: .Ad, index: AdsPassed))
                FeedsPassedPerAd = 0
                
                AdsPassed += 1
                if AdsPassed == Advertises.count
                {
                    AdsPassed = 0
                }
            }
            else
            {
                indexes.append((celltype: .Feed, index:FeedsPassed))
                FeedsPassedPerTip += 1
                FeedsPassedPerAd += 1
                FeedsPassed += 1
            }
        }
    }
    func GetTip()
    {
        let RequestParameters:NSDictionary =
            [
                "UserId" : User!.Id!
        ]
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetTipOfTheDay, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            //print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // //print("JSON: \(JSON)")
                        let FeedsResponse = ModTipResponse(json:JSON)
                        
                        //                        let t = ModTip(); t.Text = "adsaASsd SAD s fEF we"
                        //                        t.Title = "tip title"; t.Date = "5/6/2010"
                        //                        self.Ads.append(t)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            //print("--- error loading tips code :\(FeedsResponse.ResultResponse)")
                        }
                        else
                        {
                            self.Tips.removeAll(keepCapacity: false)
                            self.Tips.append(FeedsResponse.Tipob!)
                            self.RefreshIndexes()
                            self.FeedsTable.reloadData()
                        }
                    }
                }
            }
            , callbackDictionary: nil)
        
    }
    func GetNewsFeeds(index:Int, FromPagerOrRefresher:Bool)
    {
        var RequestParameters = NSDictionary()
        
        var service = ""
        
        switch Mode
        {
        case .NewsFeed:
            service = WSMethods.GetNewsFeed
            RequestParameters =
                [
                    "PageIndex" : index,
                    "PageSize" : 10,
                    "ProfileUserId" : User!.Id!,
                    "UserId" : User!.Id!
            ]
            break
        case .TimeLine:
            service = WSMethods.GetUserTimeLine
            RequestParameters =
                [
                    "PageIndex" : index,
                    "PageSize" : 10,
                    "ProfileUserId" : User!.Id!,
                    "UserId" :profileID!
            ]
            break
        case .SavedPosts:
            service = WSMethods.GetMySavedPosts
            RequestParameters =
                [
                    "PageIndex" : index,
                    "PageSize" : 10,
                    "ProfileUserId" : User!.Id!,
                    "UserId" : User!.Id!
            ]
            break
            
        case .GroupPosts:
            service = WSMethods.GetGroupPosts
            RequestParameters =
                [
                    "GroupId" : GroupID!,
                    "GroupIdTypeId" : GroupIdTypeId!,
                    "PageIndex" : index,
                    "PageSize" : 10,
                    
                    "UserId" : User!.Id!
            ]
            break
        case .SearchPosts:
            //            if keyword!.characters.count == 0 {return}
            service = WSMethods.PostsSearch
            RequestParameters =
                [
                    "Keyword" : keyword!,
                    "PageIndex" : index,
                    "PageSize" : 10,
                    "ProfileUserId" : User!.Id!,
                    "UserId" : User!.Id!
            ]
            break
            
        default : break //print("-- warning unhandeled feedMode case"); break
            
            
        }
        
        var brog : MBProgressHUD!
        if !FromPagerOrRefresher
        {
            brog = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            brog.label.text = "Loading..."
        }
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: service, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                if(index == 1)
                { self.newsfeeds.removeAll(keepCapacity: false)}
                
                self.DoneFirstFeedLoad = true
                if FromPagerOrRefresher
                {
                    self.FeedsTable.finishInfiniteScroll()
                    self.refresher.endRefreshing()
                }
                else
                {
                    brog.hideAnimated(true)//.hideHUDForView(self.view, animated: true)
                    //  self.refresher.endRefreshing()
                }
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            //print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // //print("JSON: \(JSON)")
                        let FeedsResponse = ModNewsFeed(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            //print("--- error loading feed code :\(FeedsResponse.ResultResponse)")
                        }
                        else
                        {
                            if(FeedsResponse.ListCount?.integerValue > 0)
                            {self.PagesCount += 1}
                            
                            self.newsfeeds.appendContentsOf(FeedsResponse.Feedobj!)
                            self.RefreshIndexes()
                            self.FeedsTable.reloadData()
                        }
                    }
                }
            }
            , callbackDictionary: nil)
        
    }
    static func RefrehFeed(target:UIViewController,PostID:Int,feedmaster:VcMasterNewsFeed?=nil,callback:(feed:ModFeedFeedobj?)->())
    {
        MBProgressHUD.showHUDAddedTo(target.view, animated: true).label.text = "Loading..."
        let RequestParameters:NSDictionary =
            [
                "PostId" : PostID,
                "UserId" : User!.Id!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetPostByPostId, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(target.view, animated: true)
                
                var feedobj:ModFeedFeedobj?
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            target.view.makeToast("No Internet connection")
                        }
                        else
                        {
                            target.view.makeToast("An error has been occurred")
                            //print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // //print("JSON: \(JSON)")
                        let FeedsResponse = ModNewsFeed_Single(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            callback(feed: nil)
                        }
                        else
                        {
                            feedobj = FeedsResponse.Feedobj
                            if let feedmaster = feedmaster
                            {
                                
                                let index = feedmaster.newsfeeds.indexOf({ (feed) -> Bool in
                                    return feed.Id == FeedsResponse.Feedobj?.Id
                                })
                                if index != nil
                                {
                                    feedmaster.newsfeeds[index!] = FeedsResponse.Feedobj!
                                    feedmaster.FeedsTable.beginUpdates()
                                    feedmaster.FeedsTable.reloadRowsAtIndexPaths([NSIndexPath(forItem: index!, inSection: 1)], withRowAnimation: .Fade)
                                    feedmaster.FeedsTable.endUpdates()
                                }
                            }
                        }
                    }
                }
                callback(feed: feedobj)
                //self.FeedsTable.reloadData()
            }
            , callbackDictionary: nil)
    }
    
    func loadAds()
    {
        
        //    MBProgressHUD.showHUDAddedTo(self.view, animated: true).label.text = "Loading..."
        let RequestParameters:NSDictionary =
            [
                "CountryCode" : "JO",
                
                ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetAdvertisement, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                //   MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            //  self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            //     self.view.makeToast("An error has been occurred")
                            //print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // //print("JSON: \(JSON)")
                        let FeedsResponse = ModAdvertisementResponse(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            
                        }
                        else
                        {
                            Advertises.removeAll(keepCapacity: false)
                            Advertises.appendContentsOf(FeedsResponse.AdsList!);
                            self.RefreshIndexes()
                            self.FeedsTable.reloadData()
                        }
                    }
                }
                
            }
            , callbackDictionary: nil)
    }
    func LoadProfile(ProfilID:String) {
        VcLogin.GetProfile(self,ForClient: false, UserID: ProfilID) { (success, profile) in
            if success
            {
                if let profileCell = self.profileCell
                {
                    profileCell.SetData(profile)
                }
                else
                {
                    //print("----- warning  profileCell is nil")
                }
                
            }
        }
    }
    
    
}
