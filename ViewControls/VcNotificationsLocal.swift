//
//  VcNotificationsLocal.swift
//  ShowRey
//
//  Created by M-Hashem on 12/18/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIScrollView_InfiniteScroll
import UIKit
import DZNEmptyDataSet
import MBProgressHUD
import FirebaseAnalytics

class VcNotificationsLocal: UIViewController,UITableViewDataSource, UITableViewDelegate,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate
{
    
    @IBOutlet weak var NotificationsTable: UITableView!
    var notifications:[ModNotification] = [ModNotification]()
    var PagesCount = 1
    let refresher = UIRefreshControl()
    var DoneFirstLoad = false
    var parent:UIViewController!
    var NotificationTap:VTapItem!
    
    static var LastReadNotification:Int
    {
        get
        {
            let val = NSUserDefaults.standardUserDefaults().objectForKey("LastReadNotification")
            if val == nil {return 1}
            return (val as? Int)!
        }
        set
        {
            
            let userDefaults = NSUserDefaults.standardUserDefaults()
            userDefaults.setValue(newValue, forKey: "LastReadNotification")
            userDefaults.synchronize();
        }
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        parent.title = "Notifications"
        NotificationsTable.registerClass(TcNotification.self, forCellReuseIdentifier: "TcNotification")
        NotificationsTable.registerNib(UINib(nibName: "TcNotification",bundle: nil), forCellReuseIdentifier: "TcNotification")
        
        NotificationsTable.dataSource = self
        NotificationsTable.delegate = self
        NotificationsTable.emptyDataSetSource = self
        NotificationsTable.emptyDataSetDelegate = self
        NotificationsTable.addInfiniteScrollWithHandler
            { (UITableView) in
                self.GetNotifications(self.PagesCount + 1, fromRefresher: true)
        }
        
        GetNotifications(self.PagesCount, fromRefresher: false)
        
        refresher.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
        NotificationsTable.addSubview(refresher)
        FIRAnalytics.logEventWithName("Notifications", parameters: nil)
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        parent.title = "Notifications"
    }

    
    override func viewDidAppear(animated: Bool) {
       VcNotificationsLocal.CalculateUnreadNotifications(NotificationTap)
    }

    static func CalculateUnreadNotifications(NotificationTap:VTapItem)
    {
        var RequestParameters = NSDictionary()
        
        RequestParameters =
        [
                "LastNotificationId" :VcNotificationsLocal.LastReadNotification ?? 1,
                "UserId" : User!.Id!
        ]
       // var brog : MBProgressHUD!
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetNotificationNumber, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                // brog.hideAnimated(true)//.hideHUDForView(self.view, animated: true)
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                          
                            
                        }
                        else
                        {
                         
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    print("🙆 ount is \(response) json = \(response.result) val = \(response.result.value)")
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let NotiNumb = ModNotiNum(json:JSON)
                        
                        if (NotiNumb.ResultResponse != "0")
                        {
                            print("--- error loading NotiNum code :\(NotiNumb.ResultResponse)")
                        }
                        else
                        {
                            NotificationTap.setNum((NotiNumb.NotiNo?.integerValue)!)
                            
                        }
                    }
                }
            }
            , callbackDictionary: nil)
        
    }
    func refresh()
    {
        PagesCount = 1
        
        GetNotifications(self.PagesCount, fromRefresher:  true)
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        NotificationsTable.separatorColor = UIColor.clearColor()
        NotificationsTable.backgroundColor = UIColor(hex: "CDCDCD")
        return NSAttributedString(string: "No Notifications Found", attributes: [NSFontAttributeName : UIFont.boldSystemFontOfSize(18)
            ,NSForegroundColorAttributeName:UIColor.grayColor()])
    }
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        return notifications.count == 0 && DoneFirstLoad
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 71;
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return notifications.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let notiCell = tableView.dequeueReusableCellWithIdentifier("TcNotification") as! TcNotification
        let noti = notifications[indexPath.row]
        if Int(noti.Id!)! <= VcNotificationsLocal.LastReadNotification
        {
           noti.isread = true
        }
        notiCell.SetData(noti)
        return notiCell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath)
    {
        let noti = notifications[indexPath.row]
        if Int(noti.Id!)! > VcNotificationsLocal.LastReadNotification
        {
            VcNotificationsLocal.LastReadNotification = Int(noti.Id!)!
            NotificationsTable.reloadData()
        }
        VcNotificationsLocal.HaandleNotifications(noti,comFromPushNotifcationFlag :false,parent: parent,userID: noti.UserProfile?.Id?.stringValue)
        return
        
        
//        if noti.Type == NotificationType.Following_you.rawValue
//        {// his wall
//            let Feedsmaster = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
//            let targerUserId = noti.UserProfile?.Id?.stringValue
//            
//            Feedsmaster.SetupForTimeLine(targerUserId!)
//            parent.navigationController?.pushViewController(Feedsmaster, animated: true)
//            return
//        }
//        else if noti.Type == NotificationType.posted_on_your_Wall.rawValue
//            || noti.Type == NotificationType.tagged_you_in_apost.rawValue
//            || noti.Type == NotificationType.liked_your_post.rawValue
//        {// regular post
//            VcNotificationsLocal.ToRegularPost((noti.ObjId?.integerValue)!, parent: parent)
//            
//            return
//        }
//        else if  noti.Type == NotificationType.fashion_Posted_on_your_Wall.rawValue
//        || noti.Type == NotificationType.tagged_you_in_afashion_Post.rawValue
//        || noti.Type == NotificationType.like_your_Fashon_Post.rawValue
//        {// fashon post
//            VcNotificationsLocal.ToFasionPost((noti.ObjId?.integerValue)!,parent: self.parent)
//                        return
//        }
//        else if noti.Type == NotificationType.added_you_in_aGroup.rawValue
//        {// to groups
//            let groups=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcGetGroups")
//            self.parent.navigationController?.pushViewController(groups, animated: true);
//            return
//        }
//        else if noti.Type == NotificationType.commented_on_your_post.rawValue
//            || noti.Type == NotificationType.liked_your_comment.rawValue
//            || noti.Type == NotificationType.replied_to_your_comment.rawValue
//            || noti.Type == NotificationType.mentioned_you_in_acomment.rawValue
//            
//            || noti.Type == NotificationType.commented_on_your_Fashon_Post.rawValue
//            || noti.Type == NotificationType.like_your_comment_Fashon_Post.rawValue
//            || noti.Type == NotificationType.replied_to_your_fashion_Post.rawValue
//            || noti.Type == NotificationType.mentioned_you_in_aFashion_Post.rawValue
//
//        { // post comments
//            let commentsController = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
//            commentsController.MyScreenMod = .All
//            commentsController.InitMod(0, ObjId: (noti.ObjId?.integerValue)!, PostType: 1)
//            
//            self.parent.navigationController?.pushViewController(commentsController, animated: true)
////            commentsController.Commentblock = { (id) in
////                
////                VcMasterNewsFeed.RefrehFeed(self.Parent, PostID: id,feedmaster: self.Parent , callback: { (feed) in })
////            }
//            return
//        }
//        else if  noti.Type == NotificationType.mentioned_you_in_aspecial_offer_comments.rawValue
//            || noti.Type == NotificationType.like_your_special_offer_comment.rawValue
//            || noti.Type == NotificationType.replied_to_your_special.rawValue
//            || noti.Type == NotificationType.mentioned_you_in_aspecial_offer_comments.rawValue
//        {
//            let commentsController = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
//            commentsController.MyScreenMod = .All
//            commentsController.InitMod(0, ObjId: (noti.ObjId?.integerValue)!, PostType: 1,IsItSpecialOffer:  true)
//            
//            self.parent.navigationController?.pushViewController(commentsController, animated: true)
//            return
//        }
//        else if noti.Type == NotificationType.new_special_offer_posted.rawValue
//        {// special Offers page
//            let CvOffers_=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("CvOffers")
//             parent.navigationController?.pushViewController(CvOffers_, animated: true)
//        }
//        else if noti.Type == NotificationType.like_your_special_offer.rawValue
//        {// special offer
//            
//            CvOffers.RefreshOffer(self, PostID: (noti.ObjId?.integerValue)!, completion: { (succ) in
//                if let succ = succ
//                {
//                    let offersDetails = VcOffersDetails(nibName: "VcOffersDetails", bundle: nil)
//                    
//                    offersDetails.setData(self, data: succ)
//                    self.parent.navigationController?.pushViewController(offersDetails, animated: true)
//                }
//            })
//        }
//        
//        let Feedsmaster = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
//        let targerUserId = (noti.Type == NotificationType.posted_on_your_Wall.rawValue
//                            || noti.Type == NotificationType.fashion_Posted_on_your_Wall.rawValue)
//                        ? User?.Id?.stringValue : (noti.UserProfile?.Id?.stringValue)!
//     
//        Feedsmaster.SetupForTimeLine(targerUserId!)
//        parent.navigationController?.pushViewController(Feedsmaster, animated: true)
        
    }
   
    func GetNotifications(index:Int, fromRefresher:Bool = false)
    {
        var RequestParameters = NSDictionary()
        
            RequestParameters =
                [
                    "PageIndex" : index,
                    "PageSize" : 20,
                    "LastNotificationId" : VcNotificationsLocal.LastReadNotification ?? 1,
                    "UserId" : User!.Id!
            ]
        var brog : MBProgressHUD!
        if !fromRefresher
        {
            brog = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            brog.label.text = "Loading..."
        }
        
        let RequestParametersString = OauthInjector.UpdateDictionary(RequestParameters)
      
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetNotification, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                if(index == 1)
                { self.notifications.removeAll(keepCapacity: false)}
                
                self.DoneFirstLoad = true
                if fromRefresher
                {
                    self.NotificationsTable.finishInfiniteScroll()
                    self.refresher.endRefreshing()
                }
                else
                {
                    brog.hideAnimated(true)//.hideHUDForView(self.view, animated: true)
                    //  self.refresher.endRefreshing()
                }
                
                if(response.result.isFailure)
                {
                    
                    if let networkError = response.result.error
                    {
                        if (networkError.code == -1009)
                        {
                            self.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value
                    {
                        // print("JSON: \(JSON)")
                        let FeedsResponse = ModNotificationsResponse(json:JSON)
                        
                        if (FeedsResponse.ResultResponse != "0")
                        {
                            print("--- error loading notifications code :\(FeedsResponse.ResultResponse)")
                        }
                        else
                        {
                            if(FeedsResponse.ListCount?.integerValue > 0)
                            {self.PagesCount += 1}
                            
                            self.notifications.appendContentsOf(FeedsResponse.Notifications!)
                            self.NotificationsTable.reloadData()
                            
                        }
                    }
                }
            }
            , callbackDictionary: nil)
        
    }
   static  func doRouting(noti:ModNotification,parent: UIViewController, userID:String?)
    {
        
        if noti.Type == NotificationType.Following_you.rawValue
        {// his wall
            let Feedsmaster = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
            let targerUserId = userID
            
            Feedsmaster.SetupForTimeLine(targerUserId!)
            parent.navigationController?.pushViewController(Feedsmaster, animated: true)
            return
        }
        else if noti.Type == NotificationType.posted_on_your_Wall.rawValue
            || noti.Type == NotificationType.tagged_you_in_apost.rawValue
            || noti.Type == NotificationType.liked_your_post.rawValue
        {// regular post
            VcNotificationsLocal.ToRegularPost((noti.ObjId?.integerValue)!, parent: parent)
            
            return
        }
        else if  noti.Type == NotificationType.fashion_Posted_on_your_Wall.rawValue
            || noti.Type == NotificationType.tagged_you_in_afashion_Post.rawValue
            || noti.Type == NotificationType.like_your_Fashon_Post.rawValue
        {// fashon post
            VcNotificationsLocal.ToFasionPost((noti.ObjId?.integerValue)!, parent: parent)
            return
        }
        else if noti.Type == NotificationType.added_you_in_aGroup.rawValue
        {// to groups
            
            
            let groups=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcGroupPageWall") as! VcGroupPageWall
            groups.notifiedGroupId = (noti.ObjId?.integerValue)!
            parent.navigationController?.pushViewController(groups, animated: true);
            return
            
            
            //            let groups=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcGetGroups")
            //            parent.navigationController?.pushViewController(groups, animated: true);
            //            return
        }
        else if noti.Type == NotificationType.commented_on_your_post.rawValue
            || noti.Type == NotificationType.liked_your_comment.rawValue
            || noti.Type == NotificationType.mentioned_you_in_acomment.rawValue
            
        { // post comments
            let commentsController = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
            commentsController.MyScreenMod = .All
            commentsController.InitMod(0, ObjId: (noti.ObjId?.integerValue)!, PostType: 1)
            
            parent.navigationController?.pushViewController(commentsController, animated: true)
            //commentsController.Commentblock = { (id) in
            //
            //  VcMasterNewsFeed.RefrehFeed(self.Parent, PostID: id,feedmaster: self.Parent , callback: { (feed) in })
            //
            //}
            return
        }
            
            
            
            
        else if
            
            noti.Type == NotificationType.replied_to_your_comment.rawValue
                || noti.Type == NotificationType.replied_to_your_fashion_Post.rawValue
            
            
        { // post comments
            let commentsController = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcComments") as! VcComments
            
            
            
            commentsController.IsItSpecialOffer = false
            commentsController.ObjId = (noti.ObjId?.integerValue)!
            commentsController.PostType = 1
            commentsController.ScreenMod = 0
            commentsController.IsitReplay = true
            //            commentsController.parentView = ModCommentsFollowersobj()
            //            commentsController.ParentComment = GmodCommentsFollowersobj
            
            
            
            
            
            
            // commentsController.InitMod(0, ObjId: (noti.ObjId?.integerValue)!, PostType: 1)
            
            parent.navigationController?.pushViewController(commentsController, animated: true)
            //commentsController.Commentblock = { (id) in
            //
            //  VcMasterNewsFeed.RefrehFeed(self.Parent, PostID: id,feedmaster: self.Parent , callback: { (feed) in })
            //
            //}
            return
        }
            
            
            
        else if noti.Type == NotificationType.commented_on_your_Fashon_Post.rawValue
            || noti.Type == NotificationType.like_your_comment_Fashon_Post.rawValue
            || noti.Type == NotificationType.mentioned_you_in_aFashion_Post.rawValue
            
        { // post comments
            
            
            
            //            let commentsController = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
            //            commentsController.MyScreenMod = .All
            //            commentsController.InitMod(0, ObjId: (noti.ObjId?.integerValue)!, PostType: 1)
            //
            //            parent.navigationController?.pushViewController(commentsController, animated: true)
            
            
            VcNotificationsLocal.ToFasionPost((noti.ObjId?.integerValue)!, parent: parent)
            return
            
        }
            
            
            
        else if  noti.Type == NotificationType.mentioned_you_in_aspecial_offer_comments.rawValue
            || noti.Type == NotificationType.like_your_special_offer_comment.rawValue
            || noti.Type == NotificationType.replied_to_your_special.rawValue
            || noti.Type == NotificationType.mentioned_you_in_aspecial_offer_comments.rawValue
        {
            let commentsController = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcCommentsContainer") as! VcCommentsContainer
            commentsController.MyScreenMod = .All
            commentsController.InitMod(0, ObjId: (noti.ObjId?.integerValue)!, PostType: 1,IsItSpecialOffer:  true)
            
            parent.navigationController?.pushViewController(commentsController, animated: true)
            return
        }
        else if noti.Type == NotificationType.new_special_offer_posted.rawValue
        {// special Offers page
            let CvOffers_=AppDelegate.storyboard.instantiateViewControllerWithIdentifier("CvOffers")
            parent.navigationController?.pushViewController(CvOffers_, animated: true)
            return
        }
        else if noti.Type == NotificationType.like_your_special_offer.rawValue
        {// special offer
            
            CvOffers.RefreshOffer(parent, PostID: (noti.ObjId?.integerValue)!, completion: { (succ) in
                if let succ = succ
                {
                    let offersDetails = VcOffersDetails(nibName: "VcOffersDetails", bundle: nil)
                    
                    offersDetails.setData(parent, data: succ)
                    parent.navigationController?.pushViewController(offersDetails, animated: true)
                }
            })
            
            return
        }
        
        //            let Feedsmaster = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        //            let targerUserId = User?.Id?.stringValue
        //
        //            Feedsmaster.SetupForTimeLine(targerUserId!)
        //            parent.navigationController?.pushViewController(Feedsmaster, animated: true)
        
  
    }
    static func HaandleNotifications(noti:ModNotification,comFromPushNotifcationFlag :Bool,parent: UIViewController, userID:String?)
    {
        
         switch UIApplication.sharedApplication().applicationState {
         case .Active:
            
            if (comFromPushNotifcationFlag == true)
            {
                
                
                
         let alert = UIAlertController(title: noti.title ?? "Push Notification", message: noti.message, preferredStyle: UIAlertControllerStyle.Alert)
         let ok = UIAlertAction(title: "Show", style: UIAlertActionStyle.Default , handler:
         {(action: UIAlertAction) -> Void in
            
          self.doRouting(noti, parent: parent, userID: userID)
         })
         let cancel = UIAlertAction(title: "Cancel", style: .Default, handler: {(action: UIAlertAction) -> Void in
         alert.dismissViewControllerAnimated(true, completion: { _ in })
         })
         alert.addAction(ok)
         alert.addAction(cancel)
       
         parent.presentViewController(alert, animated: true, completion: nil)
            }
            else
            {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                appDelegate.comFromPushNotifcationFlag = false
                
                
                doRouting(noti, parent: parent, userID: userID)}
          break
         default:
         doRouting(noti, parent: parent, userID: userID)
         }
 
    }
    static func ToRegularPost(FeedID:Int,parent:UIViewController)
    {
        let Feedsmaster = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        
        Feedsmaster.SetupForSingleFeed(FeedID)
        parent.navigationController?.pushViewController(Feedsmaster, animated: true)
    }
    static func ToFasionPost(FeedID:Int,parent:UIViewController)
    {
        VcMasterNewsFeed.RefrehFeed(parent, PostID: FeedID, callback: { (feed) in
            if let feed = feed
            {
//                if feed.AttachedFiles != nil
//                {
                    let fasionPostView = AppDelegate.storyboard.instantiateViewControllerWithIdentifier("TvFashionPost") as! TvFashionPost
                    fasionPostView.mainFeed = feed
                    fasionPostView.Parent = parent;
                    parent.navigationController?.pushViewController(fasionPostView, animated: true)
//                }
//                else
//                {
//                    ToRegularPost(FeedID,parent: parent)
                    
//                }
            }
        })
    }
}


