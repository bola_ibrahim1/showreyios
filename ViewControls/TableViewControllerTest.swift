//
//  TableViewControllerTest.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/12/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit

class TableViewControllerTest: UITableViewController {

    @IBOutlet var profileTableInfo: UITableView!
    @IBOutlet var profileTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        profileTableInfo.registerClass(kk.self, forCellReuseIdentifier: "kk")
        profileTableInfo.registerNib(UINib(nibName: "kk",bundle: nil), forCellReuseIdentifier: "kk")
        
       // profileTable.registerClass(TcProfileTabs.self, forCellReuseIdentifier: "TcProfileTabs")
       // profileTable.registerNib(UINib(nibName: "TcProfileTabs",bundle: nil), forCellReuseIdentifier: "TcProfileTabs")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 373
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       // let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let cell : kk  = tableView.dequeueReusableCellWithIdentifier("kk", forIndexPath: indexPath) as! kk

        cell.Parent = self
        
       //let cell : TcProfileInfo  = tableView.dequeueReusableCellWithIdentifier("TcProfileInfo") as! TcProfileInfo
        
        
     //   let cell: CcImageGallery = collectionView.dequeueReusableCellWithReuseIdentifier("CcImageGallery", forIndexPath: indexPath) as! CcImageGallery

        // Configure the cell...
        
        return cell
    }

    /*
     func tableView(tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
    }
 */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
