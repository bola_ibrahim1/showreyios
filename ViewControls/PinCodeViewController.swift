//
//  PinCodeViewController.swift
//  ShowRey
//
//  Created by Abd Aboudi on 11/14/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit
import EGFloatingTextField
import Alamofire
import MBProgressHUD
import Toast_Swift
import SwiftyJSON
import Firebase
import FirebaseCrash
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import Crashlytics

class PinCodeViewController: UIViewController {

    @IBOutlet weak var pinCodeTextField: EGFloatingTextField!
    
    var isFromLogin = false
    var isFromSocialRegistration = false
    
    var passedEmail = ""
    var passedFullName = ""
    var passedUserName = ""
    var passedInvitationCode = ""
    var passedPhoneNumber = ""
    var passedInstagramId = ""
    var passedInstagramProfilePictureURL = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        setupPinCodeView()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions

    @IBAction func confirmButtonAction(sender: AnyObject) {
        
        if(pinCodeTextField.text == "")
        {
            self.view.makeToast("Please Enter Pin Code")
            
            return
        }
        
        //AA : To check if the user has came from regular/social login/registartion
        if isFromSocialRegistration {
            
        }else{
            
            if isFromLogin {
                
                Login(passedPhoneNumber)
                
            }else{
                
                callRegisterApi()
                
            }

        }
        
    }

    // MARK: - Helping Methods
    
    func setupPinCodeView() {
        
        //AA : To setup pin code textField
        pinCodeTextField.floatingLabel = true
        pinCodeTextField.validationType = .Number
        pinCodeTextField.setPlaceHolder("Pin Code")
        
    }
    
    func jsonCallBack(response: (JSON: JSON, NSError: NSError?))
    {
        
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        if(response.NSError != nil)
        {
            if (response.NSError!.code == -1009)
            {
                self.view.makeToast("No Internet connection")
            }
            else
            {
                self.view.makeToast("An error has been occurred")
                print("error")
            }
        }
        else
        {
            
            let ResultResponse = response.JSON["ResultResponse"].stringValue
            
            print(ResultResponse)
            
            //  VcLogin.LastLoginType = .Instagram
            if (ResultResponse == "0" || ResultResponse == "1")
            {
                let userid = response.JSON["UserId"].stringValue
                // get profile
                PinCodeViewController.GetProfile(self,UserID: userid){success,pro in
                    if(success)
                    {
                        
                        //AA : To save the mobile number into the user defaults to be able to login directly
                        let userDefaults = NSUserDefaults.standardUserDefaults()
                        userDefaults.setValue(self.passedPhoneNumber, forKey: "MobileNumber")
                        userDefaults.setValue(self.pinCodeTextField.text ?? "", forKey: "PinCode")
                        
                        userDefaults.synchronize();
                        VcLogin.LastLoginType = LoginTypes.Normal;
                        
                        /*
                         if(rememberMe)
                         {
                         let userDefaults = NSUserDefaults.standardUserDefaults()
                         userDefaults.setValue(username, forKey: "UserName")
                         userDefaults.setValue(password, forKey: "Password")
                         userDefaults.synchronize();
                         LastLoginType = LoginTypes.Normal;
                         }
                         */
                        let welcome = AppDelegate.RoutToScreen("VcPostingOptions") as! VcPostingOptions
                        
                        
                        if redirectionId != nil{
                            // welcome.openLink = true
                            if redirectionScreen == "VcOffersDetails" {
                                let offersDetails = VcOffersDetails(nibName: redirectionScreen, bundle: nil)
                                offersDetails.loadFromID(redirectionId)
                                AppDelegate.navControler.pushViewController(offersDetails, animated: true)
                                
                            }
                        }
                    }
                }
                
            }
                //                else if (ResultResponse == "1")
                //                {
                //                    // no verefied
                //                    target.view.makeToast("This account has not been verified")
                //                }
                
            else if (ResultResponse == "2")
            {
                //   invalid user or pass
                self.view.makeToast("Invalid username or password")
            }
            else
            {
                self.view.makeToast("Somthing went wrong. try again")
            }
        }
        
    }
    
    // MARK: - API Calls Methods

    func callRegisterApi() {
        
        var RequestParameters: [String:String] = [:]  //= NSDictionary()
        let wsmethod = WSMethods.Registration
        
        
            RequestParameters = [
                "City":"",
                "Country":"",
                "Email":"\(passedEmail)",
                "FullName":"\(passedFullName ?? "")",
                "MobileNumber":passedPhoneNumber,
                "PinCode":"\(pinCodeTextField.text ?? "")",
                "RegId":"",
                "Username":"\(passedUserName ?? "")",
                "InviteCode":"\(passedInvitationCode ?? "")"
            ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: wsmethod, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString , responseType: ResponseType.DictionaryJson
            , callbackString: nil, callbackDictionary: jsonCallBack)
        
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
    }


    func Login(mobileNubmer:String)
    {
        
        
        var token = FIRInstanceID.instanceID().token()
        // print("InstanceID token: \(token)")
        
        if token == nil {
            
            token = NSUserDefaults.standardUserDefaults().stringForKey("regId")
            
            if token == nil {
                token = ""
            }
        }
        
        // login
        let RequestParameters : NSDictionary = [
            "RegId" : token ?? "",
            "MobileNumber" : passedPhoneNumber,
            "PinCode" : pinCodeTextField.text ?? ""
            
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Logging in"
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.Login, hTTPMethod: .post, parameters: nil, httpBody: RequestParametersString, responseType: .DictionaryJson, callbackString: nil, callbackDictionary: { (JSON, NSError) in
            MBProgressHUD.hideHUDForView(self.view, animated: true)
            if(NSError != nil)
            {
                if (NSError!.code == -1009)
                {
                    self.view.makeToast("No Internet connection")
                }
                else
                {
                    self.view.makeToast("An error has been occurred")
                    print("error")
                }
            }
            else
            {
                
                let ResultResponse = JSON["ResultResponse"].stringValue
                
                print(ResultResponse)
                
                //  VcLogin.LastLoginType = .Instagram
                if (ResultResponse == "0" || ResultResponse == "1")
                {
                    let userid = JSON["UserId"].stringValue
                    // get profile
                    PinCodeViewController.GetProfile(self,UserID: userid){success,pro in
                        if(success)
                        {
                            
                            //AA : To save the mobile number into the user defaults to be able to login directly
                            let userDefaults = NSUserDefaults.standardUserDefaults()
                            userDefaults.setValue(mobileNubmer, forKey: "MobileNumber")
                            userDefaults.setValue(self.pinCodeTextField.text ?? "", forKey: "PinCode")

                            userDefaults.synchronize();
                            VcLogin.LastLoginType = LoginTypes.Normal;

                            /*
                             if(rememberMe)
                             {
                             let userDefaults = NSUserDefaults.standardUserDefaults()
                             userDefaults.setValue(username, forKey: "UserName")
                             userDefaults.setValue(password, forKey: "Password")
                             userDefaults.synchronize();
                             LastLoginType = LoginTypes.Normal;
                             }
                             */
                            let welcome = AppDelegate.RoutToScreen("VcPostingOptions") as! VcPostingOptions
                            
                            
                            if redirectionId != nil{
                                // welcome.openLink = true
                                if redirectionScreen == "VcOffersDetails" {
                                    let offersDetails = VcOffersDetails(nibName: redirectionScreen, bundle: nil)
                                    offersDetails.loadFromID(redirectionId)
                                    AppDelegate.navControler.pushViewController(offersDetails, animated: true)
                                    
                                }
                            }
                        }
                    }
                    
                }
                    //                else if (ResultResponse == "1")
                    //                {
                    //                    // no verefied
                    //                    target.view.makeToast("This account has not been verified")
                    //                }
                    
                else if (ResultResponse == "2")
                {
                    //   invalid user or pass
                    self.view.makeToast("Invalid username or password")
                }
                else
                {
                    self.view.makeToast("Somthing went wrong. try again")
                }
            }
            
        })
    }
    
    static func GetProfile(target: UIViewController ,ForClient:Bool = true,UserID:String, complition:((success:Bool,profile:ModProfileInformation?)->())?)
    {
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(target.view, animated: true)
        loadingNotification.label.text = "Loading Profile..."
        
        let RequestParameters : NSDictionary = [
            "ProfileUserId" : UserID,
            "UserId" : ForClient ? UserID : (User?.Id)!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetProfileInformation, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(target.view, animated: true)
                
                if(response.result.isFailure)
                {
                    if(complition != nil)
                    {complition!(success: false,profile: nil);}
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            target.view.makeToast("No Internet connection")
                            
                        }
                        else
                        {
                            target.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        // print("JSON: \(JSON)")
                        let Response = ModProfileInformationResponse(json:JSON)
                        
                        if (Response.ResultResponse != "0"){
                            target.view.makeToast("An error has been occurred")
                            
                            if(complition != nil)
                            {complition!(success: false,profile: nil);}
                        }
                        else
                        {
                            //                            target.view.makeToast("got the profile :D")
                            if ForClient
                            {User = Response.Profileobj;}
                            
                            if(complition != nil)
                            {complition!(success: true,profile: Response.Profileobj);}
                            
                            
                        }
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
    }


    func callSocialRegistrationApi() {
        
        //  if let accessToken = AccessToken.current {
        
        for subview in self.view.subviews {
            
            if subview.tag == 2 && subview.hidden
            {
                
                continue
                
            }
            if let textField = subview as? EGFloatingTextField {
                
                textField.resignFirstResponder()
                
                if textField.hasError == true
                {
                    return
                }
            }
            
        }
        
        var RequestParameters: [String:String] = [:]  //= NSDictionary()
        var wsmethod = WSMethods.Registration
        
        wsmethod = WSMethods.RegistrationSocial
            //RequestParameters = [];
            
            RequestParameters["RegType"] = "3"
            RequestParameters["SMUserId"] = passedInstagramId
            RequestParameters["City"] = ""
            RequestParameters["Country"] = ""
            RequestParameters["Email"] = passedEmail ?? ""
            RequestParameters["FullName"] = passedFullName ?? ""
            RequestParameters["MobileNumber"] = passedPhoneNumber ?? ""
            RequestParameters["ProfilePicture"] = passedInstagramProfilePictureURL ?? ""
            RequestParameters["RegId"] = ""
            RequestParameters["Username"] = passedUserName ?? ""
            RequestParameters["InviteCode"] = passedInvitationCode ?? ""
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: wsmethod, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString , responseType: ResponseType.DictionaryJson
            , callbackString: nil, callbackDictionary: socialRegistrationJsonCallBack)
        
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
    }
    
    func socialRegistrationJsonCallBack(response: (JSON: JSON, NSError: NSError?))
    {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    self.view.makeToast("No Internet connection")
                }
                else
                {
                    self.view.makeToast("An error has been occurred")
                    print("error")
                }
                return
            }
        }
        else
        {
            
            //  let ResultResponse = response.JSON["ResultResponse"].stringValue;
            
            let UserId = response.JSON["UserId"].stringValue;
            
            
            // get profile
            
            
            AppDelegate.RoutToScreen("VcLogin")
            
            //            VcLogin.GetProfile(self, UserID: UserId, complition: { (success,pro) in
            //                if(success)
            //                {
            //
            //                    AppDelegate.RoutToScreen("VcPostingOptions")
            //                }
            //                
            //            })
        }
        
    }

}
