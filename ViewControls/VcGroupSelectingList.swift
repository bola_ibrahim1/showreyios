//
//  VcGroupSelectingList.swift
//  ShowRey
//
//  Created by User on 11/15/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import MBProgressHUD
import SwiftyJSON
import FirebaseAnalytics

class VcGroupSelectingList: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var followeesTable: UITableView!
    
    enum comeFrom : Int{
        case createGroup = 0
        case createPost = 1
    }
    
    var come = comeFrom.createGroup
    
    
    var tvCreatePost : TvCreatePost!
    var feedProfilePros : ModFeedProfilePros!
    var search:Bool = false
    var PagesCount = 1
    var selectedFollowees:[Int] = []
    var cameSelectedFollowees:[Int] = []
    var participantsObj = [ModFollowersobj]()
    var followees = [ModFollowersobj]()
    var searchedFollowees = [ModFollowersobj]()
    var cameSelected = [ModFollowersobj]()
    var button:UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        followeesTable.allowsSelection = true
        self.navigationItem.title = "Add Participants"
        getFollowees(PagesCount, FromPagerOrRefresher: false)
        
        followeesTable.addInfiniteScrollWithHandler { (UITableView) in
            
            self.getFollowees(self.PagesCount, FromPagerOrRefresher: true)
        }
        
        
        
        
        
        
        // add search button to navigation bar
        
        button = UIButton()
        button.frame = CGRectMake(0, 0, 31, 31)
        button.setImage(UIImage(named: "Nav-Bar-(Search)"), forState: .Normal)
        button.addTarget(self, action: #selector(btnSearch), forControlEvents: .TouchUpInside)
        let barButton = UIBarButtonItem()
        barButton.customView = button
        navigationItem.rightBarButtonItem = barButton
        
        // Do any additional setup after loading the view.
        
        FIRAnalytics.logEventWithName("MYBroadCasts", parameters: nil)
        
        if come == .createPost {
            
            btnNext.setTitle("Send", forState: .Normal )
        }
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        //        selectedFollowees = []
        //
        //        for pp in followees
        //        {
        //            let K = cameSelected.indexOf({o in return o.Id == pp.Id} )
        //
        //            if (K != nil)
        //            {
        //
        //                followees = followees.filter { $0 != pp }
        //            }
        //        }
        //
        //
        self.followeesTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func btnSearch() {
        button.frame = CGRectMake(0, 0, 0, 0)
        let searchBarItem = UISearchBar()
        searchBarItem.showsCancelButton = true
        searchBarItem.placeholder = "Search"
        searchBarItem.delegate = self
        
        self.navigationItem.titleView = searchBarItem
        
    }
    
    
    @IBAction func btnNextAction(sender: AnyObject) {
        
        
        if selectedFollowees.count == 0 {
            
            self.view.makeToast("Please select participant")
            return
        }
        
        participantsObj = []
        
        if followees.count != 0 {
            for i in 0  ..< followees.count  {
                
                
                if selectedFollowees.contains( (followees[i].Id?.integerValue)! ){
                    
                    
                    participantsObj.append(followees[i])
                }
            }
            
        }
        
        if searchedFollowees.count != 0 {
            
            for i in 0  ..< searchedFollowees.count  {
                
                if selectedFollowees.contains( (searchedFollowees[i].Id?.integerValue)!){
                    
                    participantsObj.append(searchedFollowees[i])
                }
                
            }
        }
        
        
        if selectedFollowees.count != participantsObj.count {
            
            
            //          participantsObj // obj1
            
            //           cameSelected // obj2
            
            
            
            for pp in cameSelected
            {
                let K = participantsObj.indexOf({o in return o.Id == pp.Id} )
                
                if (K == nil)
                {
                    participantsObj.append(pp)
                }
            }
            
            
            
            
            var participantsObjCopy = participantsObj
            
            for i in 0  ..< participantsObjCopy.count  {
                
                if !selectedFollowees.contains( (participantsObjCopy[i].Id?.integerValue)!){
                    
                    participantsObj.removeAtIndex(i)
                }
                
            }
            
            
        }
        
        //        cameSelected.appendContentsOf(participantsObj)
        //        cameSelectedFollowees.appendContentsOf(selectedFollowees)
        
        if come == .createGroup {
            
            let allViewController: [UIViewController] = self.navigationController!.viewControllers as [UIViewController];
            
            var foundview = false
            for selectingPeople : UIViewController in allViewController
            {
                if selectingPeople .isKindOfClass(VcGroupEdit)
                {
                    
                    let cast = selectingPeople as! VcGroupEdit
                    
                    foundview  = true
                    
                    
                    cast.getSelectedFollowees = selectedFollowees
                    cast.getFollowees = participantsObj
                    cast.parent = self
                    
                    self.navigationController?.popToViewController(selectingPeople, animated: true)
                }
            }
            if (foundview == false)
            {
                
                
                let createGroup = self.storyboard?.instantiateViewControllerWithIdentifier("VcGroupEdit") as! VcGroupEdit
                createGroup.getSelectedFollowees = selectedFollowees
                createGroup.getFollowees = participantsObj
                createGroup.parent = self
                
                
                self.navigationController?.pushViewController(createGroup, animated: true)
            }
        }else if come == .createPost{
            
            tvCreatePost.CreatePost(self.view, FeedProfilePros: feedProfilePros ,comeFromWelcome: true, WPostToId: selectedFollowees, WPostToType: 1)
            
            
            
        }
        
        
        
    }
    
    // MARK: - search bar delegate
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        
        self.navigationItem.titleView = nil
        self.navigationItem.title = "Add Participants"
        button.frame = CGRectMake(0, 0, 31, 31)
        
        
        search = false
        self.followeesTable.reloadData()
        
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText != ""{
            
            
            getSearchableFollowees(searchText)
            
            
        }else{
            
            
            
            search = false
            self.followeesTable.reloadData()
            
        }
        
    }
    
    
    
    // MARK: - table data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if !search {
            
            if followees.count == 0 {
                tableView.hidden = true
                btnNext.hidden = true
                
                
            }else {
                tableView.hidden = false
                btnNext.hidden = false
            }
            
            return followees.count
            
        }else {
            
            if searchedFollowees.count == 0 {
                tableView.hidden = true
                btnNext.hidden = true
                
                
            }else {
                tableView.hidden = false
                btnNext.hidden = false
            }
            
            return searchedFollowees.count
            
            
        }
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: TcGroupSelectingList = tableView.dequeueReusableCellWithIdentifier("TcGroupSelectingList", forIndexPath: indexPath) as! TcGroupSelectingList
        
        cell.btnAdd.setImage(UIImage(named: "Add-Mark"), forState: .Normal)
        
        if !search {
            
            if selectedFollowees.contains((followees[indexPath.row].Id?.integerValue)!) {
                cell.btnAdd.setImage(UIImage(named: "Check-Mark"), forState: .Normal)
                
            }else{
                cell.btnAdd.setImage(UIImage(named: "Add-Mark"), forState: .Normal)
            }
            
            
            cell.lblName.text = followees[indexPath.row].FullName
            cell.lblNo.text = followees[indexPath.row].UserName
            
            
            cell.profileImage.kf_setImageWithURL(NSURL(string: (followees[indexPath.row].ProfilePicture)! ), placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            
            cell.btnAdd.tag = followees[indexPath.row].Id as! Int
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VcGetGroups.showProfileInfo(_:)))
            cell.profileImage.tag = followees[indexPath.row].Id as! Int
            cell.profileImage.userInteractionEnabled = true
            
            cell.profileImage.addGestureRecognizer(tapGestureRecognizer)
            
            cell.btnprofileoutlit.tag = followees[indexPath.row].Id as! Int
            
            
        }else {
            
            if selectedFollowees.contains((searchedFollowees[indexPath.row].Id?.integerValue)!) {
                cell.btnAdd.setImage(UIImage(named: "Check-Mark"), forState: .Normal)
                
            }else{
                cell.btnAdd.setImage(UIImage(named: "Add-Mark"), forState: .Normal)
            }
        
            cell.lblName.text = searchedFollowees[indexPath.row].FullName
            cell.lblNo.text = searchedFollowees[indexPath.row].UserName
            
            
            cell.profileImage.kf_setImageWithURL(NSURL(string: (searchedFollowees[indexPath.row].ProfilePicture)! ), placeholderImage: UIImage(named:"Profile-Picture"), optionsInfo: nil, progressBlock: nil, completionHandler: nil)
            cell.profileImage.tag = searchedFollowees[indexPath.row].Id as! Int
            cell.btnAdd.tag = searchedFollowees[indexPath.row].Id as! Int
            
            cell.btnprofileoutlit.tag = searchedFollowees[indexPath.row].Id as! Int
            
            
            
            
            cell.parentView = self;
              cell.id = searchedFollowees[indexPath.row].Id as! Int
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(VcGetGroups.showProfileInfo(_:)))
            
            cell.profileImage.userInteractionEnabled = true
            
            cell.profileImage.addGestureRecognizer(tapGestureRecognizer)
            
        }
        
        
        cell.btnAdd.addTarget(self, action:  #selector(addFollower(_:)), forControlEvents: .TouchUpInside)
         cell.btnprofileoutlit.addTarget(self, action:  #selector(clickOnProfile(_:)), forControlEvents: .TouchUpInside)
        
        
        
        cell.profileImage.layer.cornerRadius = cell.profileImage.frame.size.width / 2
        cell.profileImage.clipsToBounds = true
        
        if indexPath.row % 2 == 0 {
            
            cell.backgroundColor = UIColor(hex: "#f7f7f7")
            
        }else {
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        return cell
    }
    
    
    var FeedData:ModFeedFeedobj!
    
    var Parent:VcMasterNewsFeed!
    func showProfileInfo(sender:AnyObject){
       /* var mainFeed = FeedData
        let profile = Parent.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        if !search {
            profile.SetupForTimeLine(followees[sender.tag].Id as! Int)
            
        }
        else{
            profile.SetupForTimeLine(searchedFollowees[sender.tag].Id as! Int)
            
        }
        
        
        Parent.parent.navigationController?.pushViewController(profile, animated: true)
        */
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as!
        TcGroupSelectingList
        let mybutton = cell.btnAdd
        let img = mybutton.imageForState(.Normal)
        
        
        if(img  == UIImage(named: "Check-Mark"))
        {
            mybutton.setImage(UIImage(named: "Add-Mark"), forState: .Normal)
            selectedFollowees = selectedFollowees.filter { $0 != mybutton.tag }
            
            
        }
        else{
            
            mybutton.setImage(UIImage(named: "Check-Mark"), forState: .Normal)
            selectedFollowees.append(mybutton.tag)
            
            
        }
    }
    
    func addFollower(mybutton: UIButton)
    {
        
        let img = mybutton.imageForState(.Normal)
        
        
        if(img  == UIImage(named: "Check-Mark"))
        {
            mybutton.setImage(UIImage(named: "Add-Mark"), forState: .Normal)
            selectedFollowees = selectedFollowees.filter { $0 != mybutton.tag }
            
            
        }
        else{
            
            mybutton.setImage(UIImage(named: "Check-Mark"), forState: .Normal)
            selectedFollowees.append(mybutton.tag)
            
            
        }
        
        
    }
    
    
    // MARK: - get data from web service
    
    func getSearchableFollowees(searchStr : String){
        let RequestParameters : NSDictionary = [
            "Keyword" : searchStr,
            "UserId":(User?.Id)!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.AutoCompleteFollowers, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: { (JSON, NSError) in
                
                
                if((NSError) != nil)
                {
                    if let networkError = NSError {
                        if (networkError.code == -1009) {
                            self.view.makeToast("No Internet connection")
                        }
                        else
                        {
                            self.view.makeToast("An error has been occurred")
                            print("error")
                        }
                        return
                    }
                }
                else
                {
                    
                    
                    if ( JSON["ResultResponse"].stringValue == "0")
                    {
                        let Profileobjobj =   JSON["Followersobj"]
                        
                        
                        self.searchedFollowees.removeAll()
                        
                        for commentDictionary in Profileobjobj  {
                            
                            let groupfollow : ModFollowersobj = ModFollowersobj()
                            
                            
                            
                            groupfollow.UserName = commentDictionary.1["UserName"].rawString()
                            groupfollow.Id  =  NSNumberFormatter().numberFromString(commentDictionary.1["Id"].rawString()!)!
                            groupfollow.FullName  = commentDictionary.1["FullName"].rawString()
                            groupfollow.ProfilePicture  = commentDictionary.1["ProfilePicture"].rawString()
                            //   groupfollow.FollowStatus = commentDictionary.1["FollowStatus"].rawString()
                            
                            
                            self.searchedFollowees.append(groupfollow)
                        }
                        
                        self.search = true
                        
                        self.followeesTable.reloadData()
                    }
                }
                
                
        })
        
    }
    
    
    func getFollowees(index:Int, FromPagerOrRefresher:Bool)
    {
        
        if !FromPagerOrRefresher
        {
            let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            loadingNotification.label.text = "Loading Following..."
        }
        
        
        let RequestParameters : NSDictionary = [
            "PageIndex":index,
            "PageSize":100,
            "ProfileUserId": (User?.Id)!,
            "UserId": (User?.Id)!
        ]
        
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        
        NetworkHelper.RequestHelper(nil, service: WSMethods.GetFollowees, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.StringJson , callbackString:
            {response in
                
                MBProgressHUD.hideHUDForView(self.view, animated: true)
                
                self.followeesTable.finishInfiniteScroll()
                
                
                if(response.result.isFailure)
                {
                    
                    
                    if let networkError = response.result.error {
                        if (networkError.code == -1009) {
                            
                            
                            
                            self.view.makeToast("No Internet connection", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                        }
                        else
                        {
                            
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            print("error")
                        }
                        return
                    }
                }
                
                if(response.result.isSuccess)
                {
                    if let JSON = response.result.value {
                        
                        let Response = ModListOfPeoples(json:JSON)
                        
                        if (Response.ResultResponse == "21"){
                            
                            self.view.makeToast("An error has been occurred", duration: 3, position: CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height - 110))
                            
                        }
                        else
                        {
                            //
                            
                            self.followees.appendContentsOf(Response.Followersobj!)
                            print("Success")
                            self.PagesCount += 1
                            self.followeesTable.reloadData()
                            
                            
                        }
                        
                    }
                }
                
            }
            , callbackDictionary: nil)
        
        
    }
    

    func clickOnProfile(mybutton: UIButton)
    {
        let profile = self.storyboard?.instantiateViewControllerWithIdentifier("VcMasterNewsFeed") as! VcMasterNewsFeed
        profile.SetupForTimeLine("\(mybutton.tag)")
        self.navigationController?.pushViewController(profile, animated: true)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
