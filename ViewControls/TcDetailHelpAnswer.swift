//
//  TcDetailHelpAnswer.swift
//  ShowRey
//
//  Created by Appsinnovate on 1/28/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit

class TcDetailHelpAnswer: UITableViewCell {

    @IBOutlet weak var detailAnswers: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setCell(answer:String){
        detailAnswers.text = answer
    }

}
