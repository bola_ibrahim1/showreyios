//
//  VcForgetPassword2.swift
//  ShowRey
//
//  Created by Appsinnovate on 10/2/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import UIKit
import PureLayout
import EGFloatingTextField
import MBProgressHUD
import SwiftyJSON

class VcForgetPassword2: UIViewController {

   
    @IBOutlet weak var LblErrorPin: UILabel!
    @IBOutlet weak var TxtPinCode: EGFloatingTextField!
    var email : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Forget password"
        
        TxtPinCode.floatingLabel = true
        TxtPinCode.setPlaceHolder(" Pin Code")
        TxtPinCode.validationType = .Number
        TxtPinCode.errorLabel = LblErrorPin
        TxtPinCode.customerrorMessage = "Wrong pin code"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func BtnConfirmPassw2(sender: AnyObject) {
        let RequestParameters : NSDictionary = [
            "PinCode" : TxtPinCode.text!,
            "Email" : email
        ]
        let RequestParametersString =  OauthInjector.UpdateDictionary(RequestParameters)
        NetworkHelper.RequestHelper(nil, service: WSMethods.ForgetPasswordStp2, hTTPMethod: Method.post, parameters: nil, httpBody: RequestParametersString
            , responseType: ResponseType.DictionaryJson , callbackString: nil, callbackDictionary: jsonCallBack2)
        
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.label.text = "Loading..."
        
    }
    
    func jsonCallBack2(response: (JSON: JSON, NSError: NSError?))
    {
        MBProgressHUD.hideHUDForView(self.view, animated: true)
        
        if((response.NSError) != nil)
        {
            if let networkError = response.NSError {
                if (networkError.code == -1009) {
                    self.view.makeToast("No Internet connection")
                }
                else
                {
                    self.view.makeToast("An error has been occurred")
                    print("error")
                }
                return
            }
        }
        else
        {
            let resultResponse = response.JSON["ResultResponse"].stringValue
            if( resultResponse == "0"){
                
                
                let thirdView = self.storyboard?.instantiateViewControllerWithIdentifier("VcForgetPassword3") as! VcForgetPassword3
                self.navigationController?.pushViewController(thirdView, animated: true)
                
                thirdView.email =  email
                print(email)
                print("Success")
            }else{
                self.view.makeToast("An error has been occurred")
            }
            
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
