//
//  SLInstagramBaseModel.swift
//  instagram-swift
//
//  Created by Dimas Gabriel on 10/21/14.
//  Copyright (c) 2014 Swift Libs. All rights reserved.
//

import Foundation
import SwiftyJSON

class SLInstagramBaseModel {
  var ID : String?
  
  init(json: JSON) {
    ID = json["id"].string
  }
}

class SLInstagramMeta {
    var error_type : String?
    var code : String?
    var error_message : String?
    
    init(json: JSON) {
        error_type = json["error_type"].stringValue
        code = json["code"].stringValue
        error_message = json["error_message"].stringValue
    }
    
}
