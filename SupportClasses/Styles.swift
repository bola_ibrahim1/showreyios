//
//  Styles.swift
//  ShowRey
//
//  Created by M-Hashem on 10/24/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation

class Styles
{
    // shadow view should be provided for the realtime sizing views (constrains)
    static func Shadow(To:UIView, target: UIView,Radus: CGFloat,shadowview:UIView? = nil)
    {
        let shade = shadowview ?? UIView(frame: CGRect(x: target.frame.origin.x,y: target.frame.origin.y,width: target.frame.width,height: target.frame.height))
        shade.backgroundColor = UIColor.clearColor()
        shade.layer.shadowRadius = Radus
        shade.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        shade.layer.shadowOpacity = 0.6
        if (shadowview == nil)
        {
            shade.addSubview(target)
            // target.frame = CGRect(x: 0, y: 0, width: target.frame.width, height: target.frame.height)
            To.addSubview(shade)
        }
    }
}
