import UIKit
//import JDStatusBarNotification

class ValidationManager: NSObject {

    internal static func isEmptyText(testStr:String) -> Bool {
        
        if testStr.isEmpty || testStr == ""
        {
            return true
        } else {
            return false
        }
    }
    
    internal static func isValidEmail(testStr:String) -> Bool {

            do {
                let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .CaseInsensitive)
                return regex.firstMatchInString(testStr, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, testStr.characters.count)) != nil
            } catch {
                return false
            }
    }
    
    internal static func isValidPhone(testStr:String) -> Bool {
        
        let x: Int = testStr.characters.count
        
        if x >= 3 && x <= 15 {
            let pattern = "^(\\+)[0-9]{3,14}"
            
            let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
            if (predicate.evaluateWithObject(testStr) == true) {
                // Okay
                return true
            } else {
                // Not found
                return false
            }
        }
        return false
    }
    
    internal static func isValidZipCode(testStr:String) -> Bool {

        let x: Int = testStr.characters.count
        
        if x == 5 {
            
            let pattern = "^[0-9]{5}$"
            
            let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
            if (predicate.evaluateWithObject(testStr) == true) {
                // Okay
                
                return true
            } else {
                // Not found
                
                return false
            }
        }
        return false

    }
    
    internal static func isOnlyText(testStr: String) -> Bool {
        
        let pattern = "^[a-zA-Z]*$"
        
        let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
        if (predicate.evaluateWithObject(testStr) == true) {
            // Okay
            
            return true
        } else {
            // Not found
            
            return false
        }
        
    }
    
    

    internal static func isValidCreditCardNum(testStr: String) -> Bool {
        
        let x: Int = testStr.characters.count
        
        if x == 16 {
            
            let pattern = "^[0-9]*$"
            
            let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
            if (predicate.evaluateWithObject(testStr) == true) {
                // Okay
                
                return true
            } else {
                // Not found
                
                return false
            }
        }
        return false
        
    }

    internal static func isValidMonthNum(testStr: String) -> Bool {
        
        let x: Int = testStr.characters.count
        
        if x == 2 {
            
            let pattern = "^(1[0-2]|0[1-9])$"
            
            let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
            if (predicate.evaluateWithObject(testStr) == true) {
                // Okay
                
                return true
            } else {
                // Not found
                
                return false
            }
        }
        return false
        
    }
    
    internal static func isValidYearNum(testStr: String) -> Bool {
        
        let x: Int = testStr.characters.count
        
        if x == 2 {
            
            let pattern = "^[0-9]{2}$"
            
            let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
            if (predicate.evaluateWithObject(testStr) == true) {
                // Okay
                
                return true
            } else {
                // Not found
                
                return false
            }
        }
        return false
        
    }
    
    internal static func isValidCVV(testStr: String) -> Bool {
        
        let x: Int = testStr.characters.count
        
        if x == 3 {
            
            let pattern = "^[0-9]{3}$"
            
            let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
            if (predicate.evaluateWithObject(testStr) == true) {
                // Okay
                
                return true
            } else {
                // Not found
                
                return false
            }
        }
        return false
        
    }
    
    internal static func isOnlyNum(testStr: String) -> Bool {

        
            let pattern = "^[0-9]*$"
            
            let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
            if (predicate.evaluateWithObject(testStr) == true) {
                // Okay
                
                return true
            } else {
                // Not found
                
                return false
            }


    }
    
    internal static func isValidCodeValidation(testStr: String) -> Bool {
        
        let x: Int = testStr.characters.count
        
        if x >= 4 && x <= 6 {
            
            let pattern = "^[0-9]*$"
            
            let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
            if (predicate.evaluateWithObject(testStr) == true) {
                // Okay
                
                return true
            } else {
                // Not found
                
                return false
            }
        }
        return false
        
    }
    
    
    
//------------------------------------------------------------------------------------------
        
 /*   internal static func ValidStyle(controls :UIView... , ErrorMessage : String)
    {
        for control in controls
        {
            
            control.layer.borderWidth = 2.0
            control.layer.borderColor = UIColor.redColor().CGColor
        }
         JDStatusBarNotification.showWithStatus(ErrorMessage, dismissAfter: 3, styleName: JDStatusBarStyleError)
    }
*/
    
}
