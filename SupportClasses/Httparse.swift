//
//  LinkView.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 10/23/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import Ji

class httparse {
    
    var HtmlTitle  = "";
    var HtmlDescription  = "";
    var HtmlUrl  = "";
    var HtmlPhotoLink  = "";
    var Fullurl  = "";
    
    
    var HtmlPhoto:UIImage  = UIImage();
    
    static func LinkView( mytxt:String , complition:((httparse:httparse)->())? ) -> Bool
    {
        var myurl = "";
        

        
        let text = mytxt
        let types: NSTextCheckingType = .Link
        var URLStrings = [NSURL]()
        let detector = try? NSDataDetector(types: types.rawValue)
        let matches = detector!.matchesInString(text, options: .ReportCompletion, range: NSMakeRange(0, text.characters.count))
        
        for match in matches {
            print(match.URL!)
            URLStrings.append(match.URL!)
        }
        if ( URLStrings.count == 0) {
            return false;
        }
        else
        {
            myurl = URLStrings[0].absoluteString!
            
        }
        
        
        if (myurl == "")
        {
            return false
        }
        
        
        
        
        
        
        let myLinkView = httparse();
        
        myLinkView.HtmlDescription = ""
        myLinkView.HtmlUrl = ""
        myLinkView.HtmlPhotoLink = ""
        myLinkView.HtmlTitle = ""
        myLinkView.Fullurl = ""
        
       // var Fullurl :String = ""
        
        
        dispatch_async(dispatch_get_global_queue(2, 0)) {
            
            //# MARK: - convert to long path
            if (myurl.rangeOfString("http") == nil)
            {
                myLinkView.Fullurl = "http://\(myurl)"
            }
            else
            {
                myLinkView.Fullurl = myurl
            }
            
            if let home = Ji(htmlURL: NSURL(string: myLinkView.Fullurl)!){
                
                
                //# MARK: - find Facebook Tages
                
                if let metaNodes = home.xPath("//head/meta") {
                    
                    
                    for x in metaNodes
                    {
                        if ( x.attributes["property"]  == "og:description")
                        {
                            if let orgdescription = x.attributes["content"]
                            {myLinkView.HtmlDescription  = orgdescription}
                            
                        } else
                            if ( x.attributes["property"]  == "og:title")
                            {
                                if let orgtitle = x.attributes["content"]
                                {myLinkView.HtmlTitle  = orgtitle}
                                
                            }else
                                if ( x.attributes["property"]  == "og:url")
                                {
                                  if let orgurl = x.attributes["content"]
                                    {myLinkView.HtmlUrl  = orgurl}
                                }else
                                    if ( x.attributes["property"]  == "@og:image")
                                    {
                                        if let orgPhotoLink = x.attributes["content"]
                                        {myLinkView.HtmlPhotoLink  = orgPhotoLink}
                                      
                                    }else
                                        if ( x.attributes["name"]  == "description")
                                        {
                                            if (myLinkView.HtmlDescription  ==  "")
                                            {
                                                if let orgDescriptio = x.attributes["content"]
                                                {myLinkView.HtmlDescription  = orgDescriptio}
                                            }
                        }
                        
                    }
                }
                
                
                //# MARK: - find Title
                if (myLinkView.HtmlTitle  ==  "")
                {
                    if let titleNode = home.xPath("//head/title")!.first{
                        myLinkView.HtmlTitle = titleNode.content!
                    }
                }
                
                //# MARK: - find Description
                if (myLinkView.HtmlDescription  ==  "")
                {
                    if let pNode =  home.xPath("//body//p")!.first
                    {
                        if let con = pNode.content
                        {
                            myLinkView.HtmlDescription = con
                        }
                        
                    }
                    
                    
                }
                
                
                
                //# MARK: - find first photo "JPG" or "PNG"
                
                if (myLinkView.HtmlPhotoLink  ==  "")
                {
                    if let ImgNodes = home.xPath("//body//img") {
                        
                        
                        for pImgJpg in ImgNodes  // jpg
                        {
                            if let src = pImgJpg.attributes["src"]
                            {
                                if src.rangeOfString(".jpg") != nil{
                                    myLinkView.HtmlPhotoLink = pImgJpg.attributes["src"]!
                                    break;
                                }
                            }
                        }
                        if (myLinkView.HtmlPhotoLink  ==  "")
                        {
                            for pImgPng in ImgNodes  // png
                            {
                                if let src = pImgPng.attributes["src"]
                                {
                                    if src.rangeOfString(".png") != nil{
                                        myLinkView.HtmlPhotoLink = pImgPng.attributes["src"]!
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                
                
            }
            
            //# MARK: - Photo Convert Short Pasth to long one
            
            if (myLinkView.HtmlPhotoLink  !=  "")
            {
                if (myLinkView.HtmlPhotoLink.rangeOfString("http") == nil)&&(myLinkView.HtmlPhotoLink.rangeOfString("www") == nil)  // shortphoto path
                {
                    
                    if (myLinkView.HtmlPhotoLink.hasPrefix("/") == false)
                    {
                        myLinkView.HtmlPhotoLink = "/\(myLinkView.HtmlPhotoLink)"
                    }
                    
                    myLinkView.HtmlPhotoLink = "\(NSURL(string: myLinkView.Fullurl)!.scheme!)://\(NSURL(string: myLinkView.Fullurl)!.host!)\(myLinkView.HtmlPhotoLink)"
                    
                }
                
            }
            
            //# MARK: - find Url
            if (myLinkView.HtmlUrl  ==  "")
            {
                if let templink = NSURL(string: myLinkView.Fullurl)!.host
                {
                    myLinkView.HtmlUrl = templink
                }
            }
            
            
            //# MARK: - Defult Values
            
            if (myLinkView.HtmlTitle  ==  "")
            {
                if let templink = NSURL(string: myLinkView.Fullurl)!.host
                {
                    myLinkView.HtmlTitle = templink
                }
            }
            
            if (myLinkView.HtmlDescription  ==  "")
            {
                myLinkView.HtmlDescription  = "Click here to get more information..."
            }
            
            if (myLinkView.HtmlPhotoLink == "")
            {
                myLinkView.HtmlPhotoLink = myLinkView.Fullurl
            }
            //# MARK: - load photos from url
            if (myLinkView.HtmlPhotoLink != "")
            {
                
                if let PhotData = NSData(contentsOfURL: NSURL(string: myLinkView.HtmlPhotoLink)!){
                    myLinkView.HtmlPhoto = convertDataToPhoto(PhotData)
                }
                else
                {
                    myLinkView.HtmlPhoto = UIImage(named: "NoImageAvailable")!
                    
                }
                
            }
            else
            {
                myLinkView.HtmlPhoto = UIImage(named: "NoImageAvailable")!
                
            }
            
            
            //# MARK: - load photos in case of facebook or google
            
            if ( myLinkView.Fullurl.lowercaseString.rangeOfString("facebook") != nil)
            {
                myLinkView.HtmlPhoto = UIImage(named: "facebook_logo")!
            }
            
            if (myLinkView.Fullurl.lowercaseString.rangeOfString("google") != nil)
            {
                myLinkView.HtmlPhoto = UIImage(named: "google_logo")!
            }
            
            
            
            
            
            //# MARK: - call back
            dispatch_async(dispatch_get_main_queue()) {
                
                
                if(complition != nil)
                {
                    complition!(httparse: myLinkView)
                }
                
                
            }
        }
        return true;
    }
    //# MARK: - Conver tData To Photo
    static  func convertDataToPhoto(data:NSData) -> UIImage
    {
        var  myImage: UIImage = UIImage()
        
        
        if let tempphoto = UIImage(data: data)
        {
            myImage = tempphoto
        }
        else
        {
            myImage = UIImage(named: "NoImageAvailable")!
        }
        
        
        
        return myImage
    }
    
    
    
}



