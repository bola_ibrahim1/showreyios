//
//  ModNewsFeed.swift
//  ShowRey
//
//  Created by M-Hashem on 10/25/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import EVReflection

class ModFeedAttachedFiles : EVObject
{
     var Id : NSNumber? = nil
     var LevelId : NSNumber? = nil
     var ObjId : NSNumber? = nil
     var Thumb : String? = nil
     var thumbImage:UIImage?
     var URL : String? = nil
     var UserId : NSNumber? = nil
}

class ModFeedCItems : EVObject
{
     var Disliked : Bool = false
     var Id : NSNumber? = nil
     var It_Id : NSNumber? = nil
     var It_Name : String? = nil
     var Name : String? = nil
     var Liked : Bool = false
     var NoOfDislikes : NSNumber? = nil
     var NoOfLikes : NSNumber? = nil
}
class ModFeedProfilePros : EVObject
{
    var AgeGroup : NSNumber? = nil
    var BodyShape : NSNumber? = nil
    var Gender : String? = nil
    var Height : NSNumber? = nil
    var Size : String? = nil
    var Weight : NSNumber? = nil
}
class ModFeedUserInfo : EVObject
{
     var AccountType : NSNumber? = nil
     var FollowStatus : NSNumber? = nil
     var FullName : String? = nil
     var Id : NSNumber? = nil
     var ProfilePicture : String? = nil
     var UserName : String? = nil
}

enum PostToType : Int
{
    case Person = 1
}
enum PostType:Int
{
    case Regular = 1
    case Fashion = 2
}
enum PostContentType:Int
{
    case Text_emotions = 1
    case Text_Images = 2
    case Text_Videos = 3
    case Text_Link = 4
    case Shared = 6
}


class ModFeedFeedobj : EVObject
{
    var AttachedFiles : [ModFeedAttachedFiles]? = nil
    var CItems : [ModFeedCItems]? = nil
    var CommentPrivacy : Bool = false
    var Disliked : Bool = false
    var Id : NSNumber? = nil
    var IsEdited : Bool = false
    var Liked : Bool = false
    var NoOfComments : NSNumber? = nil
    var NoOfDislikes : NSNumber? = nil
    var NoOfLikes : NSNumber? = nil
    var PostCType : NSNumber? = nil
    var PostToId : NSNumber? = nil
    var PostToType : NSNumber? = nil
    var PostType : NSNumber? = nil
    var ProfilePros : ModFeedProfilePros? = nil
    var SharedPost : ModFeedFeedobj? = nil
    var SharedPostId : NSNumber? = nil
    var TaggedUsers : [ModFeedUserInfo]? = nil
    var Text : String? = nil
    var LinkPreview : httparse? = nil
    var Time : String? = nil
    var ToUserProfile : ModFeedUserInfo? = nil
    var UserId : NSNumber? = nil
    var UserProfile : ModFeedUserInfo? = nil
    var isAnonymous : Bool = false
    
    var OriginalPrice : NSNumber? = nil
    var Price : NSNumber? = nil
    var Title : String? = nil
    var ValidTo : String? = nil
    
    
}

class ModNewsFeed : ModResponse
{
     var Feedobj : [ModFeedFeedobj]? = nil
     var ListCount : NSNumber? = nil
     var PageCount : NSNumber? = nil
}
class ModNewsFeed_Single : ModResponse
{
    var Feedobj : ModFeedFeedobj? = nil

}
