//
//  ModPostStatistics.swift
//  ShowRey
//
//  Created by User on 12/21/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import EVReflection


class ModPostStatistics : EVObject {
    
     var CountNO : NSNumber? = nil
     var CountNoAnswer : NSNumber? = nil
     var CountYES : NSNumber? = nil
     var Date : String = ""
     var NO : NSNumber? = nil
     var NoAnswer : NSNumber? = nil
     var PostId : NSNumber? = nil
     var Title : String = ""
     var YES : NSNumber? = nil
     var UserCount : NSNumber? = nil
    

}


class ModPostStatisticsResponse : ModResponse {
      var ListCount : NSNumber? = nil
      var PageCount : NSNumber? = nil
      var PostStatistics : [ModPostStatistics]? = nil
      var PostStatisticob : ModPostStatistics? = nil
}
