//
//  ModGetGroups.swift
//  ShowRey
//
//  Created by Radwa Khaled on 11/15/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import EVReflection

class ModGroupsobj : EVObject {
     var Id : NSNumber? = nil
     var Info : String? = nil
     var Name : String? = nil
     var Privacy : Bool = false
     var ProfilePicture : String? = nil
     var UserId : NSNumber? = nil
     var IsAdmin : Bool = false
}


class ModGetGroupsResponse : ModResponse {
    
    var Groupsobj : [ModGroupsobj]? = nil
     var ListCount : NSNumber? = nil
     var PageCount : NSNumber? = nil
    
}

class ModGetGroupResponse : ModResponse {
    
    var Group : ModGroupsobj? = nil
    var ListCount : NSNumber? = nil
    var PageCount : NSNumber? = nil
    
}
