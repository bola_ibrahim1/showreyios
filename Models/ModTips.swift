//
//  ModTips.swift
//  ShowRey
//
//  Created by M-Hashem on 11/21/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import EVReflection

class ModTip : EVObject {
    var Date : String? = nil
    var Id : NSNumber? = nil
    var Text : String? = nil
    var Title : String? = nil
}

class ModTipsResponse : ModResponse
{
    var ListCount : NSNumber? = nil
    var PageCount : NSNumber? = nil
    var Tips : [ModTip]? = nil
}
class ModTipResponse : ModResponse
{
    var Tipob : ModTip? = nil
}

