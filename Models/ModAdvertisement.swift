//
//  ModAdvertisement.swift
//  ShowRey
//
//  Created by M-Hashem on 1/17/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import Foundation
import EVReflection


class ModAdvertise : EVObject
{
    enum Action_type:Int
    {
        case preview = 0
        case link = 1
        case phone_number = 2
        case SMS = 3
        case offers = 4
    }
    
     var Action_Details : String? = nil // browser url
     var Action_target : String? = nil // phone num, offer_id
     var Action_type : String? = nil
     var Adv_URL : String? = nil // img
     var Details : String? = nil // ignore
     var DurationTime : String? = nil
     var Id : NSNumber? = nil
     var Lang : String? = nil
     var Name : String? = nil
     var Type : NSNumber? = nil
}

class ModAdvertisementResponse : ModResponse
{
    var AdsList : [ModAdvertise]? = nil
    var Count : NSNumber? = nil
    var LUpdateTime : String? = nil
    var NxtAdsTime : String? = nil
    
}
