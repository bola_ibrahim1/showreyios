//
//  RootObject.swift
//
//  Created by JSON to Swift on 2016/9/21
//  Copyright (c) 2016 json2swift.anyinfa.com. All rights reserved.
//

import Foundation
import EVReflection

class ModAppConfiguration:ModResponse {
    
    var AboutCompany: ModAboutCompany!
    
    var AboutDeveloper: ModAboutDeveloper!
    
    var ConfigSettings: ModConfigSettings!
    
    var ConfigString: ModConfigString!
    
    var UpdateStatus: ModUpdateStatus!
}

class ModUpdateStatus:EVObject {
    
    /** AppUrl: "String content" */
    var AppUrl: String? = nil
    
    /** Description: "String content" */
    var Description: String? = nil
    
    
    /** IsMand: true */
    var IsMand: Bool = true
    
    /** Status: true */
    var Status: Bool = true
    
    /** version: 1267432370000000 */
    var version: NSNumber? = nil
    
}

class ModConfigString:EVObject {
    
    /** AccTypeReg: "String content" */
    var AccTypeReg: String? = nil
    
    /** Disclaimer: "String content" */
    var Disclaimer: String? = nil
    
    /** Help: "String content" */
    var Help: String? = nil
    
    /** NewVersionText: "String content" */
    var NewVersionText: String? = nil
    
    /** PrivacyPolicy: "String content" */
    var PrivacyPolicy: String? = nil
    
    /** TellAFriend: "String content" */
    var TellAFriend: String? = nil
    
    /** TermandCond: "String content" */
    var TermandCond: String? = nil
    
    /** UpStatus: "String content" */
    var UpStatus: String? = nil
    
}

class ModConfigSettings:EVObject {
    
    /** Ads: true */
    var Ads: Bool = true
    
    /** MaximumVideosLength: "String content" */
    var MaximumVideosLength: String? = nil
    
    /** NoOfItemBetweenAds: 2147483647 */
    var NoOfItemBetweenAds: NSNumber? = nil
    
    /** UpStatus: "String content" */
    var UpStatus: String? = nil
    
    /** UploadImages: true */
    var UploadImages: Bool = true
    
    /** UploadVideos: true */
    var UploadVideos: Bool = true
    
   }

class ModAboutDeveloper:EVObject {
    
    /** Description: "String content" */
    var Description: String? = nil
    
    /** Email: "String content" */
    var Email: String? = nil
    
    /** Facebook: "String content" */
    var Facebook: String? = nil
    
    /** Id: 2147483647 */
    var Id: NSNumber? = nil
    
    /** Logo: "String content" */
    var Logo: String? = nil
    
    /** Name: "String content" */
    var Name: String? = nil
    
    /** Phone: "String content" */
    var Phone: String? = nil
    
    /** Twitter: "String content" */
    var Twitter: String? = nil
    
    /** UpStatus: "String content" */
    var UpStatus: String? = nil
    
    /** Website: "String content" */
    var Website: String? = nil
    
}

class ModAboutCompany:EVObject {
    
    /** Description: "String content" */
    var Description: String? = nil
    
    /** Email: "String content" */
    var Email: String? = nil
    
    /** Facebook: "String content" */
    var Facebook: String? = nil
    
    /** Id: 2147483647 */
    var Id: NSNumber? = nil
    
    /** Logo: "String content" */
    var Logo: String? = nil
    
    /** Name: "String content" */
    var Name: String? = nil
    
    /** Phone: "String content" */
    var Phone: String? = nil
    
    /** Twitter: "String content" */
    var Twitter: String? = nil
    
    /** UpStatus: "String content" */
    var UpStatus: String? = nil
    
    /** Website: "String content" */
    var Website: String? = nil
    
}


