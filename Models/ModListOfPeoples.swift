//
//  ModListOfPeoples.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 11/15/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import EVReflection

class ModFollowersobj : EVObject {
     var AccountType : NSNumber? = nil
     var FollowStatus : NSNumber? = nil
     var FullName : String? = nil
     var Id : NSNumber? = nil
     var ProfilePicture : String? = nil
     var UserName : String? = nil
     var IsAdmin : Bool = false

}

class ModListOfPeoples :  ModResponse {   
     var Followersobj : [ModFollowersobj]? = nil
     var ListCount : NSNumber? = nil
     var PageCount : NSNumber? = nil
}
