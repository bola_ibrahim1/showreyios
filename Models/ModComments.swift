//
//  ModComments.swift
//  ShowRey
//
//  Created by Mohamed Helmy on 12/2/16.
//  Copyright © 2016 Appsinnovate. All rights reserved.
//

import Foundation
import EVReflection

class ModAttachedFiles : EVObject {
     var Id : NSNumber? = nil
     var LevelId : NSNumber? = nil
     var ObjId : NSNumber? = nil
     var Thumb : String? = nil
     var URL : String? = nil
     var UserId : NSNumber? = nil
}
class ModTaggedUsers : EVObject {
     var AccountType : NSNumber? = nil
     var FollowStatus : NSNumber? = nil
     var FullName : String? = nil
     var Id : NSNumber? = nil
     var ProfilePicture : String? = nil
     var UserName : String? = nil
}
class ModCommentsFollowersobj : EVObject {
     var AccountType : NSNumber? = nil
    var AttachedFiles : [ModAttachedFiles]? = nil
     var Comment : String? = nil
     var CommentType : NSNumber? = nil
     var Disliked : Bool = false
     var FullName : String? = nil
     var Id : NSNumber? = nil
     var IsEdited : Bool = false
     var LevelId : NSNumber? = nil
     var Liked : Bool = false
     var NoOfComments : NSNumber? = nil
     var NoOfDilikes : NSNumber? = nil
     var NoOfLikes : NSNumber? = nil
     var PostId : NSNumber? = nil
     var ProfilePicture : String? = nil
     var TaggedUsers : [ModTaggedUsers]? = nil
     var Time : String? = nil
     var UserId : NSNumber? = nil
     var UserName : String? = nil
     var isWhisper : Bool = false
    var LinkPreview : httparse? = nil
}

class ModComments : ModResponse {
    var Followersobj : [ModCommentsFollowersobj]? = nil
     var ListCount : NSNumber? = nil
     var PageCount : NSNumber? = nil

}
