//
//  TcAd.swift
//  ShowRey
//
//  Created by M-Hashem on 1/18/17.
//  Copyright © 2017 Appsinnovate. All rights reserved.
//

import UIKit
import MessageUI

class TcAd: UITableViewCell {

    @IBOutlet weak var AdImage: UIImageView!
    var AdData:ModAdvertise!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        AdImage.addGestureRecognizer(UITapGestureRecognizer(target: self,action: #selector(Clicked)))
   
        AdImage.layer.cornerRadius = 3;
    AdImage.layer.borderColor = UIColor.whiteColor().CGColor
        AdImage.layer.borderWidth = 5
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func Setup(data: ModAdvertise)
    {
        AdData = data
        AdImage.kf_setImageWithURL(NSURL(string: data.Adv_URL!),placeholderImage: UIImage(named: "placeholder"), optionsInfo: nil, progressBlock: nil, completionHandler: nil);
  
    }
    func Clicked()
    {
        if AdData.Type!.integerValue == ModAdvertise.Action_type.link.rawValue
        {
            let unrappedURL = NSURL(string: AdData.Action_Details!.stringByReplacingOccurrencesOfString(" ", withString: "%20").lowercaseString)
            if let rappedURL = unrappedURL
            {
            AppDelegate.attemptOpenURL(AdData.Action_Details!.stringByReplacingOccurrencesOfString(" ", withString: "%20"))
            }
            else
            {
               // makeToast("Invalid URL : \(AdData.Action_Details!)")
            }
        }
        else if AdData.Type!.integerValue == ModAdvertise.Action_type.offers.rawValue
        {
            let offersDetails = VcOffersDetails(nibName: "VcOffersDetails", bundle: nil)
            offersDetails.loadFromID(AdData.Action_target!)
            AppDelegate.navControler.pushViewController(offersDetails, animated: true)
        }
        else if AdData.Type!.integerValue == ModAdvertise.Action_type.SMS.rawValue
        {
            HomeScreen.sendTextMessage("", Num: AdData.Action_target!)
        }
        else if AdData.Type!.integerValue == ModAdvertise.Action_type.phone_number.rawValue
        {
            VcHome.callNumber(AdData.Action_target!)
        }
        
    }
    
  
    
}
