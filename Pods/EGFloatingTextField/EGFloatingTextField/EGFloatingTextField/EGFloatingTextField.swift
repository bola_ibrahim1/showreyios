//
//  EGFloatingTextField.swift
//  EGFloatingTextField
//
//  Created by Enis Gayretli on 26.05.2015.
//
//
import UIKit
import Foundation
import PureLayout


public enum EGFloatingTextFieldValidationType {
    case Email
    case Number
    case Empty
    case Phone
    case OnlyText
    case Password
    case ConfirmPassword
}

public class EGFloatingTextField: UITextField {
    
    
    private typealias EGFloatingTextFieldValidationBlock = ((text:String,inout message:String)-> Bool)!
    
    public var validationType : EGFloatingTextFieldValidationType!
    
    
    private var emailValidationBlock  : EGFloatingTextFieldValidationBlock
    private var numberValidationBlock : EGFloatingTextFieldValidationBlock
    private var emptyValidationBlock : EGFloatingTextFieldValidationBlock
    private var phoneValidationBlock : EGFloatingTextFieldValidationBlock
    private var onlyTextValidationBlock : EGFloatingTextFieldValidationBlock
    private var passwordTextValidationBlock : EGFloatingTextFieldValidationBlock
    private var confirmpasswordTextValidationBlock : EGFloatingTextFieldValidationBlock
    
    
    let kDefaultInactiveColor = UIColor(white: CGFloat(0), alpha: CGFloat(0.54))
    let kDefaultActiveColor = UIColor(red: 179/255, green: 121/255, blue: 231/255, alpha: 1)
    //  let kDefaultErrorColor = UIColor.redColor()
    let kDefaultLineHeight = CGFloat(22)
    let kDefaultLabelTextColor = UIColor(white: CGFloat(0), alpha: CGFloat(0.54))
    
    
    public var floatingLabel : Bool!
    var label : UILabel!
    var labelFont : UIFont!
    var labelTextColor : UIColor!
    var activeBorder : UIView!
    var floating : Bool!
    var active : Bool!
    public var ErrorColor = UIColor.redColor()
    public var hasError : Bool!
    public var customerrorMessage : String!
    public var errorMessage : String!
    public var errorLabel = UILabel()
    public var validationARG1 : UITextField = UITextField();
    
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }
    
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit(){
        
        self.emailValidationBlock = ({(text:String, inout message: String) -> Bool in
            
            let x: Int = text.characters.count
            if( x > 0 && x <= 40) {
                
                let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
                
                let emailTest: NSPredicate = NSPredicate(format: "SELF MATCHES %@", emailRegex)
                if (emailTest.evaluateWithObject(text) == true) {
                    // Okay
                    
                    return true
                } else {
                    // Not found
                    message = "Enter a valid email address"
                    return false
                }
                
            }
            message = "Enter a valid email address"
            return false
            
            //            let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
            //
            //            let emailTest = NSPredicate(format:"SELF MATCHES %@" , emailRegex)
            //
            //            let  isValid = emailTest.evaluateWithObject(text)
            //            if !isValid {
            //                message = "Invalid Email Address"
            //            }
            //            return isValid;
            
        })
        
        self.emptyValidationBlock = ({(text:String, inout message: String) -> Bool in
            // if !(text.isEmpty || text == "")
            let x: Int = text.characters.count
            if (x > 0 && x <= 20)
            {
                return true
            } else if x == 0 {
                message = "Enter user name"
                return false
            }
            
            message = "User name should be less than 20 character"
            return false
        })
        
        self.numberValidationBlock = ({(text:String,inout message: String) -> Bool in
            let numRegex = "[0-9.+-]+";
            let numTest = NSPredicate(format:"SELF MATCHES %@" , numRegex)
            
            let isValid =  numTest.evaluateWithObject(text)
            if !isValid {
                message = "Invalid Number"
            }
            return isValid;
            
        })
        
        self.phoneValidationBlock = ({(text:String,inout message: String) -> Bool in
            
            let x: Int = text.characters.count
            
            
            if(x > 0 && x <= 15) {
                
                let pattern = "^[0-9]{3,10}"
                
                //let pattern = "^(\\+)[0-9]{3,14}"
                
                let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
                if (predicate.evaluateWithObject(text) == true) {
                    // Okay
                    return true
                } else {
                    // Not found
                    message = "Invalid phone number"
                    return false
                }
            }else if (x == 0){
                return true
            }
            
            message = "Phone number should be less than 15 character"
            return false
            
        })
        self.onlyTextValidationBlock = ({(text:String,inout message: String) -> Bool in
            // only text code /////////////////////////////////
            
            let x: Int = text.characters.count
            if( x > 0 && x <= 20) {
                
                let pattern = "^[a-zA-Z ]+$"
                
                let predicate: NSPredicate = NSPredicate(format: "SELF MATCHES %@", pattern)
                if (predicate.evaluateWithObject(text) == true) {
                    // Okay
                    
                    return true
                } else {
                    // Not found
                    message = "Invalid full name"
                    return false
                }
                
            }else if x == 0 {
                
                message = "Enter full name"
                return false
            }
            message = "Full name should be less than 20 character"
            return false
            
            //empty code ///////////////////////
            
            //            let x: Int = text.characters.count
            //            if (x > 0 && x <= 20)
            //            {
            //                return true
            //            } else if x == 0 {
            //                message = "Enter user name"
            //                return false
            //            }
            //
            //            message = "User name should be less than 20 character"
            //            return false
            
            
        })
        
        self.passwordTextValidationBlock = ({(text:String,inout message: String) -> Bool in
            let x: Int = text.characters.count
            if x > 5 && x < 15{
                return true
            }else if x < 5 {
                
                message = "Password should be more than 5 characters"
                return false
                
            }
            message = "Password should be less than 15 character"
            return false
            
        })
        
        self.confirmpasswordTextValidationBlock = ({(text:String,inout message: String) -> Bool in
            
            if text ==  self.validationARG1.text {
                return true
            }
            else
            {
                message = "Password dosen't match"
                return false
            }
            
        })
        
        
        self.floating = false
        self.hasError = false
        
        self.labelTextColor = kDefaultLabelTextColor
        self.label = UILabel(frame: CGRectZero)
        self.label.font = self.labelFont
        self.label.textColor = self.labelTextColor
        self.label.textAlignment = NSTextAlignment.Left
        self.label.numberOfLines = 1
        self.label.layer.masksToBounds = false
        self.addSubview(self.label)
        
        
        self.activeBorder = UIView(frame: CGRectZero)
        self.activeBorder.backgroundColor = kDefaultActiveColor
        self.activeBorder.layer.opacity = 0
        self.addSubview(self.activeBorder)
        
        self.label.autoAlignAxis(ALAxis.Horizontal, toSameAxisOfView: self)
        self.label.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Left, ofView: self)
        self.label.autoMatchDimension(ALDimension.Width, toDimension: ALDimension.Width, ofView: self)
        self.label.autoMatchDimension(ALDimension.Height, toDimension: ALDimension.Height, ofView: self)
        
        self.activeBorder.autoPinEdge(ALEdge.Bottom, toEdge: ALEdge.Bottom, ofView: self)
        self.activeBorder.autoPinEdge(ALEdge.Left, toEdge: ALEdge.Left, ofView: self)
        self.activeBorder.autoPinEdge(ALEdge.Right, toEdge: ALEdge.Right, ofView: self)
        self.activeBorder.autoSetDimension(ALDimension.Height, toSize: 2)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(UITextInputDelegate.textDidChange(_:)), name: "UITextFieldTextDidChangeNotification", object: self)
    }
    public func setPlaceHolder(placeholder:String){
        
        self.label.text = placeholder
        
        if(self.text != ""){
            let flag:Bool = true
            
            
            /*   if flag {
             
             if self.floatingLabel! {
             
             if !self.floating! || self.text!.isEmpty {
             self.floatLabelToTop()
             self.floating = true
             }
             } else {
             self.label.textColor = kDefaultActiveColor
             self.label.layer.opacity = 0
             }
             self.showActiveBorder()
             }
             
             self.active=flag
             */
            self.floatLabelToTop()
            if flag {
                
                if self.floatingLabel! {
                    
                    if self.floating! && self.text!.isEmpty {
                        self.animateLabelBack()
                        self.floating = false
                    }
                } else {
                    if self.text!.isEmpty {
                        self.label.layer.opacity = 1
                    }
                }
                //  self.label.textColor = UIColor(white: CGFloat(0), alpha: CGFloat(0.54))
                //    self.showInactiveBorder()
                self.validate()
            }
            self.active = flag
            self.label.textColor = UIColor(white: CGFloat(0), alpha: CGFloat(0.54))
            // super.resignFirstResponder()
            
            
        }
        
    }
    
    override public func becomeFirstResponder() -> Bool {
        
        let flag:Bool = super.becomeFirstResponder()
        
        if flag {
            
            if self.floatingLabel! {
                
                if !self.floating! || self.text!.isEmpty {
                    self.floatLabelToTop()
                    self.floating = true
                }
            } else {
                self.label.textColor = kDefaultActiveColor
                self.label.layer.opacity = 0
            }
            self.showActiveBorder()
        }
        
        self.active=flag
        return flag
    }
    override public func resignFirstResponder() -> Bool {
        
        let flag:Bool = super.becomeFirstResponder()
        
        if flag {
            
            if self.floatingLabel! {
                
                if self.floating! && self.text!.isEmpty {
                    self.animateLabelBack()
                    self.floating = false
                }
            } else {
                if self.text!.isEmpty {
                    self.label.layer.opacity = 1
                }
            }
            self.label.textColor = kDefaultInactiveColor
            self.showInactiveBorder()
            self.validate()
        }
        self.active = flag
        
        super.resignFirstResponder()
        return flag
        
    }
    
    override public func drawRect(rect: CGRect){
        super.drawRect(rect)
        
        let borderColor = self.hasError! ? ErrorColor : kDefaultInactiveColor
        
        let textRect = self.textRectForBounds(rect)
        let context = UIGraphicsGetCurrentContext()
        let borderlines : [CGPoint] = [CGPointMake(0, CGRectGetHeight(textRect) - 1),
                                       CGPointMake(CGRectGetWidth(textRect), CGRectGetHeight(textRect) - 1)]
        
        if  self.enabled  {
            
            CGContextBeginPath(context!);
            CGContextAddLines(context!, borderlines, 2);
            CGContextSetLineWidth(context!, 1.0);
            CGContextSetStrokeColorWithColor(context!, borderColor.CGColor);
            CGContextStrokePath(context!);
            
        } else {
            
            CGContextBeginPath(context!);
            CGContextAddLines(context!, borderlines, 2);
            CGContextSetLineWidth(context!, 1.0);
            let  dashPattern : [CGFloat]  = [2, 4]
            CGContextSetLineDash(context!, 0, dashPattern, 2);
            CGContextSetStrokeColorWithColor(context!, borderColor.CGColor);
            CGContextStrokePath(context!);
            
        }
    }
    
    func textDidChange(notif: NSNotification){
        self.validate()
    }
    
    func floatLabelToTop() {
        
        CATransaction.begin()
        CATransaction.setCompletionBlock { () -> Void in
            self.label.textColor = self.kDefaultActiveColor
        }
        
        let anim2 = CABasicAnimation(keyPath: "transform")
        let fromTransform = CATransform3DMakeScale(CGFloat(1.0), CGFloat(1.0), CGFloat(1))
        var toTransform = CATransform3DMakeScale(CGFloat(0.75), CGFloat(0.75), CGFloat(1))
        toTransform = CATransform3DTranslate(toTransform, -CGRectGetWidth(self.label.frame)/6, -CGRectGetHeight(self.label.frame), 0)
        anim2.fromValue = NSValue(CATransform3D: fromTransform)
        anim2.toValue = NSValue(CATransform3D: toTransform)
        anim2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        let animGroup = CAAnimationGroup()
        animGroup.animations = [anim2]
        animGroup.duration = 0.3
        animGroup.fillMode = kCAFillModeForwards;
        animGroup.removedOnCompletion = false;
        self.label.layer.addAnimation(animGroup, forKey: "_floatingLabel")
        self.clipsToBounds = false
        CATransaction.commit()
    }
    func showActiveBorder() {
        
        self.activeBorder.layer.transform = CATransform3DMakeScale(CGFloat(0.01), CGFloat(1.0), 1)
        self.activeBorder.layer.opacity = 1
        CATransaction.begin()
        self.activeBorder.layer.transform = CATransform3DMakeScale(CGFloat(0.01), CGFloat(1.0), 1)
        let anim2 = CABasicAnimation(keyPath: "transform")
        let fromTransform = CATransform3DMakeScale(CGFloat(0.01), CGFloat(1.0), 1)
        let toTransform = CATransform3DMakeScale(CGFloat(1.0), CGFloat(1.0), 1)
        anim2.fromValue = NSValue(CATransform3D: fromTransform)
        anim2.toValue = NSValue(CATransform3D: toTransform)
        anim2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        anim2.fillMode = kCAFillModeForwards
        anim2.removedOnCompletion = false
        self.activeBorder.layer.addAnimation(anim2, forKey: "_activeBorder")
        CATransaction.commit()
    }
    
    func animateLabelBack() {
        CATransaction.begin()
        CATransaction.setCompletionBlock { () -> Void in
            self.label.textColor = self.kDefaultInactiveColor
        }
        
        let anim2 = CABasicAnimation(keyPath: "transform")
        var fromTransform = CATransform3DMakeScale(0.75, 0.75, 1)
        fromTransform = CATransform3DTranslate(fromTransform, -CGRectGetWidth(self.label.frame)/6, -CGRectGetHeight(self.label.frame), 0);
        let toTransform = CATransform3DMakeScale(1.0, 1.0, 1)
        anim2.fromValue = NSValue(CATransform3D: fromTransform)
        anim2.toValue = NSValue(CATransform3D: toTransform)
        anim2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        let animGroup = CAAnimationGroup()
        animGroup.animations = [anim2]
        animGroup.duration = 0.3
        animGroup.fillMode = kCAFillModeForwards;
        animGroup.removedOnCompletion = false;
        
        self.label.layer.addAnimation(animGroup, forKey: "_animateLabelBack")
        CATransaction.commit()
    }
    func showInactiveBorder() {
        
        CATransaction.begin()
        CATransaction.setCompletionBlock { () -> Void in
            self.activeBorder.layer.opacity = 0
        }
        let anim2 = CABasicAnimation(keyPath: "transform")
        let fromTransform = CATransform3DMakeScale(1.0, 1.0, 1)
        let toTransform = CATransform3DMakeScale(0.01, 1.0, 1)
        anim2.fromValue = NSValue(CATransform3D: fromTransform)
        anim2.toValue = NSValue(CATransform3D: toTransform)
        anim2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        anim2.fillMode = kCAFillModeForwards
        anim2.removedOnCompletion = false
        self.activeBorder.layer.addAnimation(anim2, forKey: "_activeBorder")
        CATransaction.commit()
    }
    
    func performValidation(isValid:Bool,message:String){
        if !isValid {
            self.hasError = true
            if ( customerrorMessage == nil)
            {
                self.errorMessage = message
            }
            else
            {
                self.errorMessage = customerrorMessage
                
            }
            errorLabel.text = self.errorMessage;
            errorLabel.textColor = ErrorColor;
            errorLabel.hidden = false
            self.labelTextColor = ErrorColor
            self.activeBorder.backgroundColor = ErrorColor
            self.setNeedsDisplay()
        } else {
            self.hasError = false
            self.errorMessage = nil
            errorLabel.text = ""
            errorLabel.hidden = true
            self.labelTextColor = kDefaultActiveColor
            self.activeBorder.backgroundColor = kDefaultActiveColor
            self.setNeedsDisplay()
        }
    }
    
    func validate(){
        
        if self.validationType != nil {
            var message : String = ""
            
            if self.validationType! == .Email {
                
                let isValid = self.emailValidationBlock(text: self.text!, message: &message)
                
                performValidation(isValid,message: message)
                
            } else if self.validationType! == .Number {
                let isValid = self.numberValidationBlock(text: self.text!, message: &message)
                
                performValidation(isValid,message: message)
            } else if self.validationType! == .Empty {
                let isValid = self.emptyValidationBlock(text: self.text!, message: &message)
                
                performValidation(isValid,message: message)
            } else if self.validationType! == .OnlyText {
                let isValid = self.onlyTextValidationBlock(text: self.text!, message: &message)
                
                performValidation(isValid,message: message)
            }else if self.validationType! == .Password {
                let isValid = self.passwordTextValidationBlock(text: self.text!, message: &message)
                
                performValidation(isValid,message: message)
            }else if self.validationType! == .Phone {
                let isValid = self.phoneValidationBlock(text: self.text!, message: &message)
                
                performValidation(isValid,message: message)
            }else if self.validationType! == .ConfirmPassword {
                let isValid = self.confirmpasswordTextValidationBlock(text: self.text!, message: &message)
                
                performValidation(isValid,message: message)
            }
            
            
            
            
            
        }
    }
    
    
}

extension EGFloatingTextField {
    
}
